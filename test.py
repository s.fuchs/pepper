#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author Stephan Fuchs (fuchss@rki.de)
# This code is under a modified MIT licence, you can find the complete licence here: https://gitlab.com/s.fuchs/pepper


import pytest
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__))))

if __name__ == "__main__":
	pytest.main()
