<img src="img/logo.png"  width="130" height="130" align="right"><br>
<img src="img/saltnpepper.png" width="178" height="100" align="left"><br><br><br><br><br><br>

## Short Description
Salt & Pepper is a software suite for **bacterial proteogenomics**. 
While Salt enables the creation of protein sequence databases and feature tables 
based on a specific genome sequence, Pepper provides the identification of open
reading frames supported by mass spectrometrically detected peptides.

You can find detailed documentation under 
[README_SALT.md](README_SALT.md) (Salt) and
[README_PEPPER.md](README_PEPPER.md) (Pepper), respectively.

## Requirements
Salt & Pepper requires Python 3 with the following non-standard modules:
*  biopython
*  pandas
*  pytest

```bash
# install requirements using pip
pip3 install biopython pandas pytest
```

## Installation

```bash
# install git (if not installed yet)
sudo apt-get install git

# install salt & pepper using Git
git clone https://gitlab.com/s.fuchs/pepper

# update salt & pepper (once installed)
cd path/to/saltnpepper
git update

# test salt & pepper (once installed)
path/to/saltnpepper/test.py
```

## Quick Help
```bash
#salt
path/to/saltnpepper/salt.py -h

#pepper
path/to/saltnpepper/pepper.py -h
```

## Full Documentation
Full documentation can be found
for Salt under [README_SALT.md](README_SALT.md) and
for Pepper under [README_PEPPER.md](README_PEPPER.md)


