#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author Stephan Fuchs (fuchss@rki.de)
# This code is under MIT licence, you can find the complete licence here: https://gitlab.com/s.fuchs/pepper

#dependencies
try:
	import argparse
except ImportError:
	print('The "argparse" module is not available. Use Python 3.2 or better.')
	sys.exit(1)

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Blast.Applications import NcbitblastnCommandline
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from bisect import bisect_right
from collections import defaultdict
import bisect
import doctest
import re
import sys
import time
import os
import csv
import multiprocessing
import datetime
import itertools

from _version import __version_info__, __version__

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "modules"))
import speccheck

#functions
class output():

	@staticmethod
	def createResultDir(prefix):
		err = 1
		while err > 0:
			timecode = time.strftime("%Y-%m-%d_%H-%M-%S")
			path = prefix+ "_" + timecode
			if os.path.isdir(path):
				err += 1
				time.sleep(1)
			else:
				os.makedirs(path)
				err = 0
			if err == 30:
				print("ERROR: giving up to find available result directory name")
				exit()
		return path

	@staticmethod
	def formatColumns(rows, empty='', space=5):
		'''
		formats nested list (1. level = rows; 2. level = columns) in equal-sized columns.
		Fields containing integers, floats or percentages are aligned to the right.

		Input:
		rows	list of rows. Each row is a sublist of field values (columns)
		empty	value used for empty fields
		space   number of spaces used between columns

		doctest:
		>>> data=[[1, "first row", "100%"], [2, "second row", "1%"], [3, "", "10%"]]
		>>> output.formatColumns(data, empty='na')
		'1     first row      100%     \\n2     second row       1%     \\n3                     10%     \\n'

		'''

		#allowed signs in numbers
		allowed = ['+', '-', '.', ',', '%']

		#str conversion
		rows = [[str(x) for x in row] for row in rows]

		#fill up rows to same number of columns (important for transposing)
		maxlen = max([len(x) for x in rows])
		[x.extend([empty]*(maxlen-len(x))) for x in rows]
		align = []
		output = [''] * len(rows)
		for column in zip(*rows):
			#detect numbers to align right
			string = ''.join(column)
			for i in allowed:
				string = string.replace(i, '')
			if string.strip('+-.,').isdigit():
				align = 'r'
			else:
				align = 'l'

			maxlen = max([len(x) for x in column])
			for i in range(len(column)):
				if align == 'r':
					output[i] += ' ' * (maxlen - len(column[i])) + column[i] + ' ' * space
				else:
					output[i] += column[i] + ' ' * (maxlen - len(column[i])) + ' ' * space

		return '\n'.join(output) + '\n'

class seqCollection():
	'''
	seqCollections are collections of DNA sequences (e.g. genome sequences of a given species) providing lookups of dna and peptide sequences
	'''

	def __init__(self, fastafile, descr = False, id=False, circular=True, tt_id=11, cpus=1, silent = False):
		#seqs
		self.seqs = []
		self.circular = circular
		self.tt_id = tt_id

		#general attributes
		self.cpus = cpus
		self.silent = silent
		self.descr = descr
		self.colSize = 0
		self.id = id

		self.addFastaFile(fastafile)
		self.name = fastafile
		self.basename = os.path.basename(fastafile)
		self.absname = os.path.abspath(fastafile)

	def getCollectionId(self):
		return str(self.id)

	def getCollectionFiles(self):
		'''
		returns set of filenames imported to the collection
		'''
		return sorted(set([x[-1] for x in self.seqs]))

	def logParameter(self, id=False):
		'''
		returns a list of information about the collection
		'''
		log = []
		if not self.id:
			id = ""
		else:
			id = " " + str(self.id).strip()
		log.append('# seqCollection' + id + ': ' +  self.descr)

		i = 0
		for file in self.getCollectionFiles():
			i += 1
			log.append('# seqCollection' + id + ' file ' + str(i) + ': ' +  os.path.basename(file))

		log.append('# seqCollection' + id + ' size: ' + str(self.getCollectionSize()) + " sequence(s)")

		if self.circular:
			log.append('# seqCollection' + id + ' moleculate type: circular')
		else:
			log.append('# seqCollection' + id + ' moleculate type: linear')

		return log

	def revCompl(self, seq):
		'''
		reverse-complements a given DNA sequence (ambiguous letters are not considered)
		'''
		return seq.lower().replace("a", "T").replace("t", "A").replace("c", "G").replace("g", "C")[::-1]

	def addEntry(self, input_queue, output_queue):
		'''
		adds FASTA entry to collection
		'''
		for seq, header, file in iter(input_queue.get, '{{QUEUE_END}}'):
			seq = seq.upper()
			revseq = self.revCompl(seq)
			if self.circular:
				searchstr = seq*2 + "\t" + revseq*2
			else:
				searchstr = seq + "\t" + revseq
			aa = self.translateSeq(seq) + "*" + self.translateSeq(revseq)

			output_queue.put([seq, revseq, searchstr, aa, header, file])
		output_queue.put("{{JOB_END}}")
		return

	def addFastaFile(self, file):
		'''
		adds entries of a given FASTA file to the collection
		'''
		if file in self.getCollectionFiles():
			return False

		jobs = []

		for record in SeqIO.parse(file, "fasta"):
			jobs.append([str(record.seq), str(record.id), file])

		if self.silent:
			msg = False
		else:
			msg = "  adding entries ... [%s/" + str(len(jobs)) + "]"

		self.seqs.extend(self.multiprocessor(self.addEntry, jobs, msg))
		self.colSize = self.getCollectionSize()
		return

	def getCollectionSize(self):
		return len(self.seqs)

	def seqExists(self, input_queue, output_queue):
		for needle in iter(input_queue.get, '{{QUEUE_END}}'):
			output_queue.put((needle, len([True for x in self.getSearchStr() if needle in x])))
		output_queue.put("{{JOB_END}}")

	def seqsWithSubseqs(self, seqs):
		out = {}
		if not self.silent:
			msg = "  lookup DNA seqs in db #" + str(self.getCollectionId()) + " ... [%s/" + str(len(seqs)) + "]"
		else:
			msg = False
		for seq, n in self.multiprocessor(self.seqExists, seqs, msg):
			out[seq] = n
		return out

	def getTranslationTable(self):
		'''
		returns translation table based on NCBI's numbering
		'''
		tables = {}
		tables[11] = {'TTT':  'F', 'TTC':  'F', 'TTA':  'L', 'TTG':  'L', 'CTT':  'L', 'CTC':  'L', 'CTA':  'L', 'CTG':  'L', 'ATT':  'I', 'ATC':  'I', 'ATA':  'I', 'ATG':  'M', 'GTT':  'V', 'GTC':  'V', 'GTA':  'V', 'GTG':  'V', 'TCT':  'S', 'TCC':  'S', 'TCA':  'S', 'TCG':  'S', 'CCT':  'P', 'CCC':  'P', 'CCA':  'P', 'CCG':  'P', 'ACT':  'T', 'ACC':  'T', 'ACA':  'T', 'ACG':  'T', 'GCT':  'A', 'GCC':  'A', 'GCA':  'A', 'GCG':  'A', 'TAT':  'Y', 'TAC':  'Y', 'TAA': '*', 'TAG': '*', 'CAT':  'H', 'CAC':  'H', 'CAA':  'Q', 'CAG':  'Q', 'AAT':  'N', 'AAC':  'N', 'AAA':  'K', 'AAG':  'K', 'GAT':  'D', 'GAC':  'D', 'GAA':  'E', 'GAG':  'E', 'TGT':  'C', 'TGC':  'C', 'TGA':  '*', 'TGG':  'W', 'CGT':  'R', 'CGC':  'R', 'CGA':  'R', 'CGG':  'R', 'AGT':  'S', 'AGC':  'S', 'AGA':  'R', 'AGG':  'R', 'GGT':  'G', 'GGC':  'G', 'GGA':  'G', 'GGG':  'G'}
		if not self.tt_id in tables:
			exit("ERROR: Translation table " + str(self.translTableID) + " not implemented yet")

		return tables[self.tt_id]

	def getEntries(self):
		return [x for x in self.seqs]

	def getSearchStr(self):
		return [x[2] for x in self.seqs]

	def getAa(self):
		return [x[3] for x in self.seqs]

	def getDna(self, strand = "+"):
		if strand == "+":
			return [x[0] for x in self.seqs]
		elif strand == "-":
			return [x[1] for x in self.seqs]

	def multiprocessor (self, worker, jobs, msg = False):
		#config
		cpus = self.cpus

		#build queues and processes
		input_queue = multiprocessing.Queue()
		output_queue = multiprocessing.Queue()
		[input_queue.put(x) for x in jobs]
		[input_queue.put('{{QUEUE_END}}') for x in range(cpus)]
		processes = [multiprocessing.Process(target=worker, args=(input_queue, output_queue, )) for i in range(cpus)]

		#start processes
		for process in processes:
			process.start()

		#collate job outputs
		outputs = []
		completed = 0
		l = 0
		while completed < cpus:
			out = output_queue.get()
			if out == "{{JOB_END}}":
				completed += 1
			else:
				outputs.append(out)
				l += 1
				if msg:
					print(msg % l, end ="\r")

		#joining
		i = 0
		for process in processes:
			i += 1
			process.join()

		#messsaging
		if msg:
			print()

		return outputs

	def translateSeq(self, seq):
		tt = self.getTranslationTable()
		all_frame_aa = []
		aa = [[]]
		if len(seq)%3 == 0:
			seq = seq + seq[1:] + seq[0] + seq[2:] + seq[:2] # concatenate all 3 frames
			codons = [seq[i:i+3] for i in range(0, len(seq), 3)]
			for codon in codons:
				if codon not in tt:
					a = "*"
				else:
					a = tt[codon]
				if a == "*":
					aa.append([])
				else:
					aa[-1].append(a)
			aa = ["".join(x) for x in aa]
			if len(aa) > 1:
				all_frame_aa.extend(aa[1:-1] + ["".join(aa[-1] + aa[0])])
			else:
				all_frame_aa.extend(aa)
		else:
			seq = seq * 4
			codons = [seq[i:i+3] for i in range(0, len(seq), 3)]
			for codon in codons:
				if codon in tt:
					a = tt[codon]
				else:
					a = "*"
				if a == "*":
					aa.append([])
				else:
					aa[-1].append(a)
			aa = ["".join(x) for x in aa]
			if len(aa) > 1:
				all_frame_aa.extend(aa[1:-1] + ["".join(aa[-1] + aa[0])])
			else:
				all_frame_aa.extend(aa)
		return ("*".join(list(set(filter(None, all_frame_aa)))))

	def pepExists(self, input_queue, output_queue):
		for needle in iter(input_queue.get, '{{QUEUE_END}}'):
			output_queue.put((needle, len([True for x in self.getAa() if needle in x])))
		output_queue.put("{{JOB_END}}")

	def seqsWithPeps(self, peps):
		out = {}
		if not self.silent:
			msg = "  lookup peptides and predicted proteins in db #" + str(self.getCollectionId()) + " ... [%s/" + str(len(peps)) + "]"
		else:
			msg = False
		for aa, n in self.multiprocessor(self.pepExists, peps, msg):
			out[aa] = n
		return out

class evidenceFile():
	def __init__(self, filename, perrp, msms, score, minint, delim="\t", id = False, group=0):
		'''
		class to parse evidence files generated by MaxQuant

		Input:
		filename	path & file name of evidence file
		delim		field delimiter (default is "\t")
		'''
		self.name = filename
		self.basename = os.path.basename(filename)
		self.absname = os.path.abspath(filename)
		self.delim = delim
		self.dir = os.path.dirname(self.absname)
		self.id = id
		self.group = group
		self.perrp = perrp #minimal Posterior Error Probability to consider peptide for spectral counting (Float)
		self.msms = msms #if true consider only peptides with MS/MS data for spectral counting (bool)
		self.score = score #minimal score to consider peptide for spectral counting (Float)
		self.peps, self.pepids, self.proteins, self.pepexistence = self.processPepData()

	def logParameter(self):
		'''
		returns a list of lines to log parameter
		an object id can be optionally given
		'''
		if not self.id:
			id = ""
		else:
			id = " " + str(self.id).strip()

		log = []
		log.append('# evidence file' + id + ': ' + self.name)
		log.append('# evidence file replication group' + id + ': ' + str(self.group))
		log.append('# unique peptides in evidence file' + id + ': ' +  str(len(self.peps)))
		if self.msms:
			m = ""
		else:
			m = "not "

		log.append('# peptides criteria to consider for spectral counting (evidence file' + id + '): posterior error probability <= ' + str(self.perrp) + "; score >= " + str(self.score) + "; MS/MS data " + m + "mandatory")
		return log

	def exists(self):
		'''
		returns True if evidence file exists otherwise False
		'''
		return os.path.isFile(self.absname)

	def lower_first(self, iterator):
		return itertools.chain([next(iterator).lower()], iterator)

	def read(self):
		'''
		iterator providing CSV-based dictionary for each data line in evidence file
		'''
		if self.exists == False:
			exit("ERROR: Evidence file does not exist.")
		with open(self.absname, 'r') as handle:
			l = 0
			for line in csv.DictReader(self.lower_first(handle), delimiter=self.delim):
				l += 1
				if line['sequence'] == "":
					continue
				yield(line)

	def processPepData(self):
		'''
		extracts all peptide-related infos from evidence file including
			Experiment
			Sequence
			Peptide ID
			Proteins (Identifications)
		'''
		peps = {}
		pepids = {}
		proteins = {}
		pepexistence = {}
		for pep in self.read():
			exp = pep['experiment']
			seq = pep['sequence']
			pepid = pep['peptide id']
			if len(pep['proteins'].strip()) > 0:
				prots = pep['proteins'].split(",")
			else:
				prots = pep['leading proteins'].split(",")

			#Spectral Counting Filter
			f = 0

			##MS/MS Filter
			if self.msms == False or int(pep['ms/ms count']) > 0:
				f +=1
				msms = True
			else:
				msms = False

			##PEP filter
			if float(pep['pep']) <= self.perrp:
				f += 1

			##Score filter
			if float(pep['score']) >= self.score:
				f += 1

			if f == 3:
				if not seq in peps:
					peps[seq] = {}
					pepids[seq] = set()
					proteins[seq] = set()
				if not exp in peps[seq]:
					peps[seq][exp] = 0

				peps[seq][exp] += int(pep['ms/ms count'])
				pepids[seq].add(pepid)
				for prot in prots:
					proteins[seq].add(prot)

			elif f == 2 and not msms:
				if not exp in pepexistence:
					pepexistence[exp] = set()
				pepexistence.add(seq)

		return peps, pepids, proteins, pepexistence

	def delPep(self, pepseq):
		'''
		deletes a peptide based on sequence from dataset
		'''
		if pepseq in self.peps:
			del(self.peps[pepseq])
			del(self.pepids[pepseq])
			del(self.proteins[pepseq])
			return True
		else:
			return False


	def getExperiments(self):
		'''
		returns a sorted list of all Experiments included in the evidence file
		'''
		exps = set()
		for pepseq in self.getPepSeqs():
			exps.update(self.peps[pepseq].keys())
		return sorted(exps)

	def getSpectralCounts(self, pepseq, exp):
		'''
		returns spectral counts (occurence of the peptide within the evidence file) of a given peptide and experiment

		Input:
		pepseq		aa sequence of the given peptide
		exp			Experiment name

		Output
		na		means that peptide sequence not found in evidence file
		-		peptide not found in this experiment
		int		spectral counts		'''

		count = False
		if pepseq in self.peps and exp in self.peps[pepseq]:
			return self.peps[pepseq][exp]

		if exp in self.pepexistence and pepseq in pepexistence[exp]:
			return "+"

		return "-"

	def getPepProts(self, pepseq):
		'''
		returns a set of protein identifications listed in the evidence file for the given peptide

		Input:
		pepseq		aa sequence of the given peptide

		Output:
		na			unkown peptide
		set(...)	set of identifications

		'''

		if pepseq not in self.proteins:
			return []
		else:
			return self.proteins[pepseq]

	def getPepId(self, pepseq):
		'''
		returns a set of protein identifcations listed in the evidence file forn the given peptide

		Input:
		pepseq		aa sequence of the given peptide

		Output:
		na			unkown peptide
		set(...)	set of identifications

		'''

		if pepseq not in self.pepids:
			return False
		else:
			return self.pepids[pepseq]

	def getPepIds(self, pepseq):
		'''
		returns sorted list of stored peptide IDs
		'''
		return sorted(self.pepids.values())

	def getPepSeqs(self, sortById = False):
		'''
		returns list of stored peptide sequences.
		Optionally, list can be sorted based on peptide IDs using sortById=True
		'''
		if not sortById:
			return self.peps.keys()
		else:
			pepseqs = []
			for pepseq, pepid in self.pepids.items():
				pepseqs.append([pepid, pepseq])
			pepseqs.sort()
			return [x[1] for x in pepseqs]

class pepper():
	def __init__(self, reffilename, gb_record = False, tt=11, silent = True, cpus=1, digestEnzyme="trypsin", uniq_only = False, uniq_only_smart = False):
		'''
		class to provide peptide mapping to circular prokaryote genomes (no splicing)

		Input:
		reffilename		name of genbank file conatining reference sequence
		tt				number of NCBI's translation table to use (default: 11)
		silent			if True, all stdout statements are supressed
		aaBefore		aminoacids before digestion site (for ORF detection; only single aa allowed)
		'''

		#general attributes
		self.cpus = cpus
		self.silent = silent

		#process translation table
		self.translTableID = tt
		self.translTable = self.getTranslationTable()
		self.reversedTableFwd = self.createReverseTable("fwd")
		self.reversedTableRev = self.createReverseTable("rev")

		#process sequence to map against
		self.refrecord = None
		self.refid = None
		self.refheader = None
		self.refseq = None
		self.reflen = None
		self.refgenes = [] #(list index, start, end, strand, frame, dna sequence, aa sequence, locus number)
		self.refgenestarts = []
		self.refproducts = []
		if reffilename:
			self.addRef(reffilename)
		elif gb_record:
			self.addGbRecord(gb_record)
		else:
			sys.exit("Input error: neither reffilename or gb_record defined")
		self.refFilename = reffilename

		#pep attributes
		self.pepids = {}
		self.seqfilter = False

		#filter
		self.uniq_only = uniq_only
		self.uniq_only_smart = uniq_only_smart
		self.not_uniq = 0
		self.not_uniq_smart = 0

		#proteases cut pattern description: number aa before cut, number aa after cut, regex
		proteases = {
			"trypsin": (1, 1, "[KR][^P]"),
			"lys-c": (1, 1, "K[^P]"),
			"arg-c": (2, 1, ".R[^P]|KK."),
			"glu-c": (1, 1, "[DE]."),
			"asp-n": (1, 1, ".[DC]"),
			"chymotrypsin": (1, 1, "[WYFL]."),
			"proteinase-k": (1, 1, "..")
		}

		self.protease = (proteases[digestEnzyme][0]*-3, (proteases[digestEnzyme][1])*3, re.compile(proteases[digestEnzyme][2]))
		self.digestEnzyme = digestEnzyme

		#misc regexp
		self.startPattern = re.compile("|".join([x[0] + "(?=" + x[1:] + ")" for x in self.getStartCodons()])) #finds all start codons in all forward frames of a given sequence
		self.upstreamPattern = re.compile('(' + "|".join(self.getStopCodons()) + ')(?=((?:(?!' + "|".join(self.getStopCodons()) + ')...)+$))') # finds a subsequence starting immediatly after the last inframe stop codon to the end of a given sequence
		self.downstreamPattern = re.compile('(?=((?:(?!' + "|".join(self.getStopCodons()) + ')...)+(?:' + "|".join(self.getStopCodons()) + ')))') #finds a subsequence starting from the beginning of a given sequence to the first inframe start codon; use it with re.match only!
		self.rbsPattern = re.compile('(?=(.G[AT]G.))') #finds shine dalgarno candidates

		#storing backmap results
		self.mappings = defaultdict(list)
		self.pep2map = defaultdict(set)
		self.unmatched = 0

		#analysis
		self.overlaps = defaultdict(list) #key: mapID; values: (refgenes list index, ovlen, relovlen, ovtype)
		self.syntenies = defaultdict(set)
		self.mapping2orf = defaultdict(set)
		self.orfs = {} #key: strand_coord_code; values: (seq, start, end, strand, len, aa, upstream seq, [rbs seq, rbs class, spacer class, spacer len] | None, annotated)
		self.detect_all_orfs = False
		self.evFiles = []
		self.evFileGroups = []
		self.matchTypes = {} # key: pepIDs
		self.seqCollections = []
		self.dnalookups = {}
		self.peplookups = {}
		self.startcodonScores = defaultdict(lambda: 3)
		for codon in ['ATG', 'TTG', 'GTG']:
			self.startcodonScores[codon] = 1
		for codon in ['CTG', 'ATT', 'ATC', 'ATA']:
			self.startcodonScores[codon] = 2

		#report
		self.navalue = "-"

	def iterAllPeps(self):
		'''
		generator returning all considered peptides as a set of peptide id and aa sequence
		return values are sorted by peptide id
		'''
		for seq, pepid in [(pepseq, self.pepids[pepseq]) for pepseq in sorted(self.pepids, key=self.pepids.get)]:
			yield pepid, seq

	def logParameter(self):
		'''
		returns a list of lines to log parameter
		'''
		log = []
		log.append('# reference sequence for peptide mapping (file name): ' + self.refFilename)
		log.append('# reference sequence for peptide mapping (accession): ' + self.refid)
		log.append('# reference sequence for peptide mapping (description): ' + self.refheader)
		log.append('# reference sequence for peptide mapping (length): ' + str(self.reflen))
		log.append('# reference sequence for peptide mapping (number genes): ' + str(len(self.refgenes)))
		log.append('# genetic code for peptide mapping: NCBI translation table ' + str(self.translTableID))
		log.append('# orf class 1: potential RBS and classical start codon (ATG, TTG, CTG, GTG)')
		log.append('# orf class 2: potential RBS and alternative start codon (ATT, ATC, ATA)' )
		log.append('# orf class 3: potential RBS but no classical or alternative start codon')
		log.append('# orf class 4: no potential RBS but classical start codon')
		log.append('# orf class 5: no potential RBS but alternative start codon')
		log.append('# orf class 6: no potential RBS and no alternative start codon')
		log.append('# enzyme: ' + self.digestEnzyme)

		return log

	def multiprocessor(self, worker, jobs, msg = False):
		#config
		cpus = self.cpus

		#build queues and processes
		input_queue = multiprocessing.Queue()
		output_queue = multiprocessing.Queue()
		for x in jobs:
			input_queue.put(x)
		for x in range(cpus):
			input_queue.put('{{QUEUE_END}}')
		processes = [multiprocessing.Process(target=worker, args=(input_queue, output_queue, )) for i in range(cpus)]

		#start processes
		for process in processes:
			process.start()

		#collate job outputs
		outputs = []
		completed = 0
		l = 0
		while completed < cpus:
			out = output_queue.get()
			if out == "{{JOB_END}}":
				completed += 1
			else:
				outputs.append(out)
				l += 1
				if msg:
					print(msg % l, end ="\r")

		#joining
		i = 0
		for process in processes:
			i += 1
			process.join()

		#messsaging
		if msg:
			print()

		return outputs

	def createReverseTable(self, direction="fwd"):
		'''
			provides a "reversed" translation table as dict with aa as keys and list of translated aa as respective value
			uses the translation table defined at init

			Input:
			direction	if 'fwd', genuine codons are used
						if 'rev', reverse complementary codons are used

			Output:
			dict with aa as keys and list of translated aa as respective value

			Test:
			>>> gb_record = SeqRecord(Seq("ATGTTTGTTGCAACTTAG"), id="TestSeq")
			>>> mapper = pepper(reffilename=False, gb_record=gb_record, tt = 11)
			>>> sorted(mapper.reversedTableFwd['F'])
			['TTC', 'TTT']
			>>> sorted(mapper.reversedTableRev['F'])
			['AAA', 'GAA']
		'''
		table={}
		for codon, aa in self.translTable.items():
			if not aa in table:
				table[aa] = []
			if direction == "fwd":
				table[aa].append(codon)
			elif direction == "rev":
				table[aa].append(pepper.revCompl(codon))
			else:
				exit("ERROR: direction for translation table is not valid.")
		return table

	def getTranslationTable(self):
		'''
		returns translation table based on NCBI's numbering
		'''
		tables = {}
		tables[11] = {'TTT':  'F', 'TTC':  'F', 'TTA':  'L', 'TTG':  'L', 'CTT':  'L', 'CTC':  'L', 'CTA':  'L', 'CTG':  'L', 'ATT':  'I', 'ATC':  'I', 'ATA':  'I', 'ATG':  'M', 'GTT':  'V', 'GTC':  'V', 'GTA':  'V', 'GTG':  'V', 'TCT':  'S', 'TCC':  'S', 'TCA':  'S', 'TCG':  'S', 'CCT':  'P', 'CCC':  'P', 'CCA':  'P', 'CCG':  'P', 'ACT':  'T', 'ACC':  'T', 'ACA':  'T', 'ACG':  'T', 'GCT':  'A', 'GCC':  'A', 'GCA':  'A', 'GCG':  'A', 'TAT':  'Y', 'TAC':  'Y', 'TAA': '*', 'TAG': '*', 'CAT':  'H', 'CAC':  'H', 'CAA':  'Q', 'CAG':  'Q', 'AAT':  'N', 'AAC':  'N', 'AAA':  'K', 'AAG':  'K', 'GAT':  'D', 'GAC':  'D', 'GAA':  'E', 'GAG':  'E', 'TGT':  'C', 'TGC':  'C', 'TGA':  '*', 'TGG':  'W', 'CGT':  'R', 'CGC':  'R', 'CGA':  'R', 'CGG':  'R', 'AGT':  'S', 'AGC':  'S', 'AGA':  'R', 'AGG':  'R', 'GGT':  'G', 'GGC':  'G', 'GGA':  'G', 'GGG':  'G'}
		if not self.translTableID in tables:
			exit("ERROR: Translation table " + str(self.translTableID) + " not implemented yet")
		return tables[self.translTableID]

	def getStartCodons(self):
		'''
		returns possible start codons listed in NCBI's translation table
		'''
		codons = {}
		codons[11] = ['TTG', 'CTG', 'ATT', 'ATC', 'ATA', 'ATG', 'GTG']
		if not self.translTableID in codons:
			exit("ERROR: table " + str(self.translTableID) + " for initiation codons not implemented yet")
		return set(codons[self.translTableID])

	def getStopCodons(self):
		'''
		returns possible stop codons listed in NCBI's translation table
		'''
		codons = {}
		codons[11] = ['TAA', 'TAG', 'TGA']
		if not self.translTableID in codons:
			exit("ERROR: table " + str(self.translTableID) + " for stop codons not implemented yet")
		return set(codons[self.translTableID])

	def pep2RegExp(self, pep, strand="+"):
		'''
		returns a regexp describing a given aa sequence in degenerated DNA code
		importantly, the regexp is based on look-behinds to allow overlapping matches.
		Thus, each match contains only the first codon of the matching DNA region.

		Input:
		pep			petide sequence
		strand 		can be '+' or '-'. If '-' is selected, regexp is based on reverse complement codons
		'''
		if strand == "+":
			reversedTable = self.reversedTableFwd
			j = 1
		elif strand == "-":
			reversedTable = self.reversedTableRev
			pep = pep[::-1]
			j = len(pep)
		else:
			exit("ERROR: direction for translation table is not valid.")

		regexp = []
		i = 0
		for aa in pep:
			i+= 1
			if i == j and aa == "M":
				aas = []
				for aa in reversedTable.keys():
					if aa != "*":
						aas.extend(reversedTable[aa])
				regexp.append("(?:(?:" + ")|(?:".join(aas) + "))")
			elif len(reversedTable[aa]) == 0:
				exit("ERROR: unknown amino acid '" + aa + "'")
			elif len(reversedTable[aa]) == 1:
				regexp.append(reversedTable[aa][0])
			else:
				regexp.append("(?:(?:" + ")|(?:".join(reversedTable[aa]) + "))")

		return re.compile("(" + regexp[0] + "(?=(" + "".join(regexp[1:]) + ")))")

	def correctRefPos(self, pos):
		'''
		corrects nucleotide positions (1-based) which were obtained from reference sequence multimers

		Input:
		pos		position within the reference sequence (1-based!)
		'''
		while pos > self.reflen:
			pos -= self.reflen
		while pos < 1:
			pos += self.reflen
		return pos

	def translateSeq(self, seq, acc=""):
		'''
		translates a DNA Sequence to an AA sequence
		Unkown codons (including ambigous signs) are translated as '?' (a note is shown on screen)
		stop codon is not translated
		'''
		aa = []
		i = 1
		for codon in [seq[i:i+3] for i in range(0, len(seq), 3)]:
			if codon not in self.translTable:
				aa.append("?")
				print("  Note: '" + codon + " at pos "  + str(i) + "' translated as ? in " + acc)
			else:
				aa.append(self.translTable[codon])
			i += 3

		return "".join(aa).strip("*")

	@staticmethod
	def revCompl(seq):
		'''
		returns a reverse complementary DNA sequence
		Please consider: Only ATGC can be handled

		Input:
		seq		DNA sequence

		Test:
		>>> pepper.revCompl("ATGC")
		'GCAT'
		'''
		return seq.lower().replace("a", "T").replace("t", "A").replace("c", "G").replace("g", "C")[::-1]

	def extendORF(self, pos, strand, alternativeStart = False):
		'''returns longest putative orfs for a given pos starting from the first in-frame stop codon (excluding)

		Input
		pos		position which has to be inframe (first nucleotide of a codon; 1-based)
				position counting refers to the forward strand
		strand	strand

		Output:
		(seq, startpos, endpos)

		Test:
		>>> gb_record = SeqRecord(Seq("ATGAAAATGTATGAAAAAAAATGAAAATATAAAAAAAATATAAAATGA"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.extendORF(7, "+")
		('ATGAAAATGTATGAAAAAAAATGA', 1, 24)

		>>> gb_record = SeqRecord(Seq("ATGAAAATGTATGAAAAAAAATGAAAATATAAAAAAAATATAAAA"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.extendORF(28, "+")
		('AAATATAAAAAAAATATAAAAATGAAAATGTATGAAAAAAAATGA', 25, 24)

		>>> gb_record = SeqRecord(Seq("TTTTATATTTTTTTTATATTTTCATTTTTTTTCATACATTTTCAT"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.extendORF(18, "-")
		('AAATATAAAAAAAATATAAAAATGAAAATGTATGAAAAAAAATGA', 21, 22)

		'''
		if strand == "-":
			pos = self.reflen - pos + 1
			refseq = self.revCompl(self.refseq)
		else:
			refseq = self.refseq

		seq = (refseq[pos-1:] + refseq[:pos-1]) * 4
		l = self.reflen*4
		start = self.upstreamPattern.search(seq)
		seq_cut = self.translateSeq(seq[self.protease[0]:]+seq[:self.protease[1]])
		protease_cut = self.protease[2].match(seq_cut)
		if self.translateSeq(seq[-3:]) == "M":
			upseq = seq[-3:]
			start_coord = self.correctRefPos(pos-3)
		elif start == None or alternativeStart or protease_cut == None:
			upseq = ""
			start_coord = pos
		else:
			upseq = start.group(2)
			start_coord = self.correctRefPos(pos-(l-start.start()-2)+1)

		end = self.downstreamPattern.match(seq) #match looks from string start only
		if end == None:
			return None

		end_coord = self.correctRefPos(pos+end.end(1)-1)
		if strand == "-":
			start_coord = self.reflen - start_coord + 1
			end_coord = self.reflen - end_coord + 1

		return (upseq+end.group(1), start_coord, end_coord)


	def iterORFs(self, pos, strand, alternativeStart = False):
		'''
		generator of all possible orfs from first inframe upstream stop codon (exclusive) and first inframe downstream stop codon (inclusive).
		SubORFS are built by from each inframe (alternative) start codon upstream of the given pos
		Generator returns orf sorted by length (descending)

		Input
		pos					position which has to be inframe (first nucleotide of a codon; 1-based)
							position counting refers to the forward strand
		strand				strand
		alternativeStart	if True the start site is covered by an peptide indicating an alternative N-terminal start (M in peptide, but not M encoded by DNA)

		Test:
		>>> gb_record = SeqRecord(Seq("ATGAAAATGTATGAAAAAAAATGAAAATATAAAAAAAATATAAAA"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> for orf in mapper.iterORFs(28, "+", False):
		...		print(orf)
		('AAATATAAAAAAAATATAAAAATGAAAATGTATGAAAAAAAATGA', 25, 24)
		>>> gb_record = SeqRecord(Seq("ATGAAAATGTATGAAAAAAAATGAAAATATAAAAAAAATATAAAA"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> for orf in mapper.iterORFs(28, "+", True):
		...		print(orf)
		('TATAAAAAAAATATAAAAATGAAAATGTATGAAAAAAAATGA', 28, 24)
		>>> gb_record = SeqRecord(Seq("TTTTATATTTTTTTTATATTTTCATTTTTTTTCATACATTTTCAT"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> for orf in mapper.iterORFs(25, "-", False):
		...		print(orf)
		('AAAAAAATGAAAATATAA', 31, 14)
		('ATGAAAATATAA', 25, 14)
		'''

		region = self.extendORF(pos, strand, alternativeStart)
		if region:
			yield region
			if strand == "-":
				v = -1
				upstrseq = region[0][:region[1]-pos+3]
			else:
				v = 1
				upstrseq = region[0][:pos-region[1]+3]
			for match in self.startPattern.finditer(upstrseq):
				if (match.start()+1)%3 == 1 and match.start() > 0:
					yield (region[0][match.start():], region[1] + v*match.start(), region[2])

	def mapping(self, pepseq):
		'''
		mapping peptides to a degenerated DNA sequence (reference sequence) using regular expressions

		Input:
		pepseq		petide sequence to map

		Output:
		List of DNA matches represented by dicts with keys
			strand		strand of DNA match ('+' or '-')
			start		start of DNA match (1-based)
			end			end of DNA match (1-based)
			seq			sequence of DNA match

		Test:
		>>> gb_record = SeqRecord(Seq("GCCAGGATAACAGCGCGAATTACT"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> for match in mapper.mapping('ARITAR'):
		... 	print(sorted(match.items()))
		[('end', 18), ('frame', '+1'), ('seq', 'GCCAGGATAACAGCGCGA'), ('start', 1), ('strand', '+')]
		[('end', 6), ('frame', '+1'), ('seq', 'GCGCGAATTACTGCCAGG'), ('start', 13), ('strand', '+')]


		>>> gb_record = SeqRecord(Seq("AGTAATTCGCGCTGTTATCCTGGC"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> for match in mapper.mapping('ARITAR'):
		... 	print(sorted(match.items()))
		[('end', 7), ('frame', '-1'), ('seq', 'GCCAGGATAACAGCGCGA'), ('start', 24), ('strand', '-')]
		[('end', 19), ('frame', '-1'), ('seq', 'GCGCGAATTACTGCCAGG'), ('start', 12), ('strand', '-')]

		'''

		backmapping = []
		l = len(pepseq)*3
		if l > self.reflen:
			return []

		for strand in ['+', '-']:
			if strand == "+":
				refseq = self.refseq + self.refseq[:l-1]
			else:
				refseq = self.refseq[self.reflen - l - 1:] + self.refseq
			refseq = self.refseq + self.refseq[:l-1]
			pattern=self.pep2RegExp(pepseq, strand)
			for match in pattern.finditer(refseq):
				data = {}
				data['strand'] = strand
				x = match.start()
				y = x + l
				if strand == "+":
					data['start'] = self.correctRefPos(x + 1)
					data['end'] = self.correctRefPos(y)
				else:
					data['start'] = self.correctRefPos(y)
					data['end'] = self.correctRefPos(x + 1)
				data['seq'] = refseq[x:y]
				if strand == "-":
					data['seq'] = self.revCompl(data['seq'])
				data['frame'] = self.getFrame(data['start'], strand)
				backmapping.append(data)
		if len(backmapping) == 0:
			data = {}
			data['strand'] = None
			data['start'] = None
			data['end'] = None
			data['frame'] = None
			data['seq'] = None
			backmapping.append(data)
		return backmapping

	def getFrame(self, startpos, strand):
		'''
		returns the frame based on gene start

		Input:
		startpos	start position of gene (1-based)
		strand		strand ('+' or '-')

		Output:
		+1, +2, +3, -1, -2 or -3

		Test:
		>>> gb_record = SeqRecord(Seq("AAAAAAAAAAA"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.getFrame(4, '+')
		'+1'
		>>> mapper.getFrame(2, '+')
		'+2'
		>>> mapper.getFrame(3, '+')
		'+3'
		>>> mapper.getFrame(11, '-')
		'-1'
		>>> mapper.getFrame(10, '-')
		'-2'
		>>> mapper.getFrame(9, '-')
		'-3'

		'''

		if strand == "+":
			modulon = startpos % 3
		else:
			modulon = (self.reflen-startpos+1) % 3

		if modulon == 0:
			return strand + str(3)
		elif modulon == 1:
			return strand + str(1)
		elif modulon == 2:
			return strand + str(2)

	def getMatches(self, pepseq):
		'''
		returns list of matching DNA regions and its properties for a given peptide sequence
		Please consider that respective peptide(s) has (have) to be mapped first using pepper.mapPeps()

		Input:
		pepseq

		Output:
		list of matches each represented as a dict with the keys:
			seq = matched DNA seq
			start = start in genome (1-based)
			end = end in genome (1-based; in case of reverse mapping end is lower than start)
			strand = strand [+|-]
			frame = frame (z.B. +3)

		Test:
		>>> gb_record = SeqRecord(Seq("ATGTTTGTTGCAACTTAG"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.addPep("FVAT")
		>>> mapper.mapPeps()
		>>> for match in mapper.getMatches("FVAT"):
		... 	sorted(match.items())
		[('end', 15), ('frame', '+1'), ('len', 12), ('pepseq', 'FVAT'), ('seq', 'TTTGTTGCAACT'), ('start', 4), ('strand', '+')]

		>>> gb_record = SeqRecord(Seq("AACTTAGATGTTTGTTGC"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.addPep("FVAT")
		>>> mapper.mapPeps()
		>>> for match in mapper.getMatches("FVAT"):
		... 	sorted(match.items())
		[('end', 4), ('frame', '+2'), ('len', 12), ('pepseq', 'FVAT'), ('seq', 'TTTGTTGCAACT'), ('start', 11), ('strand', '+')]

		>>> gb_record = SeqRecord(Seq("ATCTAAGTTGCAACAAAC"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.addPep("FVAT")
		>>> mapper.mapPeps()
		>>> for match in mapper.getMatches("FVAT"):
		... 	sorted(match.items())
		[('end', 6), ('frame', '-2'), ('len', 12), ('pepseq', 'FVAT'), ('seq', 'TTTGTTGCAACT'), ('start', 17), ('strand', '-')]

		'''
		return [self.mappings[x] for x in self.pep2map[pepseq]]

	def getOverlapLen(self, min1, max1, min2, max2):
		'''
		returns overlap of two ranges defined by mins and maxs
		ring closure is not considered
		'''
		return max(0, min(max1, max2) - max(min1, min2) + 1) #ring closure not considered

	def mapWorker(self, input_queue, output_queue):
		'''
		performs mapping of peptide and result processing/storing
		Each match is stored as dict with the keys:
			seq = matched DNA seq
			start = start in genome (1-based)
			end = end in genome (1-based; in case of reverse mapping end is lower than start)
			strand = strand [+|-]
			frame = frame (z.B. +3)

		Input:
		pepseq		sequence of peptide
		'''
		for pepseq in iter(input_queue.get, '{{QUEUE_END}}'):
			out = (pepseq, self.mapping(pepseq))
			output_queue.put(out)
		output_queue.put("{{JOB_END}}")

	def getPepID(self, pepseq):
		'''
		return peptide id based on aa sequences
		'''
		if pepseq in self.pepids:
			return self.pepids[pepseq]
		else:
			return False

	def delPep(self, pepseq):
		'''
		deletes peptide
		'''
		if pepseq in self.pepids:
			del self.pepids[pepseq]
			for mapID in [x for x in self.mappings if self.mappings[x]['pepseq'] == pepseq]:
				del(self.mappings[mapID])

	def getPeps(self):
		'''
		returns unsorted list ofall stored peptide ids
		'''
		return list(self.pepids.keys())

	def mapPeps(self):
		'''
		processes  multiple peptide sequences to mapping
		'''
		if self.silent:
			msg = False
		else:
			msg = "  mapping peptides... [%s/" + str(len(self.pepids)) + "]"

		mapID = 0
		for pepseq, pepmappings in self.multiprocessor(self.mapWorker, self.pepids.keys(), msg):
			if len(pepmappings) > 1:
				self.not_uniq += 1
				if self.uniq_only:
					self.delPep(pepseq)
					continue

			pepID = self.getPepID(pepseq)
			for mapping in pepmappings:
				mapID += 1
				if not mapping['seq']:
					mapping['len'] = None
					self.unmatched += 1
				else:
					mapping['len'] = len(mapping['seq'])
				mapping['pepseq'] = pepseq
				self.mappings[mapID] = mapping
				self.pep2map[pepseq].add(mapID)

		return

	def addRefGenes(self, gb_record):
		'''
		processing of all 'CDS' features in a Genbank record (biopython)
		CDS features are stored in a list.
		For each CDS feature a sublist is generated according to
		[l+i, start, end, strand, frame, dna, aa, locus]
		where, importantly, start position is always < end position
		start and end positions are 1-based

		Input:
		gb_record 	Genbank record (biopython)

		Output:
		list of sublists

		'''
		genes = []
		l = len(self.refgenes)
		i = -1
		for feature in gb_record.features:
			if feature.type != "CDS" or "pseudo" in feature.qualifiers:
				continue

			i += 1
			start = int(feature.location.start)+1
			end = int(feature.location.end)
			locus = feature.qualifiers['locus_tag'][0]
			dna = str(feature.extract(gb_record.seq)).upper()
			aa = self.translateSeq(dna, locus)

			if feature.location.strand == 1:
				strand = "+"
				frame = self.getFrame(start, strand)
			else:
				strand = "-"
				frame = self.getFrame(end, strand)

			genes.append((l+i, start, end, strand, frame, dna, aa, locus))

		genes.sort()

		return genes

	def getRefGeneStarts(self):
		'''
		returns start coordinates of all CDS extracted from reference GBK file
		'''
		return [x[1] for x in self.refgenes]

	def getRefProducts(self, gb_record):
		'''
		extracts product information from GBK entry
		'''
		products = {}
		for feature in gb_record.features:
			if feature.type != "CDS":
				continue

			locus = feature.qualifiers['locus_tag'][0]
			product = feature.qualifiers['product'][0]
			if "gene" in feature.qualifiers:
				product += " [" + feature.qualifiers['gene'][0] + "]"
			products[locus] = product

		return products

	def getUpstreamGene(self, start):
		'''
		returns the closest upstream gene of the reference annotation
		'''
		key = 0
		for gene in selfgenes:
			if start - gene[1] <= 0:
				break
			else:
				key += 1

		return gene[key]

	def getUpstreamRegion(self, pos, strand, length=21):
		'''
		Upstream region is returned to a given position (1-based) based on the reference sequence
		circularity of reference sequence is considered

		Input:
		pos 		position which marks the first (plus strand) or last (minus strand) of the upstream region (1-based)
		strand		'+' or '-'. If '-', upstream region is returend in reverse complement
		length		length of upstream region (default: 21)

		Output:
		upstream sequence

		Test:
		>>> gb_record = SeqRecord(Seq("AAATTTGGGCCC"), id="TestSeq")
		>>> mapper = pepper(reffilename=False, gb_record=gb_record)
		>>> mapper.getUpstreamRegion(9, '+', 6)
		'TTTGGG'
		>>> mapper.getUpstreamRegion(1, '+', 6)
		'GGCCCA'
		>>> mapper.getUpstreamRegion(10, '-', 6)
		'TTTGGG'
		>>> mapper.getUpstreamRegion(6, '-', 6)
		'GGCCCA'

		'''
		seq = self.refseq * 3
		pos = self.reflen + pos
		if strand == "+":
			return seq[pos-length:pos]
		elif strand == "-":
			return self.revCompl(seq[pos-1:pos+length-1])

	@staticmethod
	def drawGene(gene):
		'''
		provides a strand-specific text representation of a gene
		examples:
		name>	gene on forward strand
		<name	gene on reverse strand
		'''
		if gene[3] == "+":
			return gene[7] + ">"
		elif gene[3] == "-":
			return "<" + gene[7]
		else:
			return gene[7]

	def getGeneGraph(self, start, end, strand, name, l = 4):
		'''
		returns a text-based visualization of gene synteny
		'''

		#intergenic
		key = bisect.bisect_right(self.refgenestarts, start) + len(self.refgenes)

		if int(l)%2 > 0:
			l += 1
		l = int(l/2)
		elements = []
		annotations = []
		genecount = len(self.refgenes)
		refgenes = self.refgenes + self.refgenes + self.refgenes

		synteny = refgenes[key-l:key+l]
		synteny = [list(x) for x in synteny]

		ovlp = False
		i = -1
		for refgene in synteny:
			i += 1
			if sorted([start, end]) == sorted([refgene[1], refgene[2]]) and  strand == refgene[3]:
				synteny[i][-1] += "/" + name
				ovlp = True
		if not ovlp:
			synteny.insert(l, [None, min(start, end), max(start, end), strand, None, None, None, name])


		#collect annotation of products
		i = -1
		for genedata in synteny:
			i += 1
			gene = genedata[7].split("/")[0]
			if gene == name:
				continue
			if gene in self.refproducts:
				annotations.append(gene + ": " + self.refproducts[gene].replace(";", ""))
			else:
				annotations.append(gene + ":-")

		annotations = "; ".join(annotations)

		#"drawing"
		graph = "-"
		i = 0
		for gene in synteny:
			i += 1
			if i == len(synteny):
				d = "-"
			else:
				dist = synteny[i][1] - gene[2] - 1
				d = "-[" + str(dist) + "bp]-"
				if i == l:
					updist = dist
				elif i == l + 1:
					downdist = dist
			graph += "-" + pepper.drawGene(gene) + "-" + d

		if strand == "-":
			updist, downdist = downdist, updist

		return (graph, annotations, updist, downdist)

	def getOverlappingGenes(self, start, end):
		'''
		returns list of  reference genes overlapping with the given gene defined by start and end
		start must be smaller then end independently of the strand
		'''
		return [x for x in self.refgenes if self.getOverlapLen(min(x[1], x[2]), max(x[1], x[2]), min(start, end), max(start, end)) > 0]

	def addEvFile(self, filename, perrp = 0.1, msms = 1, score = 40, minint = 0.01, group=False):
		'''
		adds information of an evidence file
		order of adding defines order of respective report columns
		'''
		if not self.silent:
			print("processing evidence file " + filename + "...")

		#create obj
		objid = len(self.evFiles) + 1
		self.evFiles.append(evidenceFile(filename, perrp, msms, score, minint, id=objid, group=group))

		#give pepids (if not already given)
		for pepseq in self.evFiles[-1].peps:
			self.addPep(pepseq)

		if not self.silent:
			print("  " + str(len(self.evFiles[-1].peps)) + " unique peptides found")

	def addPep(self, pepseq):
		if not pepseq in self.pepids:
			self.pepids[pepseq] = len(self.pepids) + 1

	def addGbRecord(self, record):
		'''
		adds a GBK record
		'''
		if not self.silent:
			print("processing genbank record %s" % record.id)
		self.refid = record.id
		self.refheader = record.description
		self.refseq = str(record.seq).upper().strip()
		self.reflen = len(self.refseq)
		self.refgenes = self.addRefGenes(record) # must be sorted regarding gene starts!
		self.refgenestarts = self.getRefGeneStarts()
		self.refproducts = self.getRefProducts(record)

	def addRef(self, gbkfilename):
		'''
		adds reference sequence and annotation provided as GBK file
		'''
		if not self.silent:
			print("processing genbank file " + gbkfilename + "...")
		self.refrecord = SeqIO.read(gbkfilename, "genbank")
		self.addGbRecord(self.refrecord)

	def addSeqCollection(self, filename, descr):
		'''
		adds a collection of DNA sequences for peptide and DNA match lookups
		order of adding defines order of respective report columns
		'''
		if not self.silent:
			print("processing sequence collection " + filename + "...")

		#create obj
		objid = len(self.seqCollections) + 1
		self.seqCollections.append(seqCollection(filename, descr = descr, id=objid, cpus=self.cpus, silent=self.silent))

	def provideMappingInfo(self, mapID):
		'''
		returns list of mapping informations:
		DNA sequence, start, end, strand, frame, aa sequence, DNA sequence length
		'''
		mapping = self.mappings[mapID]
		return mapping['seq'], mapping['start'], mapping['end'], mapping['strand'], mapping['frame'], mapping['pepseq'], mapping['len']

	def findRbs(self, seq, mismatches = 2, idealRbs = 'GGAGG'):
		'''
		finds putative RBS sequences in a given sequence
		'''
		rbs = []
		seqlen = len(seq)
		rbslen = len(idealRbs)
		for match in self.rbsPattern.finditer(seq):
			s = str(match.group(1))
			m = sum([1 for i in range(len(s)) if s[i] != idealRbs[i]])

			#rbs classification
			rbsclass = m+1

			#spacer classification
			spacerlen = seqlen - match.start()  - rbslen
			if spacerlen > 2:
				spacerclass = 1
			else:
				spacerclass = 2

			if m <= mismatches:
				rbs.append([rbsclass, spacerclass, spacerlen, s])

		if len(rbs) == 0:
			return [None, None, None, None]

		return sorted(rbs, key = lambda x: (x[0], x[1], abs(3-spacerlen)))[0]

	def overlapWorker(self, input_queue, output_queue):
		for mapID in iter(input_queue.get, '{{QUEUE_END}}'):
			mapInfo = self.provideMappingInfo(mapID)
			if not mapInfo[0]:
				continue
			mincoord = min(mapInfo[1], mapInfo[2])
			maxcoord = max(mapInfo[1], mapInfo[2])
			ovs = []

			#overlapping genes
			for gene in self.getOverlappingGenes(mapInfo[1], mapInfo[2]):
				if gene[4] == mapInfo[4]:
					ovtype = 'inframe'
				else:
					ovtype = 'outframe'
				ovlen = self.getOverlapLen(gene[1], gene[2], mincoord, maxcoord)
				relovlen = int(round(ovlen/mapInfo[6], 2)*100)
				ovs.append((gene[0], ovlen, relovlen, ovtype))
			output_queue.put((mapID, ovs))
		output_queue.put("{{JOB_END}}")

	def orfOverlapWorker(self, input_queue, output_queue):
		for orfID in iter(input_queue.get, '{{QUEUE_END}}'):
			orf = self.orfs[orfID]
			start = orf['start']
			end = orf['end']
			strand = orf['strand']
			frame = orf['frame']
			ovs = []
			#overlapping genes
			for gene in self.getOverlappingGenes(start, end):
				if gene[4] == frame:
					ovtype = 'inframe'
				else:
					ovtype = 'outframe'
				ovs.append((gene[0], ovtype, gene[3]))
			output_queue.put((orfID, ovs))
		output_queue.put("{{JOB_END}}")

	def syntenyWorker(self, input_queue, output_queue):
		for mapID in iter(input_queue.get, '{{QUEUE_END}}'):
			seq, start, end, strand, frame, pepseq, length = self.provideMappingInfo(mapID)
			if not seq:
				continue
			pepID = self.getPepID(pepseq)
			data = {}
			data['graph'], data['annotation'], data['updist'], data['downdist'] = self.getGeneGraph(start, end, strand, "PEP" + str(pepID))
			output_queue.put((mapID, data))
		output_queue.put("{{JOB_END}}")

	def orfWorker(self, input_queue, output_queue):
		for mapID in iter(input_queue.get, '{{QUEUE_END}}'):
			seq, start, end, strand, frame, pepseq, length = self.provideMappingInfo(mapID)
			if not seq:
				continue

			orfs = set()

			#check for alternative start
			if self.translTable[seq[:3]] != pepseq[0]:
				alternativeStart = True
			else:
				alternativeStart = False

			#peptide in annotated region
			annotated = "+"
			annotated_orfs = set()
			for overlap in self.overlaps[mapID]:
				refgene = self.refgenes[overlap[0]]
				ostart  = refgene[1]
				if ostart != start and alternativeStart:
					newbeginning = True
				else:
					newbeginning = False
				if overlap[3] == "inframe" and overlap[2] == 100 and not newbeginning:
					oend  = refgene[2]
					strand = refgene[3]
					dna = refgene[5]
					aa = refgene[6]
					if strand == "+":
						genestart = min(ostart, oend) - 1
						frame = self.getFrame(genestart+1, strand)
					else:
						genestart = max(ostart, oend) + 1
						frame = self.getFrame(genestart-1, strand)
					upstream = self.getUpstreamRegion(genestart, strand, 21)
					rbsdata  = self.findRbs(upstream)
					rbs_class = rbsdata[0]
					spacer_class = rbsdata[1]
					spacerlen = rbsdata[2]
					rbs = rbsdata[3]

					orfs.add((dna, ostart, oend, strand, len(dna), aa, upstream, rbs_class, spacer_class, spacerlen, rbs, annotated, frame))
					annotated_orfs.add(str(min(ostart, oend)) + " " + str(max(ostart, oend)) + " " + str(strand))

			#"intergenic peptide"
			if len(orfs) == 0 or self.generic_orf_prediction == True:
				annotated = "-"
				for oseq, ostart, oend in self.iterORFs(start, strand, alternativeStart):
					if str(min(ostart, oend)) + " " + str(max(ostart, oend)) + " " + str(strand) in annotated_orfs:
						continue
					if strand == "+":
						genestart = min(ostart, oend) - 1
						frame = self.getFrame(genestart+1, strand)
					else:
						genestart = max(ostart, oend) + 1
						frame = self.getFrame(genestart-1, strand)
					upstream = self.getUpstreamRegion(genestart, strand, 21)
					rbsdata  = self.findRbs(upstream)
					rbs_class = rbsdata[0]
					spacer_class = rbsdata[1]
					spacerlen = rbsdata[2]
					rbs = rbsdata[3]
					aa = self.translateSeq(oseq)
					orfs.add((oseq, ostart, oend, strand, len(oseq), aa, upstream, rbs_class, spacer_class, spacerlen, rbs, annotated, frame))

			output_queue.put((mapID, orfs))
		output_queue.put("{{JOB_END}}")

	def classifyOrfs(self):

		for key, orf in self.orfs.items():
			#ranking based on startcodons
			startcodon_class = self.startcodonScores[orf['seq'][:3]]

			#ranking based on rbs
			if not orf['rbs']:
				rbs_class = spacer_class  = None
			else:
				rbs_class = orf['rbs_class']
				spacer_class = orf['spacer_class']

			#orf class
			if not rbs_class:
				orf_class = startcodon_class + 18
			else:
				orf_class = rbs_class + 3*(startcodon_class-1) + 9*(spacer_class-1)

			self.orfs[key]['orf_class'] = orf_class
			self.orfs[key]['rbs_class'] = rbs_class
			self.orfs[key]['startcodon_class'] = startcodon_class
			self.orfs[key]['spacer_class'] = spacer_class

			#orf type
			if self.orfs[key]['annot'] == "+":
				self.orfs[key]['orf_type'] = "E"
			elif "inframe" in [x[1] for x in self.orfs[key]['overlaps']]:
				self.orfs[key]['orf_type'] = "D"
			elif ("outframe", orf['strand']) in [(x[1], x[2]) for x in self.orfs[key]['overlaps']]:
				self.orfs[key]['orf_type'] = "B"
			elif "outframe" in [x[1] for x in self.orfs[key]['overlaps']]:
				self.orfs[key]['orf_type'] = "C"
			else:
				self.orfs[key]['orf_type'] = "A"
		return

	def getOrfs(self):
		'''
		returns set of sequences of all orfs
		'''
		return set([x['seq'] for x in self.orfs.values()])

	def analyseMappings(self, generic_orf_prediction = False):
		self.detect_all_orfs = generic_orf_prediction

		#overlaps
		if self.silent:
			msg = False
		else:
			msg = "  regarding overlaps with annotated genes... [%s/" + str(len(self.mappings)) + "]"
		for mapID, overlaps in self.multiprocessor(self.overlapWorker, self.mappings.keys(), msg):
			for overlap in overlaps:
				self.overlaps[mapID].append(overlap)

		#synteny
		if self.silent:
			msg = False
		else:
			msg = "  regarding gene synteny... [%s/" + str(len(self.mappings)) + "]"
		for synteny in self.multiprocessor(self.syntenyWorker, self.mappings.keys(), msg):
			self.syntenies[synteny[0]] = synteny[1]

		#ORF detection
		if self.silent:
			msg = False
		else:
			msg = "  regarding potential ORFs... [%s/" + str(len(self.mappings)) + "]"

		self.generic_orf_prediction = generic_orf_prediction
		for mapID, orfs in self.multiprocessor(self.orfWorker, self.mappings.keys(), msg):
			for o in orfs:
				orf = {}
				orf['seq'] = o[0]
				orf['start'] = o[1]
				orf['end'] = o[2]
				orf['strand'] = o[3]
				orf['seqlen'] = o[4]
				orf['aa'] = o[5]
				orf['upstream'] = o[6]
				orf['rbs_class'] = o[7]
				orf['spacer_class'] = o[8]
				orf['spacerlen'] = o[9]
				orf['rbs'] = o[10]
				orf['annot'] = o[11]
				orf['frame'] = o[12]
				key = orf['strand'] + "_" + str(orf['start']) + "_" + str(orf['end'])
				if key not in self.orfs:
					self.orfs[key] = orf
				self.mapping2orf[mapID].add(key)

		##counting unique peptides of ORFs
		msg = "  counting unique peptides per ORF ... [%s/" + str(len(self.orfs)) + "]"

		pep2orfs = {}
		for pepseq in self.pep2map:
			#if len(self.pep2map[pepseq]) != 1:
				#continue
			pep2orfs[pepseq] = defaultdict(set)
			for mapID in self.pep2map[pepseq]:
				for o in self.mapping2orf[mapID]:
					s = self.orfs[o]['start']
					e = self.orfs[o]['end']
					pep2orfs[pepseq][max(s, e)].add(min(s, e))

		i = 1
		for key, orf in self.orfs.items():
			if not self.silent:
				print(msg % i, end ="\r")
			i += 1
			upep = 0
			s = min(orf['start'], orf['end'])
			e = max(orf['start'], orf['end'])
			for pepseq in pep2orfs:
				if e in pep2orfs[pepseq] and any([True for x in pep2orfs[pepseq][e] if x >= s]):
					upep += 1
			self.orfs[key]['upep'] = upep
		print()


		##ORF overlaps
		if self.silent:
			msg = False
		else:
			msg = "  check overlaps of potential ORFs with annotated genes... [%s/" + str(len(self.orfs)) + "]"

		for orfID, overlaps in self.multiprocessor(self.orfOverlapWorker, self.orfs.keys(), msg):
			self.orfs[orfID]['overlaps'] = overlaps

		##ORF scoring
		if self.silent:
			msg = False
		else:
			msg = "  scoring potentials ORFs... "
		self.classifyOrfs()


		#lookup in seq collections
		##pep lookup
		seqs = set(self.getPeps() + [x['aa'] for x in self.orfs.values()])
		for seqCollection in self.seqCollections:
			colID = seqCollection.getCollectionId()
			self.peplookups[colID] = seqCollection.seqsWithPeps(seqs)

		##dna lookup
		seqs = set([x['seq'] for x in self.mappings.values() if x['seq']] + list(self.getOrfs()))
		for seqCollection in self.seqCollections:
			colID = seqCollection.getCollectionId()
			self.dnalookups[colID] = seqCollection.seqsWithSubseqs(seqs)
		return

	def smartUniqPepFilter(self):
		self.uniq_only_smart = True
		orfs = defaultdict(set)
		for mapID in self.mappings:
			pepseq = self.mappings[mapID]['pepseq']
			if self.mapping2orf[mapID]:
				bestorf = self.getBestOrf([self.orfs[x] for x in self.mapping2orf[mapID]])
			orfs[pepseq].add(bestorf['aa'])

		for pepseq in orfs:
			sorted_orfs = sorted(orfs[pepseq], key=len)
			longest = sorted_orfs.pop()
			while sorted_orfs:
				if sorted_orfs.pop() not in longest:
					self.delPep(pepseq)
					self.not_uniq_smart += 1
					break
		return orfs.keys()

	def getBestOrf(self, orfs):
		if not self.detect_all_orfs:
			return sorted(orfs, key=lambda x: (x['annot'], -x['upep'], x['orf_class'], -x['seqlen']))[0]
		else:
			return sorted(orfs, key=lambda x: (-x['upep'], x['orf_class'], -x['seqlen']))[0]

	def resultWorker(self, input_queue, output_queue):
		refseq = self.refseq*3
		matched_starts = {}
		for x in self.mappings:
			if self.mappings[x]['start']:
				matched_starts[self.mappings[x]['strand'] + str(self.mappings[x]['start'])] = self.mappings[x]['pepseq'][0]
		for mapID in iter(input_queue.get, '{{QUEUE_END}}'):
			data = {}
			mapping = self.mappings[mapID]
			synteny = self.syntenies[mapID]
			overlaps = sorted(self.overlaps[mapID])
			orfs = [self.orfs[x] for x in self.mapping2orf[mapID]]
			pepseq = mapping['pepseq']

			#peptide data
			data['PEPSEQ'] = pepseq
			data['PEPID'] = self.pepids[pepseq]
			if mapping['seq'] is None:
				data['DNAMATCHES'] = 0
			else:
				data['DNAMATCHES'] = len(self.getMatches(pepseq))
			try:
				data['PEPPI'] = round(ProteinAnalysis(pepseq).isoelectric_point(),2)
			except:
				data['PEPPI'] = "-"
			try:
				data['PEPMW'] = round(ProteinAnalysis(pepseq).molecular_weight(),2)
			except:
				data['PEPMW'] = "-"
			try:
				data['PEPGRAVY'] = round(ProteinAnalysis(pepseq).gravy(),2)
			except:
				data['PEPGRAVY'] = "-"

			#evidence file data
			data['EVPEPIDS'] = []
			data['EVPROTS'] = []
			data['EVSCOUNTS'] = []
			for evFile in self.evFiles:
				evpepid = evFile.getPepId(pepseq)
				if not evpepid:
					data['EVPEPIDS'].append(self.navalue)
				else:
					data['EVPEPIDS'].append(";".join(evpepid))
				prots = evFile.getPepProts(pepseq)
				if len(prots) == 0:
					data['EVPROTS'].append(self.navalue)
				else:
					data['EVPROTS'].append(";".join(prots))
				data['EVSCOUNTS'].append([])
				for exp in evFile.getExperiments():
					data['EVSCOUNTS'][-1].append(evFile.getSpectralCounts(pepseq, exp))

			#peptide lookup data
			data['PEPLOOKUPS'] = []
			for seqCollection in self.seqCollections:
				colID = seqCollection.getCollectionId()
				data['PEPLOOKUPS'].append(self.peplookups[colID][pepseq])

			if not mapping['seq'] is None:
				#mapping data
				data['MSEQ'] = mapping['seq']
				data['MLEN'] = mapping['len']
				data['MSTRAND'] = mapping['strand']
				data['MFRAME'] = mapping['frame']
				data['MSTART'] = mapping['start']
				data['MEND'] = mapping['end']
				if data['MSTART']:
					if mapping['strand'] == "-":
						data['MUPAA'] = self.translTable[self.revCompl(refseq[data['MSTART']:data['MSTART']+3])]
						data['MSTART'] , data['MEND'] = max(mapping['start'], mapping['end']), min(mapping['start'], mapping['end'])
					else:
						data['MUPAA'] = self.translTable[refseq[data['MSTART']-4:data['MSTART']-1]]
						data['MSTART'] , data['MEND'] = min(mapping['start'], mapping['end']), max(mapping['start'], mapping['end'])
				else:
					data['MUPAA'] = self.navalue

				#overlaps
				data['IFROVGENES'] = ";".join([self.refgenes[y][7] for y in [x[0] for x in overlaps if x[3] == 'inframe']])
				if len(data['IFROVGENES']) == 0:
					data['IFROVGENES'] = self.navalue
					data['IFROVLENS'] = self.navalue
					data['IFROVRELLENS'] = self.navalue
				else:
					data['IFROVLENS'] = ";".join([str(x[1]) for x in overlaps if x[3] == 'inframe'])
					data['IFROVRELLENS'] = ";".join([str(x[2]) for x in overlaps if x[3] == 'inframe'])

				data['OFROVGENES'] = ";".join([self.refgenes[y][7] for y in [x[0] for x in overlaps if x[3] == 'outframe']])
				if len(data['OFROVGENES']) == 0:
					data['OFROVGENES'] = self.navalue
					data['OFROVLENS'] = self.navalue
					data['OFROVRELLENS'] = self.navalue
				else:
					data['OFROVLENS'] = ";".join([str(x[1]) for x in overlaps if x[3] == 'outframe'])
					data['OFROVRELLENS'] = ";".join([str(x[2]) for x in overlaps if x[3] == 'outframe'])

				#synteny data
				data['SYNTENY'] = synteny['graph']
				data['ANNOTATION'] = synteny['annotation']
				data['UPDIST'] = synteny['updist']
				data['DOWNDIST'] = synteny['downdist']

				#mapping lookup
				data['MLOOKUPS'] = []
				for seqCollection in self.seqCollections:
					colID = seqCollection.getCollectionId()
					data['MLOOKUPS'].append(self.dnalookups[colID][mapping['seq']])

				#best orf data
				if len(orfs) > 0:
					bestorf = self.getBestOrf(orfs)
					if mapping['strand'] == "-":
						data['BOSTART'] , data['BOEND'] = max(bestorf['start'], bestorf['end']), min(bestorf['start'], bestorf['end'])
					else:
						data['BOSTART'] , data['BOEND'] = min(bestorf['start'], bestorf['end']), max(bestorf['start'], bestorf['end'])
					data['BOSYNTGRAPH'], data['BOSYNTANNOT'], data['BOSYNTUDIST'], data['BOSYNTDDIST'] = self.getGeneGraph(bestorf['start'], bestorf['end'], mapping['strand'], "ORF" + str(data['PEPID']))
					data['BOSEQ'] = bestorf['seq']
					data['BOANNOT'] = bestorf['annot']
					data['BOSTRAND'] = bestorf['strand']
					data['BOFRAME'] = bestorf['frame']
					data['BOLEN'] = bestorf['seqlen']
					data['BOAA'] = bestorf['aa']
					if data['BOSTRAND']  + str(data['BOSTART'])  in matched_starts:
						data['BOAA'] = matched_starts[data['BOSTRAND'] + str(data['BOSTART'])] + data['BOAA'][1:]
					elif data['BOAA'][0] != "M":
						data['BOAA'] = "[M/" + data['BOAA'][0] + "]" + data['BOAA'][1:]
					try:
						data['BOPI'] = round(ProteinAnalysis(bestorf['aa']).isoelectric_point(),2)
					except:
						data['BOPI'] = "-"
					try:
						data['BOMW'] = round(ProteinAnalysis(bestorf['aa']).molecular_weight(),2)
					except:
						data['BOMW'] = "-"
					try:
						data['BOGRAVY'] = round(ProteinAnalysis(bestorf['aa']).gravy(),2)
					except:
						data['BOGRAVY'] = "-"
					data['BOUPSTREAM'] = bestorf['upstream']
					if bestorf['rbs'] is not None:
						data['BORBS'] = bestorf['rbs']
						data['BORBSCLASS'] = bestorf['rbs_class']
						data['BOSPCLASS'] = bestorf['spacer_class']
						data['BOSPLEN'] = bestorf['spacerlen']
					else:
						data['BORBS'] = self.navalue
						data['BORBSCLASS'] = self.navalue
						data['BOSPCLASS'] = self.navalue
						data['BOSPLEN'] = self.navalue
					data['BOSCODON'] = bestorf['seq'][:3]
					data['BOUP'] = bestorf['upep']
					data['BOCLASS'] = bestorf['orf_class']
					data['BOTYPE'] = bestorf['orf_type']
					data['BOSTARTCLASS'] = bestorf['startcodon_class']
					data['BOIFROV'] = ";".join([self.refgenes[y][7] for y in [x[0] for x in bestorf['overlaps'] if x[1] == 'inframe']])
					if len(data['BOIFROV']) == 0:
						data['BOIFROV'] = self.navalue
					data['BOOFROV'] = ";".join([self.refgenes[y][7] for y in [x[0] for x in bestorf['overlaps'] if x[1] == 'outframe']])
					if len(data['BOOFROV']) == 0:
						data['BOOFROV'] = self.navalue
					data['BODNALOOKUPS'] = []
					data['BOPEPLOOKUPS'] = []
					for seqCollection in self.seqCollections:
						colID = seqCollection.getCollectionId()
						data['BODNALOOKUPS'].append(self.dnalookups[colID][bestorf['seq']])
						data['BOPEPLOOKUPS'].append(self.peplookups[colID][bestorf['aa']])

				#all orf data
				data['ORFCOUNT'] = len(orfs)
				if len(orfs) > 0:
					data['ORFS'] = []
					data['ORFCLASSES'] = set()
					for orf in sorted(orfs, key=lambda x: x['seqlen'], reverse=True):
						dat = {}
						if mapping['strand'] == "-":
							dat['OSTART'], dat['OEND'] = max(orf['start'], orf['end']), min(orf['start'], orf['end'])
						else:
							dat['OSTART'], dat['OEND'] = min(orf['start'], orf['end']), max(orf['start'], orf['end'])
						dat['OSEQ'] = orf['seq']
						dat['OANNOT'] = orf['annot']
						dat['OSTRAND'] = orf['strand']
						dat['OFRAME'] = orf['frame']
						dat['OLEN'] = orf['seqlen']
						dat['OAA'] = orf['aa']
						if dat['OSTRAND']  + str(dat['OSTART'])  in matched_starts:
							dat['OAA'] = matched_starts[dat['OSTRAND']  + str(dat['OSTART'])] + dat['OAA'][1:]
						elif dat['OAA'][0] != "M":
							dat['OAA'] = "[M/" + dat['OAA'][0] + "]" + dat['OAA'][1:]
						dat['OUPSTREAM'] = orf['upstream']
						if orf['rbs'] is not None:
							dat['ORBS'] = orf['rbs']
							dat['ORBSCLASS'] = orf['rbs_class']
							dat['OSPCLASS'] = orf['spacer_class']
							dat['OSPLEN'] = orf['spacerlen']
						else:
							dat['ORBS'] = self.navalue
							dat['ORBSCLASS'] = self.navalue
							dat['OSPCLASS'] = self.navalue
							dat['OSPLEN'] = self.navalue
						dat['OSCODON'] = orf['seq'][:3]
						dat['OUP'] = orf['upep']
						dat['OCLASS'] = orf['orf_class']
						dat['OTYPE'] = orf['orf_type']
						data['ORFCLASSES'].add(dat['OCLASS'])
						dat['OSTARTCLASS'] = orf['startcodon_class']
						dat['OIFROV'] = ";".join([self.refgenes[y][7] for y in [x[0] for x in orf['overlaps'] if x[1] == 'inframe']])
						if len(dat['OIFROV']) == 0:
							dat['OIFROV'] = self.navalue
						dat['OOFROV'] = ";".join([self.refgenes[y][7] for y in [x[0] for x in orf['overlaps'] if x[1] == 'outframe']])
						if len(dat['OOFROV']) == 0:
							dat['OOFROV'] = self.navalue
						dat['ODNALOOKUPS'] = []
						dat['OPEPLOOKUPS'] = []
						for seqCollection in self.seqCollections:
							colID = seqCollection.getCollectionId()
							dat['ODNALOOKUPS'].append(self.dnalookups[colID][orf['seq']])
							dat['OPEPLOOKUPS'].append(self.peplookups[colID][orf['aa']])
						data['ORFS'].append(dat)
					data['ORFCLASSES'] = "; ".join([str(x) for x in sorted(data['ORFCLASSES'])])

				#match status
				ovlens = set([x[2] for x in overlaps if x[3] == 'inframe'])
				if 100 in ovlens:
					data['MTYPE'] = (0, "annotation (peptide match fully overlapping with annotated CDS)")
				elif len(ovlens) > 0:
					data['MTYPE'] = (1, "extension I (peptide match partially and in-frame overlapping with annotated CDS)")
				elif dat['OIFROV'] != self.navalue:
					data['MTYPE'] = (2, "extension II (peptide match in-frame but not overlapping with annotated CDS)")
				elif dat['OOFROV'] != self.navalue:
					data['MTYPE'] = (3, "intergenic I (peptide match partially or fully out-frame overlapping with annotated CDS)")
				else:
					data['MTYPE'] = (4, "intergenic II (no overlap with annotated CDS)")
			output_queue.put(data)
		output_queue.put("{{JOB_END}}")

	def getResults(self):
		if self.silent:
			msg = False
		else:
			msg = "  processing results... [%s/" + str(len(self.mappings)) + "]"

		results = self.multiprocessor(self.resultWorker, self.mappings.keys(), msg)


		#posthoc analyses
		##preferred match type
		for i in range(len(results)):
			if 'MTYPE' not in results[i]:
				continue
			if results[i]['PEPID'] not in self.matchTypes or results[i]['MTYPE'][0] < self.matchTypes[results[i]['PEPID']][0]:
					self.matchTypes[results[i]['PEPID']] = results[i]['MTYPE']

		for i in range(len(results)):
			if 'MTYPE' not in results[i]:
				continue
			results[i]['PMTYPE'] = self.matchTypes[results[i]['PEPID']]

		results_with_match = [x for x in results if 'MSTART' in x]
		results_without_match = [x for x in results if not 'MSTART' in x]
		return sorted(results_with_match, key=lambda x: x['MSTART']) + results_without_match

	def writeResults(self, csv_filename, gbk_filename, gbk_all_orfs=False):
		#get results
		results = self.getResults()

		#set row numbers
		for i in range(len(results)):
			results[i]['ROW'] = i+1

		#write csv data
		if not self.silent:
			print("writing results (tsv) to ", csv_filename, "...")

		with open(csv_filename, "w") as handle:
			writer = csv.writer(handle, delimiter='\t', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)

			cols = []
			cols.append((['ROW'], '#', 'pepper ' + __version__))
			cols.append((['PEPSEQ'], 'peptide', ''))
			cols.append((['PEPID'], 'peptideID (pepper)', ''))
			for i in range(len(self.evFiles)):
				evID = str(self.evFiles[i].id)
				evName = self.evFiles[i].basename
				cols.append((['EVPEPIDS', i], 'peptideID (evidence file ' + evID + ')', evName))
			for i in range(len(self.evFiles)):
				evID = str(self.evFiles[i].id)
				evName = self.evFiles[i].basename
				cols.append((['EVPROTS', i], 'identification (evidence file ' + evID + ')', evName))
			for i in range(len(self.evFiles)):
				evID = str(self.evFiles[i].id)
				evName = self.evFiles[i].basename
				exps = self.evFiles[i].getExperiments()
				for j in range(len(exps)):
					cols.append((['EVSCOUNTS', i, j], exps[j] + ' in evidence file ' + evID + ' (MS/MS counts)', evName))
			cols.append((['PEPPI'], 'predicted pI', ''))
			cols.append((['PEPMW'], 'predicted MW [Da]', ''))
			cols.append((['PEPGRAVY'], 'predicted GRAVY', ''))
			cols.append((['DNAMATCHES'], 'DNA matches (total)', self.refid))
			cols.append((['PMTYPE', 1], 'preferred match type', self.refid))
			cols.append((['MSEQ'], 'match sequence', self.refid))
			cols.append((['MSTART'], 'match start', self.refid))
			cols.append((['MEND'], 'match end', self.refid))
			cols.append((['MLEN'], 'match length', self.refid))
			cols.append((['MSTRAND'], 'match strand', self.refid))
			cols.append((['MFRAME'], 'match frame', self.refid))
			cols.append((['MUPAA'], 'aa upstream match', self.refid))
			cols.append((['MTYPE', 1], 'match type', self.refid))
			for i in range(len(self.seqCollections)):
				colID = str(self.seqCollections[i].getCollectionId())
				colName = self.seqCollections[i].basename
				cols.append((['MLOOKUPS', i], 'DNA match occurence in DB #' + colID, colName))
				cols.append((['PEPLOOKUPS', i], 'peptide occurence in DB #' + colID, colName))
			cols.append((['IFROVGENES'], 'inframe overlapped gene(s)', self.refid))
			cols.append((['IFROVLENS'], 'inframe overlap length(s)', self.refid))
			cols.append((['IFROVRELLENS'], '%inframe overlap(s)', self.refid))
			cols.append((['OFROVGENES'], 'outframe overlapped gene(s)', self.refid))
			cols.append((['OFROVLENS'], 'outframe overlap length(s)', self.refid))
			cols.append((['OFROVRELLENS'], '%outframe overlap(s)', self.refid))
			cols.append((['UPDIST'], 'upstream distance', self.refid))
			cols.append((['DOWNDIST'], 'downstream distance', self.refid))
			cols.append((['SYNTENY'], 'synteny', self.refid))
			cols.append((['ANNOTATION'], 'synteny annotation', self.refid))
			cols.append((['ORFCOUNT'], 'number predicted orfs', self.refid))
			cols.append((['ORFCLASSES'], 'orf classes', self.refid))
			cols.append((['BOSTART'], 'best potential ORF: start', self.refid))
			cols.append((['BOEND'], 'best potential ORF: end', self.refid))
			cols.append((['BOFRAME'], 'best potential ORF: frame', self.refid))
			cols.append((['BOTYPE'], 'best potential ORF: type', self.refid))
			cols.append((['BOSYNTGRAPH'], 'best potential ORF: gene synteny', self.refid))
			cols.append((['BOSYNTANNOT'], 'best potential ORF: gene synteny annotation', self.refid))
			cols.append((['BOANNOT'], 'best potential ORF: annotated', self.refid))
			cols.append((['BOLEN'], 'best potential ORF: length', self.refid))
			cols.append((['BOSCODON'], 'best potential ORF: start codon', self.refid))
			cols.append((['BORBS'], 'best potential ORF: potential RBS', self.refid))
			cols.append((['BOSPLEN'], 'best potential ORF: spacer length', self.refid))
			cols.append((['BOSTARTCLASS'], 'best potential ORF: start codon class', self.refid))
			cols.append((['BORBSCLASS'], 'best potential ORF: RBS class', self.refid))
			cols.append((['BOSPCLASS'], 'best potential ORF: spacer class', self.refid))
			cols.append((['BOCLASS'], 'best potential ORF: orf class', self.refid))
			cols.append((['BOIFROV'], 'best potential ORF: inframe overlaps', self.refid))
			cols.append((['BOOFROV'], 'best potential ORF: outframe overlaps', self.refid))
			cols.append((['BOUP'], 'best potential ORF: mapped unique peptides', self.refid))
			for i in range(len(self.seqCollections)):
				colID = str(self.seqCollections[i].getCollectionId())
				colName = self.seqCollections[i].basename
				cols.append((['BODNALOOKUPS', i], 'best potential ORF: DNA occurence in DB #' + colID, colName))
				cols.append((['BOPEPLOOKUPS', i], 'best potential ORF: AA occurence in DB #' + colID, colName))
			cols.append((['BOSEQ'], 'best potential ORF: sequence', self.refid))
			cols.append((['BOUPSTREAM'], 'best potential ORF: upstream sequence', self.refid))
			cols.append((['BOAA'], 'best potential ORF: translation', self.refid))
			cols.append((['BOPI'], 'best potential ORF: predicted isoelectric point', self.refid))
			cols.append((['BOMW'], 'best potential ORF: predicted MW [Da]', self.refid))
			cols.append((['BOGRAVY'], 'best potential ORF: predicted GRAVY', self.refid))

			maxorfs = max([0] + [len(x['ORFS']) for x in results if 'ORFS' in x])
			for i in range(maxorfs):
				j = i+1
				cols.append((['ORFS', i, 'OSTART'], 'potential ORF #' + str(j) + ': start', self.refid))
				cols.append((['ORFS', i, 'OEND'], 'potential ORF #' + str(j) + ': end', self.refid))
				cols.append((['ORFS', i, 'OFRAME'], 'potential ORF #' + str(j) + ' frame', self.refid))
				cols.append((['ORFS', i, 'OTYPE'], 'potential ORF #' + str(j) + ' type', self.refid))
				#cols.append((['ORFS', i, 'OANNOT'], 'potential ORF #' + str(j) + ' annotated', self.refid))
				cols.append((['ORFS', i, 'OLEN'], 'potential ORF #' + str(j) + ': length', self.refid))
				cols.append((['ORFS', i, 'OSCODON'], 'potential ORF #' + str(j) + ': start codon', self.refid))
				cols.append((['ORFS', i, 'ORBS'], 'potential ORF #' + str(j) + ': potential RBS', self.refid))
				cols.append((['ORFS', i, 'OSPLEN'], 'potential ORF #' + str(j) + ': potential spacer length', self.refid))
				cols.append((['ORFS', i, 'OSTARTCLASS'], 'potential ORF #' + str(j) + ': potential startcodon class', self.refid))
				cols.append((['ORFS', i, 'ORBSCLASS'], 'potential ORF #' + str(j) + ': potential RBS class', self.refid))
				cols.append((['ORFS', i, 'OSPCLASS'], 'potential ORF #' + str(j) + ': potential spacer class', self.refid))
				cols.append((['ORFS', i, 'OCLASS'], 'potential ORF #' + str(j) + ': orf class', self.refid))
				cols.append((['ORFS', i, 'OIFROV'], 'potential ORF #' + str(j) + ': inframe overlaps', self.refid))
				cols.append((['ORFS', i, 'OOFROV'], 'potential ORF #' + str(j) + ': outframe overlaps', self.refid))
				cols.append((['ORFS', i, 'OUP'], 'potential ORF #' + str(j) + ': mapped unique peptides', self.refid))
				for k in range(len(self.seqCollections)):
					colID = str(self.seqCollections[k].getCollectionId())
					colName = self.seqCollections[k].basename
					cols.append((['ORFS', i,'ODNALOOKUPS', k], 'potential ORF #' + str(j) + ': DNA occurence in DB #' + colID, colName))
					cols.append((['ORFS', i,'OPEPLOOKUPS', k], 'potential ORF #' + str(j) + ': AA occurence in DB #' + colID, colName))
				cols.append((['ORFS', i, 'OSEQ'], 'potential ORF #' + str(j) + ': sequence', self.refid))
				cols.append((['ORFS', i, 'OUPSTREAM'], 'potential ORF #' + str(j) + ': upstream sequence', self.refid))
				cols.append((['ORFS', i, 'OAA'], 'potential ORF #' + str(j) + ': translation', self.refid))

			##write headline
			writer.writerow([x[1] for x in cols])

			##write comment line
			writer.writerow([x[2] for x in cols])

			##write csv data line
			for result in results:
				line = []
				for keys in [x[0] for x in cols]:
					data = result
					missing = False
					for key in keys:
						if type(data) is list and len(data) <= key:
							data = self.navalue
							missing = True
						elif not missing:
							if type(data) is dict and key not in data:
								data = self.navalue
								missing = True
							else:
								data = data[key]
					line.append(data)
				writer.writerow(line)

		#gbk data
		if not self.silent:
			print("writing results (gbk) to ", gbk_filename, "...")

		record = self.refrecord
		record.name += "_PEPPER"
		gbk_best_orfs = set()
		gbk_potential_orfs = set()
		for result in results:
			if 'MSTART' not in result:
				continue
			s = min(result['MSTART'], result['MEND'])-1
			e = max(result['MSTART'], result['MEND'])
			strand = int(result['MSTRAND'] + "1")
			if result['DNAMATCHES'] == 1:
				ftype = 'exclusive_match'
			else:
				ftype = 'match'
			feature = SeqFeature(FeatureLocation(start=min(s, e), end=max(s, e), strand=strand), type=ftype, id=result['PEPID'])
			feature.qualifiers["pepid"] = result['PEPID']
			feature.qualifiers["translation"] = result['PEPSEQ']
			feature.qualifiers["exclusivity"] = str(result['DNAMATCHES'])
			record.features.append(feature)
			s = min(result['BOSTART'], result['BOEND'])-1
			e = max(result['BOSTART'], result['BOEND'])
			strand = int(result['BOSTRAND'] + "1")
			if (s, e, strand) in gbk_best_orfs:
				continue
			gbk_best_orfs.add((s, e, strand))
			feature = SeqFeature(FeatureLocation(start=min(s, e), end=max(s, e), strand=strand), type='best_orf')
			feature.qualifiers["translation"] = result['BOAA']
			feature.qualifiers["annotated"] = result['BOANNOT']
			feature.qualifiers["class"] = str(result['BOCLASS'])
			feature.qualifiers["uniq_pep"] = str(result['BOUP'])
			record.features.append(feature)
			feature = SeqFeature(FeatureLocation(start=min(s, e), end=max(s, e), strand=strand), type="TYPE_" + result['BOTYPE'])
			record.features.append(feature)
			if result['BORBS'] != self.navalue:
				spacer = result['BOSPLEN']+1
				rbslen = len(result['BORBS'])
				if result['BOSTRAND'] == "+":
					s = min(result['BOSTART'],result['BOEND'])-spacer-rbslen
					e = s+rbslen
					strand = 1
				else:
					s = max(result['BOSTART'],result['BOEND'])+spacer+rbslen-2
					e = s-rbslen+2
					strand = -1
				feature = SeqFeature(FeatureLocation(start=min(s, e), end=max(s, e), strand=strand), type='best_orf_rbs')
				feature.qualifiers["seq"] = result['BORBS']
				record.features.append(feature)
			if gbk_all_orfs:
				for orf in result['ORFS']:
					s = min(orf['OSTART'], orf['OEND'])-1
					e = max(orf['OSTART'], orf['OEND'])
					strand = int(orf['OSTRAND'] + "1")
					if (s, e, strand) in gbk_potential_orfs or (s, e, strand) in gbk_best_orfs:
						continue
					gbk_potential_orfs.add((s, e, strand))
					feature = SeqFeature(FeatureLocation(start=min(s, e), end=max(s, e), strand=strand), type='potential_orf')
					feature.qualifiers["translation"] = orf['OAA']
					feature.qualifiers["annotated"] = orf['OANNOT']
					feature.qualifiers["class"] = str(orf['OCLASS'])
					feature.qualifiers["uniq_pep"] = str(orf['OUP'])
					record.features.append(feature)
					feature = SeqFeature(FeatureLocation(start=min(s, e), end=max(s, e), strand=strand), type="TYPE_" + orf['OTYPE'])
					record.features.append(feature)
					if orf['ORBS'] != self.navalue:
						spacer = orf['OSPLEN']+1
						rbslen = len(orf['ORBS'])
						if orf['OSTRAND'] == "+":
							s = min(orf['OSTART'],orf['OEND'])-spacer-rbslen
							e = s+rbslen
							strand = 1
						else:
							s = max(orf['OSTART'],orf['OEND'])+spacer+rbslen-2
							e = s-rbslen-2
							strand = -1
						feature = SeqFeature(FeatureLocation(start=min(s, e), end=max(s, e), strand=strand), type='potential_orf_rbs')
						feature.qualifiers["seq"] = orf['ORBS']
						record.features.append(feature)


		with open(gbk_filename, "w") as handle:
			SeqIO.write(record, handle, 'genbank')

	def writeLog(self, filename, creation_time=False, minsample=0):
		if not self.silent:
			print("writing log (TXT) to ", filename, "...")

		out = ["PEPPER JOB LOG"]
		out.append('')
		out.append("1. General information")
		out.append("   pepper version: " + __version__)
		out.append("   command used: " + " ".join(sys.argv))

		if not creation_time:
			out.append("   creation time: " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
		else:
			out.append("   creation time: " + creation_time)

		out.append('')
		out.append("2. Reference")
		out.append("   reference file: " + os.path.basename(self.refFilename))
		out.append("   reference accession: " + self.refid)
		out.append("   reference description: " + self.refheader)
		out.append("   reference length: " + str(self.reflen) + "bp")
		out.append("   reference molecule type: circular")
		out.append("   annotated CDS: " + str(len(self.refgenes)))

		out.append('')
		out.append("3. MS Data")
		out.append("   maxquant evidence reports:")
		evgroups = sorted(set([x.group for x in self.evFiles]))
		for group in evgroups:
			out.append("      sampling group " + str(group) + ":")
			filedata = sorted([(x.basename, x.id) for x in self.evFiles if x.group == group])
			for data in filedata:
				out.append("         " + data[0] + " = evidence file " + str(data[1]))

		out.append('')

		out.append("4. Peptide filter")
		if self.seqfilter:
			out.append('   ' + self.seqfilter)
			out.append('')
		if self.uniq_only:
			out.append('   filtered peptides that do not map uniquely: ' + str(self.not_uniq))
		elif self.uniq_only_smart:
			out.append('   filtered peptides that belong to different protein families: ' + str(self.not_uniq_smart))

		pepdata = ["evidence_file", "filename", "sampling_group", "minimal_identifications*", "minimal_peptide_score", "minimal_posterior_error_probability", "minimal_msms_count**", "considered_unique_peptides"]
		pepdata = [pepdata] + sorted([[x.id, x.basename, x.group, minsample, x.score, x.perrp, x.msms, len(x.peps)] for x in self.evFiles])
		out.extend(["   " + x for x in output.formatColumns(pepdata).split("\n")])

		out.append('')
		out.append('   * minimal_identifications in different samples of the same sampling group (see section 3)')
		out.append('   ** minimal_msms_count is only applied to spectral counting')
		out.append('')
		out.append('   considered unique peptides in total: ' + str(len(self.pepids)))

		out.append('')
		out.append("5. Mapping parameter")
		out.append("   translation table: " + str(self.translTableID) + " (NCBI)")
		out.append("   peptides not mapping to the given reference: " + str(self.unmatched))
		if self.unmatched > 0:
			out[-1] += "   <-- ATTENTION! Unmatched peptides are listed in the TSV output with an DNA match count of 0 (see section 10.2)."

		out.append('')
		out.append("6. Orf prediction parameter")
		out.append('   orf prediction for annotated regions: ' + str(self.detect_all_orfs).lower())
		out.append('   protease to detect N-terminal peptides: ' + self.digestEnzyme)
		out.append('   overall predicted orfs: ' + str(len(self.orfs)))

		out.append('')
		out.append("7. Sequence databases")

		dbdata = sorted([(x.id, x.basename, x.descr, len(x.seqs), x.circular) for x in self.seqCollections])
		if len(dbdata) == 0:
			out.append("   no sequence database used")
			out.append('')
		else:
			for data in dbdata:
				out.append("   database " + str(data[0]))
				out.append("   description: " + data[2])
				out.append("   file name: " + data[1])
				out.append("   genomes: " + str(data[3]))
				if data[4]:
					out.append("   molecule type: circular")
				else:
					out.append("   molecule type: linear")
				out.append('')

		out.append('')
		out.append("8. Classification and scoring of DNA matches")
		out.append("   DNA matches are classified as followed:")
		matchdata = [["class", "short name", "description"]]
		matchdata.append(["1", "annotation", "DNA match is completely inframe ovelapped by an annotated CDS"])
		matchdata.append(["2", "extension I", "DNA match is partly inframe overlapped by an annotated CDS"])
		matchdata.append(["3", "extension II", "DNA match is inframe and upstream of an annotated CDS without any overlap or inframe stop codon between them"])
		matchdata.append(["4", "intergenic I", "DNA match is outframe overlapped by an annotated CDS"])
		matchdata.append(["5", "intergenic II", "DNA match is not overlapped by any annotated CDS"])
		out.extend(["   " + x for x in output.formatColumns(matchdata).split("\n")])
		out.append('   in case of multiple match types the preferred match type of a peptide is selected based on the ranking: class 1 > class 2 > ... > class 5')

		out.append('')
		out.append("9. ORF Prediction")
		out.append("   Open reading frames (ORFs) are predicted for each DNA match as followed.")
		out.append("   First, the closest inframe STOP codon downstream of the respective DNA match is considered as ORF end.")
		out.append("   If the match is not introduced by a digestion site of the used protease (n-terminal peptide match), the sequence between match start and the closest downstream inframe STOP is considered as the only possible ORF.")
		out.append("   Is a methionine is encoded immediately upstream of an n-terminal peptide match, this methione is considered as ORF start.")
		out.append("   In case of non-terminal DNA matches (upstream digestion site), the sequence between the clostest STOP codon upstream (excluding) and downstream (including) of the respective DNA match are considered as longest possible ORF.")
		out.append("   Alternative potential ORFs are considered by screening for potential inframe start codons (defined by the used translation table) which are upstream of the respective DNA match")
		out.append("   Potential ORFs are classified as followed.")
		out.append('')
		out.append("9.1 Classification and scoring of start codons")
		out.append("    Start codons are classified based on their sequence.")
		out.append("    class 1:   ATG, GTG, TTG")
		out.append("    class 2:   CTG, ATA, ATT, ATC")
		out.append("    class 3:   all other codons")
		out.append("")
		out.append("    scoring: class 1 > class 2 > class 3")
		out.append("")
		out.append("9.2 Classification and scoring of RBS")
		out.append("    Ribosomal binding sites (RBS) are classified based on their sequence.")
		out.append("    class 1:   GGAGG")
		out.append("    class 2:   one of the following variations:")
		out.append("               T, C or A at position 1")
		out.append("               T or G at position 3")
		out.append("               T, C or A at position 5")
		out.append("    class 3:   two of the variations listed in class 2")
		out.append("")
		out.append("    scoring: class 1 > class 2 > class 3")
		out.append("")
		out.append("9.3 Classification and scoring of RBS spacer")
		out.append("    Spacers of ribosomal binding sites are classified based on their length.")
		out.append("    class 1:   > 2nt upstream of the start codon")
		out.append("    class 2:   < 3nt upstream of the start codon")
		out.append("")
		out.append("    scoring: class 1 > class 2")
		out.append("")
		out.append("9.4 Classification and scoring of ORFs")
		out.append("    All potential ORFs are classified based on this scheme:")
		out.append("")
		orfclasses = ([["orf class", "rbs class", "rbs spacer class", "start codon class"]])
		orfclasses.append([1, 1, 1, 1])
		orfclasses.append([2, 2, 1, 1])
		orfclasses.append([3, 3, 1, 1])
		orfclasses.append([4, 1, 1, 2])
		orfclasses.append([5, 2, 1, 2])
		orfclasses.append([6, 3, 1, 2])
		orfclasses.append([7, 1, 1, 3])
		orfclasses.append([8, 2, 1, 3])
		orfclasses.append([9, 3, 1, 3])
		orfclasses.append([10, 1, 2, 1])
		orfclasses.append([11, 2, 2, 1])
		orfclasses.append([12, 3, 2, 1])
		orfclasses.append([13, 1, 2, 2])
		orfclasses.append([14, 2, 2, 2])
		orfclasses.append([15, 3, 2, 2])
		orfclasses.append([16, 1, 2, 3])
		orfclasses.append([17, 2, 2, 3])
		orfclasses.append([18, 3, 2, 3])
		orfclasses.append([19, "-", "-", 3])
		orfclasses.append([20, "-", "-", 3])
		orfclasses.append([21, "-", "-", 3])
		out.extend(["    " + x for x in output.formatColumns(orfclasses).split("\n")])
		out.append("    scoring: class 1 > class 2 > class 3 > ... > class 21")
		out.append("             if different orfs share the same class, the longer orf is preferred")

		out.append('')
		out.append("10. Output")
		out.append("10.1 Concepts")
		out.append("    pepper is using following terms and definitions.")
		out.append("    peptide: an identified peptide meeting the filter criteria (see section 4 for details on the used peptide filter criteria)")
		out.append("    (DNA) match: a genomic region in a given reference sequence encoding for a given peptide (see section 2 for more details on the used reference)")
		out.append("    potential orf: a putative open reading frame (see section 9 for more details on orf prediction)")
		out.append("    best potential orf: the putative open reading frame of a given DNA match with the highest ranking score (see section 9.4 for details on orf ranking)")
		out.append("")
		out.append("10.2 TSV file")
		out.append("    A tabulator-separated file (TSV) has been generated containing all mapping results in tabular form.")
		out.append("    The first row of this file represents the  table head while additional column-related comments are listed in the second row.")
		out.append("    Following information is listed:")

		empty = "-"
		help_head = ["column", "comment", "value", "reference"]
		help_blankline = ["", "","",""]
		comment_evfile = "name of evidence file X (see section 3)"
		comment_ref = "accession of the used reference  (see section 2)"
		comment_db = "file name of sequence database X (see section 7)"

		helpdata = [["peptide specific information:", "","",""]]
		helpdata.append(help_head)
		helpdata.append(["#", empty, "peptide sequence", empty])
		helpdata.append(["peptide", "pepper version", "row number", empty])
		helpdata.append(["peptideID (pepper)", empty, "peptideID assigned by pepper", empty])
		helpdata.append(["peptideID (evidence file X)", comment_evfile, "peptideID(s) assigned in evidence file X (see section 3 for details on used evidence files)", empty])
		helpdata.append(["identification (evidence file X)", comment_evfile, "protein identification(s) provided in evidence file X (see section 3 for details on used evidence files)", empty])
		helpdata.append(["EXP (evidence file X; MS/MScounts)", comment_evfile, "filtered MS/MS counts for experiment EXP in evidencefile X (see section 3 and 4 for details on used evidence files and peptide filtering criteria, respectively)", empty])
		helpdata.append(["predicted pI", empty, "isoelectric point of the identified peptide predicted according to Bjellqvist et al.", "Bjellqvist, B.,Hughes, G.J., Pasquali, Ch., Paquet, N., Ravier, F., Sanchez, J.-Ch., Frutiger, S. & Hochstrasser, D.F. The focusing positions of polypeptides in immobilized pH gradients can be predicted from their amino acid sequences. Electrophoresis 1993, 14, 1023-1031; Bjellqvist, B., Basse, B., Olsen, E. and Celis, J.E. Reference points for comparisons of two-dimensional maps of proteins from different human cell types defined in a pH scale where isoelectric points correlate with polypeptide compositions. Electrophoresis 1994, 15, 529-539."])
		helpdata.append(["predicted MW", empty, "theoretic molecular weight [da] of the identified peptide", empty])
		helpdata.append(["predicted GRAVY", empty, "GRAVY (grand average of hydropathy) of the identified peptide predicted according to Kyte and Doolittle", "Kyte J., Doolittle RF. A simple method for displaying the hydropathic character of a protein. J Mol Biol. 1982 May 5;157(1):105-32"])
		helpdata.append(["DNA matches (total)", comment_ref, "number of genomic locations the identified peptide has been matched to (for each match a single  row is listed)", empty])
		helpdata.append(["preferred match type", comment_ref, "the preferred match type based on all matches of the identified peptide (see section 8 for details on match types)", empty])
		helpdata.append(help_blankline)

		helpdata.append(["match specific information:", "","",""])
		helpdata.append(help_head)
		helpdata.append(["match sequence", comment_ref, "sequence of the respective DNA match", empty])
		helpdata.append(["match start", comment_ref, "start coordinate of the respective DNA match within the given reference sequence", ""])
		helpdata.append(["match end", comment_ref, "end coordinate of the respective DNA match within the given reference sequence", ""])
		helpdata.append(["match length", comment_ref, "length [nt] of the respective DNA match", ""])
		helpdata.append(["match strand", comment_ref, "strand of the respective DNA match within the given reference sequence", empty])
		helpdata.append(["match frame", comment_ref, "frame of the respective DNA match based on the given reference sequence", empty])
		helpdata.append(["aa upstream match", comment_ref, "amino acid encoded immediatly upstream of the respective DNA match", empty])
		helpdata.append(["match type", comment_ref, "match type of the respective DNA match (see section 8)", empty])
		helpdata.append(["DNA match occurence in DB #X", comment_db, "number of genomes in the given sequence database X encoding the exact DNA match sequence at least once (ambiguous nucleotides in the sequence database, e.g. N, are not interpreted)", empty])
		helpdata.append(["peptide occurence in DB #X", comment_db, "number of genomes in the given sequence database X encoding the exact sequence of the identified peptide at least once (ambiguous amino acids in the sequence database, e.g. Y, are not interpreted!)", empty])
		helpdata.append(["inframe overlapped gene(s)", comment_ref, "accession(s) of annotated CDS inframe overlapped by the respective DNA match", empty])
		helpdata.append(["inframe overlap length(s)", comment_ref, "length of the repective DNA match [nt] inframe overlapped by the annotated CDS(s)", empty])
		helpdata.append(["%inframe overlap(s)", comment_ref, "percentage length of the respective DNA match inframe overlapped by the annotated CDS(s)", empty])
		helpdata.append(["outframe overlap length(s)", comment_ref, "length of the repective DNA match [nt] outframe overlapped by the annotated CDS(s)", empty])
		helpdata.append(["%outframe overlap(s)", comment_ref, "percentage length of the respective DNA match outframe overlapped by the annotated CDS(s)", empty])
		helpdata.append(["upstream distance", comment_ref, "distance of the repective DNA match to the closest annotated upstream CDS", empty])
		helpdata.append(["downstream distance", comment_ref, "distance of the repective DNA match to the closest annotated downstream CDS", empty])
		helpdata.append(["synteny", comment_ref, "genetic neighborhood of the repective DNA match (considered are only annotated CDS of the given reference)", empty])
		helpdata.append(["annotation", comment_ref, "annotation of CDSs adjacent to the respective DNA match", empty])
		helpdata.append(help_blankline)

		helpdata.append(["orf specific information:", "","",""])
		helpdata.append(help_head)
		helpdata.append(["orf classes", comment_ref, "classes of all potential orfs suggested for the respective DNA match (see section 9 for details on orf detection and classification)", empty])
		helpdata.append(["best potential ORF: start", comment_ref, "start coordinate of the highest rated potential orf suggested for the respective DNA match", empty])
		helpdata.append(["best potential ORF: end", comment_ref, "end coordinate of the highest rated potential orf suggested for the respective DNA match", empty])
		helpdata.append(["best potential ORF: frame", comment_ref, "frame of the highest rated potential orf suggested for the respective DNA match", empty])
		helpdata.append(["best potential ORF: gene synteny", comment_ref, "genetic synteny of the highest rated potential orf", empty])
		helpdata.append(["best potential ORF: annotation", comment_ref, "annotation of annotated CDS adjactend to the highest rated potential orf", empty])
		helpdata.append(["best potential ORF: annotated", comment_ref, "annotation status of the highest rated potential orf (a plus (+) indicates that the orf has been annotated as CDS in the used reference)", empty])
		helpdata.append(["best potential ORF: length", comment_ref, "length [nt] of the highest rated potential orf", empty])
		helpdata.append(["best potential ORF: start codon", comment_ref, "start codon of the highest rated potential orf", empty])
		helpdata.append(["best potential ORF: potential RBS", comment_ref, "sequence of the potential ribosomal binding site upstream of the highest rated potential orf", empty])
		helpdata.append(["best potential ORF: spacer length", comment_ref, "spacer length [nt] between the potential ribosomal binding site and start of the best potential orf", empty])
		helpdata.append(["best potential ORF: start codon class", comment_ref, "start codon class of the best potential orf (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: RBS class", comment_ref, "RBS class of the best potential orf (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: spacer class", comment_ref, "RBS spacer class of the best potential orf (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: orf class", comment_ref, "orf class of the best potential orf (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: inframe overlaps", comment_ref, "annotated CDS(s) that are inframe overlapped by the best potential orf", empty])
		helpdata.append(["best potential ORF: outframe overlaps", comment_ref, "annotated CDS(s) that are outframe overlapped by the best potential orf", empty])
		helpdata.append(["best potential ORF: mapped unique peptides", comment_ref, "number of unique peptides matching to the highest ranked potential orf (unique within the highest ranked potential orf)", empty])
		helpdata.append(["best potential ORF: DNA occurence in DB #X", comment_db, "number of genomes in the given database file X encoding the exact orf sequence at least once (ambiguous nucleotides in the sequence database, e.g. N, are not interpreted)", empty])
		helpdata.append(["best potential ORF: AA occurence in DB #X", comment_db, "number of genomes in the given database file X encoding the exact amino acid sequence as the highest ranked potential orf at least once (ambiguous amino acids in the sequence database, e.g. Y, are not interpreted)", empty])
		helpdata.append(["best potential ORF: AA occurence in DB #X", comment_db, "number of genomes in the given database file X encoding the exact amino acid sequence as the highest ranked potential orf at least once (ambiguous amino acids in the sequence database, e.g. Y, are not interpreted)", empty])
		helpdata.append(["best potential ORF: sequence", comment_ref, "DNA sequence of the highest ranked potential orf", empty])
		helpdata.append(["best potential ORF: upstream sequence", comment_ref, "DNA sequence immediatly upstream of the highest ranked potential orf", empty])
		helpdata.append(["best potential ORF: translation", comment_ref, "amino acid sequence encoded by the highest ranked potential orf", empty])
		helpdata.append(["best potential ORF: predicted isoelectric point", comment_ref, "isoelectric point of the protein encoded by the highest ranked potential orf predicted according to Bjellqvist et al.", "Bjellqvist, B.,Hughes, G.J., Pasquali, Ch., Paquet, N., Ravier, F., Sanchez, J.-Ch., Frutiger, S. & Hochstrasser, D.F. The focusing positions of polypeptides in immobilized pH gradients can be predicted from their amino acid sequences. Electrophoresis 1993, 14, 1023-1031; Bjellqvist, B., Basse, B., Olsen, E. and Celis, J.E. Reference points for comparisons of two-dimensional maps of proteins from different human cell types defined in a pH scale where isoelectric points correlate with polypeptide compositions. Electrophoresis 1994, 15, 529-539."])
		helpdata.append(["best potential ORF: predicted MW [Da]", comment_ref, "theoretic molecular weight [Da] of the protein encoded by the highest ranked potential orf ", empty])
		helpdata.append(["best potential ORF: predicted GRAVY", comment_ref, "GRAVY (grand average of hydropathy) of the protein encoded by the highest ranked potential orf predicted according to Kyte and Doolittle", "Kyte J., Doolittle RF. A simple method for displaying the hydropathic character of a protein. J Mol Biol. 1982 May 5;157(1):105-32"])
		helpdata.append(["potential ORF #X: start", comment_ref, "start coordinate of potential orf X suggested for the respective DNA match (potential orfs are sorted by length in descending order)", empty])
		helpdata.append(["potential ORF: end", comment_ref, "end coordinate of potential orf X suggested for the respective DNA match", empty])
		helpdata.append(["potential ORF: frame", comment_ref, "frame of potential orf X suggested for the respective DNA match", empty])
		helpdata.append(["potential ORF: annotated", comment_ref, "annotation status of potential orf X (a plus (+) indicates that the orf has been annotated as CDS in the used reference)", empty])
		helpdata.append(["potential ORF: length", comment_ref, "length [nt] of potential orf X", empty])
		helpdata.append(["potential ORF: start codon", comment_ref, "start codon of potential orf X", empty])
		helpdata.append(["potential ORF: potential RBS", comment_ref, "sequence of the potential ribosomal binding site upstream of potential orf X", empty])
		helpdata.append(["potential ORF: spacer length", comment_ref, "spacer length [nt] between the potential ribosomal binding site and start of potential orf X", empty])
		helpdata.append(["best potential ORF: start codon class", comment_ref, "start codon class of potential orf X (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: RBS class", comment_ref, "RBS class of potential orf X (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: spacer class", comment_ref, "RBS spacer class of potential orf X (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: orf class", comment_ref, "orf class of potential orf X (see section 9 for details on classification)", empty])
		helpdata.append(["best potential ORF: inframe overlaps", comment_ref, "annotated CDS(s) that are inframe overlapped by potential orf X", empty])
		helpdata.append(["best potential ORF: outframe overlaps", comment_ref, "annotated CDS(s) that are outframe overlapped by potential orf X", empty])
		helpdata.append(["best potential ORF: mapped unique peptides", comment_ref, "number of unique peptides matching to potential orf X (unique within the potential orf)", empty])
		helpdata.append(["best potential ORF: DNA occurence in DB #X", comment_db, "number of genomes in the given database file X encoding the exact orf sequence at least once (ambiguous nucleotides in the sequence database, e.g. N, are not interpreted)", empty])
		helpdata.append(["best potential ORF: AA occurence in DB #X", comment_db, "number of genomes in the given database file X encoding the exact amino acid sequence as potential orf X at least once (ambiguous amino acids in the sequence database, e.g. Y, are not interpreted)", empty])
		helpdata.append(["best potential ORF: AA occurence in DB #X", comment_db, "number of genomes in the given database file X encoding the exact amino acid sequence as potential orf X at least once (ambiguous amino acids in the sequence database, e.g. Y, are not interpreted)", empty])
		helpdata.append(["best potential ORF: sequence", comment_ref, "DNA sequence of the highest ranked potential orf", empty])
		helpdata.append(["best potential ORF: upstream sequence", comment_ref, "DNA sequence immediatly upstream of potential orf X", empty])
		helpdata.append(["best potential ORF: translation", comment_ref, "amino acid sequence encoded by potential orf X", empty])
		out.extend(["    " + x for x in output.formatColumns(helpdata).split("\n")])

		out.append("10.3 GBK")
		out.append("    A genbank file (GBK) has been generated containing the original reference annotation as well as")
		out.append("    - all DNA matches of identified peptides (feature type: match)")
		out.append("    - highest-ranked potential orf of each DNA match (feature type: best_orf")
		out.append("    - potential RBS if present upstream of a highest ranked putative orfs (feature type: best_orf_rbs")

		out.append('')
		out.append("10.4 LOG")
		out.append("     You are reading it :)")
		out.append('')
		out.append("11. Disclaimer & Contact")
		out.append("    pepper has been developed by Stephan Fuchs (fuchss@rki.de). All rights are reserved.")
		out.append("    Until pulication pepper may be not distributed in any form.")
		out.append("    Please also consider, that pepper is still in beta status.")
		out.append("    Thus, for legal reasons no liability can be assumed for any information generated by pepper.")

		with open(filename, "w") as handle:
			handle.write("\n".join(out))

class tblastn():
	def __init__(self, db, infile , outfile, resultdir = "./", descr = False, id = False, silent = False):
		#blast option
		self.db = db

		#blast results
		self.hits = {}

		#general attributes
		self.silent = silent
		self.descr = descr
		self.resultdir = resultdir
		self.dbSize = self.getDbSize()
		self.infile = infile
		self.outfile = outfile
		self.pepseqs=set()
		self.id = id

	def logParameter(self):
		log = []
		if not self.id:
			id = ""
		else:
			id = " " + str(self.id).strip()
		log.append('# blast DB' + id + ' file: ' + self.db)
		log.append('# blast DB' + id + ' description: ' +  self.descr)
		log.append('# blast DB' + id + ' entries: ' +  str(self.dbSize))
		log.append('# blast DB' + id + ' infile: ' +  os.path.basename(self.infile))
		log.append('# blast DB' + id + ' outfile: ' +  os.path.basename(self.outfile))
		return log

	def getDb(self):
		return self.db

	def getDbSize(self):
		n = 0
		with open(self.db, "r") as handle:
			for line in handle:
				if not line:
					continue
				if line[0] == ">":
					n += 1
		return n

	def prepareInfile(self, pepseqs, infile = False):
		if not self.silent:
			print("preparing query file for blast...")
		if not infile:
			infile = self.infile
		else:
			self.infile = infile
		with open(os.path.join(self.resultdir, self.infile), "w") as handle:
			for pepseq in pepseqs:
				if pepseq not in self.pepseqs:
					self.pepseqs.add(pepseq)
					handle.write(">" + pepseq + "\n" + pepseq + "\n")

	def setOutfile(self, filename):
		self.outfile = filename

	def blastPeps(self, pepseqs, evalue=10, cpus=1):
		self.prepareInfile(pepseqs)

		if not self.silent:
			print("blasting...")

		tblastn_cli = NcbitblastnCommandline(cmd='tblastn', num_threads=cpus, query=self.infile, db=self.db, evalue=evalue, outfmt=7, out=self.outfile)
		#print(NcbitblastnCommandline())
		stdout, stderr = tblastn_cli()

	def	getHitAcc(self, resultfile=False, minfullident=100):
		if not resultfile:
			resultfile=self.outfile

		blast_results = {}
		with open(resultfile, "r") as handle:
			for line in handle:
				if len(line.strip()) == 0 or line[0] == "#":
					continue
				fields = line.split("\t")
				pepseq = fields[0].strip()
				hitAcc = fields[1]
				ident = float(fields[2]) / len(pepseq) * int(fields[3])
				if ident >= minfullident:
					if pepseq not in self.hits:
						self.hits[pepseq] = set()
					self.hits[pepseq].add(hitAcc)

	def getConservation(self, pepseq):
		if not self.hits:
			self.getHitAcc()

		if pepseq not in self.pepseqs:
			return "na"
		elif pepseq not in self.hits:
			return 0
		else:
			return len(self.hits[pepseq])

if __name__ == "__main__":
	#processing command line arguments
	parser = argparse.ArgumentParser(prog=os.path.basename(__file__), description='PEPPER: A pipeline for bacterial proteogenomics.', add_help=False)

	in_opt = parser.add_argument_group('1. Input files [required]')
	in_opt.add_argument('-r', '--reffile', metavar="GBK_FILE", help="reference genome and annotation (genbank format) [required]", type=argparse.FileType('r'), required=True)
	in_opt.add_argument('-e', '--evfile', metavar="EV_FILE", help="evidence file(s) provided by MaxQuant [required]. This option can be stated multiple times to group assigend files to replication groups (see --minsample).", type=argparse.FileType('r'), action='append', nargs = "+", required=True)

	pep_opt = parser.add_argument_group('2. Peptide filter options')
	pep_opt.add_argument('--minsample', help="minimal number of identifications in different samples of a sample group (see --evfile) to consider the respective peptide (default: 2)", type=float, default = 2)
	pep_opt.add_argument('--seqfilter', metavar="FILE", help="file containing peptide sequences (one per line) to consider", type=argparse.FileType('r'), default=False)
	pep_opt.add_argument('--uniq_only', help="consider only peptide which map to the reference genome at one position only", action='store_true')
	pep_opt.add_argument('--uniq_only_smart', help="consider only peptide which map not to the same protein family", action='store_true')

	spec_opt = parser.add_argument_group('3. MS/MS check options')
	spec_mut = spec_opt.add_mutually_exclusive_group()
	spec_mut.add_argument('--no_msms', help="consider also peptides without ms/ms data", action='store_true')
	spec_mut.add_argument('--msms', metavar="FILE", help="msms file containing ms/ms data export", type=argparse.FileType('r'), nargs = "+")
	spec_opt.add_argument('--minintcov', metavar="FLOAT", help="minimal intensity coverage to filter ms/ms spectra (default: 0.01). Has to be combined with --msms", type=float, default=0.01)

	spectral_opt = parser.add_argument_group('4. Spectral counting filter options')
	spectral_opt.add_argument('--perrp', metavar="FLOAT", help="maximal posterior error (excluded)(default: 0.1)", type=float, default = 0.1)
	spectral_opt.add_argument('--score', metavar="INT", help="minimal score (excluded)(default: 40)", type=float, default = 40)

	map_opt = parser.add_argument_group('5. Mapping options')
	map_opt.add_argument('--tt', metavar="INT", help="NCBI translational table (default: 11)", type=int, default=11)

	orf_opt = parser.add_argument_group('6. ORF prediction options')
	orf_opt.add_argument('--protease', help="enzyme used for digestion (default: trypsin)", type=str, choices=["trypsin", "chymotrypsin", "proteinase-k", "lys-c", "arg-c", "glu-c", "asp-n"], default="trypsin")
	orf_opt.add_argument('--detect_all_orfs', help="predict potential ORs for annotated regions, too", action='store_true')

	db_opt = parser.add_argument_group('7. Sequence database options')
	db_opt.add_argument('--db', metavar="FASTA_FILE", help="sequence databases (multi-FASTA format) containing genomes to check phylogenetic conservation", type=argparse.FileType('r'), default=[], nargs="+")

	out_opt = parser.add_argument_group('8. Output options')
	out_opt.add_argument('--gbk_all_orfs', help="include all potential orfs in genebank output", action='store_true')

	general_opt = parser.add_argument_group('9. General options')
	general_opt.add_argument('-t', '--threads', metavar="INT", help="threads to use (by default: 1).", type=int, default=1)
	general_opt.add_argument('--version', action='version', version='%(prog)s ' + __version__)
	general_opt.add_argument("-h", "--help", action="help", help="show this help message and exit")

	#parser.add_argument('--evalue', metavar="INT", help="max evalue of blast hits (by default: 10).", type=int, default=10)
	#parser.add_argument('--ident', metavar="INT", help="min full-length identity of blast hits in percent (by default: 100).", type=int, default=100)

	args = parser.parse_args()

	#check args
	if not args.no_msms:
		msms = True
	else:
		msms = False

	if args.db == False:
		args.db = []
	else:
		for i in range(len(args.db)):
			n = args.db[i].name
			args.db[i].close()
			args.db[i] = n

	#initialize mapper
	print("initializing mapper...")
	mapper = pepper(args.reffile.name, args.tt, cpus=args.threads, digestEnzyme = args.protease, silent=False, uniq_only=args.uniq_only)
	args.reffile.close()
	outdir = False
	filtered_peps = set()

	#processing evidence file (MaxQuant export)
	evgroup = -1
	for evfile_group in args.evfile:
		evgroup += 1
		for evfile in evfile_group:
			mapper.addEvFile(evfile.name, args.perrp, msms, args.score, evgroup)
		evfile.close()

	#user-based pep filtering
	if args.seqfilter:
		print("sequence-based peptide filtering ...")
		if not outdir:
			outdir = output.createResultDir("pepper")
		include = set()
		exclude = set()
		remain = set()
		for line in args.seqfilter:
			line = line.strip()
			if line:
				include.add(line)
		if len(include) == 0:
			exit("error:  no peptide sequences found in " + args.seqfilter.name)
		for evFile in mapper.evFiles:
			peps = set(evFile.peps.keys())
			exclude.update(peps-include)
			remain.update(peps.intersection(include))

		for pepseq in exclude:
			filtered_peps.add((pepseq, "user-based sequence filter"))
			mapper.delPep(pepseq)

		mapper.seqfilter = str(len(remain)) + " peptides included based on " + str(len(include)) + " unique sequences in file " + args.seqfilter.name + " (" + str(len(exclude)) + " peptides excluded)"

		print("  " + str(len(remain)) + " peptide(s) remaining")
		if len(remain) == 0:
			exit(0)

	#sample-based pep filtering
	print("sample-based peptide filtering ...")
	pepFreq = {}
	for evFile in mapper.evFiles:
		group = evFile.group
		for pepseq, pepdata in evFile.peps.items():
			if pepseq not in pepFreq:
				pepFreq[pepseq] = {}
			if group not in pepFreq[pepseq]:
				pepFreq[pepseq][group] = 0
			pepFreq[pepseq][group] += sum([1 for exp in pepdata if pepdata[exp] > 0])

	for pepseq in [x for x in pepFreq if max(pepFreq[x].values()) < args.minsample]:
		filtered_peps.add((pepseq, "sample-based filter"))
		mapper.delPep(pepseq)

	remainingPeps = len(mapper.pepids)
	print("  " + str(remainingPeps) + " peptide(s) remaining")
	if remainingPeps == 0:
		exit(0)

	#ms/ms speccheck filter
	if args.msms:
		print("MS/MS spectrum based filtering ...")
		if not outdir:
			outdir = output.createResultDir("pepper")
		sc = speccheck.specchecker()
		sc.set_minintcov(args.minintcov)
		sc.add_files(*[x.name for x in args.msms])
		sc.classify(True)
		csv_outfile = os.path.join(outdir, outdir.lower() + ".specclasses.tsv")
		sc.write_spec_report(csv_outfile)
		for pepseq, specclass in sc.get_class_dict().items():
			if specclass > 3:
				filtered_peps.add((pepseq, "MS2 spectrum-based filter"))
				mapper.delPep(pepseq)

	remainingPeps = len(mapper.pepids)
	print("  " + str(remainingPeps) + " peptide(s) remaining")
	if remainingPeps == 0:
		exit(0)

	#processing dbs
	if len(args.db) > 0:
		for i in range(len(args.db)):
			mapper.addSeqCollection(args.db[i],  os.path.basename(args.db[i]))

	#backmapping
	print("mapping peptides...")
	mapper.mapPeps()
	if args.uniq_only:
		print(" ", str(mapper.not_uniq), "peptides mapped to multiple locations and were excluded")
	if mapper.unmatched > 0:
		print("  Attention:", mapper.unmatched, "unmatched peptides")

	#analyse mappings
	print("analyse mappings...")
	mapper.analyseMappings(args.detect_all_orfs)

	#smart unique pep filter
	if args.uniq_only_smart and not args.uniq_only:
		print("smart unique peptide filtering...")
		for pepseq in mapper.smartUniqPepFilter():
			filtered_peps.add((pepseq, "smart unique peptide filter"))
		print("  " + str(len(mapper.pepids)) + " peptide(s) remaining")

	#write results
	print("preparing results...")
	if not outdir:
		outdir = output.createResultDir("pepper")
	csv_outfile = os.path.join(outdir, outdir.lower() + ".tsv")
	gbk_outfile = os.path.join(outdir, outdir.lower() + ".gbk")
	filter_outfile = os.path.join(outdir, outdir.lower() + ".filter.tsv")
	mapper.writeResults(csv_outfile, gbk_outfile, args.gbk_all_orfs)

	if len(filtered_peps) > 0:
		print("writing filtered peptides (tsv) to " + filter_outfile + " ...")
		with open(filter_outfile, "w") as handle:
			handle.write("peptide\texcluded by\n")
			handle.write("\n".join([x[0]+"\t"+x[1] for x in filtered_peps]))

	timestamp = outdir.split("_")[-2] + " " + outdir.split("_")[-1].replace("-", ":")
	log_outfile = os.path.join(outdir, outdir.lower() + ".log")
	mapper.writeLog(log_outfile, timestamp, args.minsample)
