#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author Stephan Fuchs (fuchss@rki.de)
# This code is under MIT licence, you can find the complete licence here: https://gitlab.com/s.fuchs/pepper

import re
import argparse
import os
import bioalphabets
from Bio import SeqUtils
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from Bio.SeqUtils.CheckSum import seguid
from Bio.Seq import Seq
from Bio import SeqIO
from random import shuffle
import pandas as pd
import operator
import itertools
import math
from collections import defaultdict

version = "0.0.10"

class styledict(dict):
	def __missing__(self, key):
		return '{' + key + '}'

class bioseq(object):
	def __init__(self, seq, id = None, descr=None, parent=None, parent_frame=None, start=None, end=None):
		self._seq = seq.strip()
		self.id = id
		self.descr = descr
		self._seqlen = None
		self._seguid = None
		self._coords = [start, end]
		self._parent = parent
		self._parent_frame = parent_frame
		self._origin = None

	def get_style_dict(self, style):
		tags = set(re.findall(r'\{[a-z*]+\}', style))
		style_dict = styledict()
		if "{id}" in tags:
			style_dict['id'] = str(self.id)
		if "{descr}" in tags:
			style_dict['descr'] = str(self.descr)
		if "{end}" in tags:
			style_dict['end'] = str(self.end)
		if "{start}" in tags:
			style_dict['start'] = str(self.start)
		if "{strand}" in tags:
			style_dict['strand'] = str(self.strand)
		if "{frame}" in tags:
			style_dict['frame'] = str(self.parent_frame)
		if "{parent}" in tags:
			x = None if self.parent is None else self.parent.id
			style_dict['parent'] = str(x)
		if "{origin}" in tags:
			x = None if self._origin is None else self._origin.id
			style_dict['origin'] = str(x)
		if "{seqlen}" in tags:
			style_dict['seqlen'] = str(self.seqlen)
		if "{seguid}" in tags:
			style_dict['seguid'] = self.seguid
		if "{pi}" in tags:
			x = None if self.pi is None else str(round(self.pi, 2))
			style_dict['pi'] = x
		if "{mw}" in tags:
			style_dict['mw'] = str(self.mw)
		if "{gravy}" in tags:
			x = None if self.gravy is None else str(round(self.gravy, 2))
			style_dict['gravy'] = x
		if "{seq*}" in tags:
			style_dict['seq*'] = self.seq
		if "{seq}" in tags:
			style_dict['seq'] = self.seq.rstrip("*")
		if "{digest}" in tags:
			style_dict['digest'] = self.digest
		if "{coords}" in tags:
			if any(self.coords):
				x = "..".join([str(x) for x in self.coords])
				if self.complement:
					x = "complement(" + x + ")"
			else:
				x = str(None)
			style_dict['coords'] = x

		return style_dict

	@property
	def strand(self):
		if not self._parent_frame:
			return None
		elif self._parent_frame > 0:
			return 1
		else:
			return -1

	@property
	def seq(self):
		'''
		stores aa sequence string

		doctest:
		>>> bioseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').seq
		'MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL'
		'''
		return self._seq

	def clean_descr(self, descr):
		'''
		removes accession/id at description start
		to avoid accession doubling in header
		'''
		if self.id and descr and descr.startswith(self.id):
			return descr[len(self.id):]
		return descr

	@property
	def descr(self):
		return self._descr

	@descr.setter
	def descr(self, descr):
		'''
		sets descr
		'''
		if descr:
			descr = descr.strip().lstrip(">").strip()
			descr = self.clean_descr(descr)
		self._descr = descr

	@property
	def seqlen(self):
		'''
		stores sequence length

		>>> bioseq('MSK').seqlen
		3
		'''
		if self._seqlen is None:
			self._seqlen = len(self.seq)
		return self._seqlen

	@property
	def seguid(self):
		'''
		provides leading seguid.
		Please note: trailing * are not considered

		doctest:
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL*').seguid
		'hqcUoSAHJx1lPjM4RVARnXiwA6M'
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').seguid
		'hqcUoSAHJx1lPjM4RVARnXiwA6M'
		'''

		if self._seguid is None:
			self._seguid = seguid(self.seq.rstrip("*"))
		return self._seguid

	@property
	def start(self):
		'''
		stores start coordinate related to a parent molecule (aa or nt)
		if no parent molecule is defined it is None
		'''
		return self._coords[0]

	@property
	def end(self):
		'''
		stores end coordinate related to a parent molecule (aa or nt)
		if no parent molecule is defined it is None
		'''
		return self._coords[1]

	@property
	def coords(self):
		'''
		stores coordinates as list [start, end] related to a parent molecule (aa or nt)
		if no parent molecule is defined it is None
		'''
		return self._coords

	@property
	def parent(self):
		'''
		stores parent molecule object (aaseq or ntseq)
		if no parent molecule is defined it is None

		doctest:
		>>> x = [x for x in aaseq('MSKIVKIIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').iterdigest()]
		>>> x[0].parent.seq
		'MSKIVKIIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL'
		'''
		return self._parent

	@property
	def parent_frame(self):
		'''
		stores frame of parent molecule object (aaseq or ntseq)
		if no parent molecule is defined it is None
		'''
		return self._parent_frame

	@property
	def complement(self):
		'''
		Trzue if encoded on reverse complement strand of parent molecule
		if no parent frame is defined it is None
		'''
		if self.parent_frame is None:
			return None
		elif self.parent_frame > 0:
			return False
		else:
			return True

	@property
	def origin(self):
		'''
		stores the original molecule object (most uppest parent)
		if no parent molecule is defined it is None

		doctest:
		>>> x = [x for x in aaseq('MSKIVKIIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').iterdigest(miscleav=1)]
		>>> x[1].seq
		'MSKIVK'
		>>> x = [x for x in x[1].iterdigest()]
		>>> x[0].seq
		'MSK'
		>>> x[0].parent.seq
		'MSKIVK'
		>>> x[0].origin.seq
		'MSKIVKIIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL'

		'''
		if self._origin is None:
			if self.parent is None:
				return None
			obj = self.parent
			while obj.parent:
				obj = obj.parent
		return obj

	@staticmethod
	def fromfile(fname, format="fasta"):
		'''
		returns a bioseq object from a one-and-only-one record fasta or genbank file
		type 	nt|aa
		'''
		if format not in {"fasta", "gb", "genbank"}:
			raise ValueError("unknown file format.")
		record = SeqIO.read(fname, format)
		return {'seq': str(record.seq), 'descr': record.description, 'id': record.id}

	def wrap_seq(self, l=None, star=False, sep="\n"):
		seq = self.seq.rstrip("*") if not star else self.seq
		if l:
			return sep.join(re.findall(r'.{1,' + str(l) + '}', seq))
		return seq


	def tofasta(self, style="{id} {descr} [coords={coords}][seguid={seguid}][parent={parent}]", star=False, seqwrap = False):
		'''
		returns a FASTA formatted string

		doctest:
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL*').tofasta()
		'>None None [coords=None][seguid=hqcUoSAHJx1lPjM4RVARnXiwA6M][parent=None]\\nMSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL'
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL*').tofasta("I don't care [pi={pi}]", star=True)
		">I don't care [pi=5.7]\\nMSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL*"
		'''
		if style[0] == ">":
			style = style[1:]
		return ">" + style.format_map(self.get_style_dict(style)) + "\n" + self.wrap_seq(seqwrap, star=star)

	def tostr(self, style="{seq}"):
		'''
		returns a formatted string

		doctest:
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').tostr("{id},{pi},{mw},{gravy},{seq}")
		'None,5.7,6328.1106,-0.17,MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL'
		'''
		return style.format_map(self.get_style_dict(style))


############################ aaseq #############################################

class aaseq(bioseq):
	def __init__(self, seq, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, digest=None):
		super(aaseq, self).__init__(seq=seq, id=id, descr=descr, parent=parent, parent_frame=parent_frame, start=start, end=end)

		self._type = "aa"

		self._param = None
		self._gravy = None
		self._mw = None
		self._pi = None
		self._proteases = None
		self.digest = digest

	def __repr__(self):
		return 'aaseq(seq=%s, id=%s, descr=%s, parent=%s, parent_frame=%s, start=%s, end=%s)' % (self.seq, self.id, self.descr, self.parent, self.parent_frame, self.start, self.end)


	@property
	def type(self):
		'''
		stores 'aa' to indicate protein/peptide type
		'''
		return self._type

	@property
	def param(self):
		if self._param is None:
			self._param = ProteinAnalysis(self._seq.rstrip("*"))
		return self._param

	@property
	def gravy(self):
		'''
		stores GRAVY

		doctest:
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').gravy
		-0.1716666666666667
		'''
		if self._gravy is None:
			try:
				self._gravy	= self.param.gravy()
			except:
				pass
		return self._gravy

	@property
	def mw(self):
		'''
		stores molecular weight [Da]

		doctest:
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').mw
		6328.1106
		'''
		if self._mw is None:
			try:
				self._mw = self.param.molecular_weight()
			except:
				pass
		return self._mw

	@property
	def pi(self):
		'''
		stores pi

		doctest:
		>>> aaseq('MSKIVKIIGREIIDSRGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').pi
		5.700064277648925
		'''
		if self._pi is None:
			try:
				self._pi = self.param.isoelectric_point()
			except:
				pass
		return self._pi

	@property
	def proteases(self):
		'''
		return digestion pattern (regex) of defined proteases.
		If defining a new protease, please consider:
		The first match group has to refer to the position immediately after the cut.
		'''
		if self._proteases is None:
			self._proteases = {
				"trypsin": "[KR](?=([^P]))",
				"lys-c": "K(?=([^P]))",
				"arg-c": "(?:(?<=R(?=[^P]))|(?<=KK))(.)",
				"glu-c": "(?<=[DE])(.)",
				"asp-n": ".(?=([DC]))",
				"chymotrypsin": "[WYFL](?=(.))",
				"proteinasek": ".(?=(.))"
			}
		return self._proteases

	@staticmethod
	def fromfile(fname, format="fasta", **args):
		return aaseq(**bioseq.fromfile(fname, format), **args)

	def iterdigest(self, protease="trypsin", miscleav=0, minlen=0):
		'''
		returns an iterator providing aaseq objects for any peptide after digestion

		doctest:
		>>> [x.seq for x in aaseq('MSKIVKIIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').iterdigest()]
		['MSK', 'IVK', 'IIGR', 'EIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSR', 'EALELR', 'DGDK', 'SR', 'FL']
		>>> [x.seq for x in aaseq('MSKIVKIIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').iterdigest(minlen=5)]
		['EIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSR', 'EALELR']
		>>> [x.seq for x in aaseq('MSKIVKIIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELRDGDKSRFL').iterdigest(miscleav=1)]
		['MSK', 'MSKIVK', 'IVK', 'IVKIIGR', 'IIGR', 'IIGREIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSR', 'EIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSR', 'EIIDSRPGNPTVEAEVHLEGGFVGMAAAPSGASTGSREALELR', 'EALELR', 'EALELRDGDK', 'DGDK', 'DGDKSR', 'SR', 'SRFL', 'FL']
		'''
		prefix=str(self.id) + "_pep"
		if protease not in self.proteases:
			raise ValueError('protease not known')

		s = 0
		cut_sites = list(reversed([0] + [x.start(1) for x in re.finditer(self._proteases[protease], self.seq.upper())] + [len(self.seq)]))
		mc = [-x for x in range(1,miscleav+2)]
		i = 0
		while len(cut_sites) > 1:
			s = cut_sites.pop()
			l = len(cut_sites)
			zerofill = len(str(l))
			for e in [cut_sites[x] for x in mc if -x <= l]:
				if e-s >= minlen:
					i += 1
					yield aaseq(self.seq[s:e], id=prefix + str(i).zfill(zerofill), parent=self, start=s, end=e, digest=protease)

############################ ntseq #############################################

class ntseq(bioseq):
	def __init__(self, seq, tt=11, id=None, descr=None, start=None, end=None, parent=None, parent_frame=None, circ=True):
		super(ntseq, self).__init__(seq=seq, id=id, descr=descr, parent=parent, parent_frame=parent_frame, start=start, end=end)

		self._seqobj = Seq(seq)
		self._type = "nt"

		self._seqmod = None
		self._revcompl = None
		self._gc = None
		self._mw = None
		self.__circ = circ
		self.__tt = None
		self.set_tt(tt)
		self._alphabet = None

	def __repr__(self):
		return 'ntseq(seq=%s, id=%s, descr=%s, parent=%s, parent_frame=%s, start=%s, end=%s, circ=%s)' % (self.seq, self.id, self.descr, self.parent, self.parent_frame, self.start, self.end, self.circ)


	@property
	def seqmod(self):
		'''
		stores sequence length modulon to 3 (= nt overhang when spliced to codons)

		doctest:
		>>> ntseq('ATG').seqmod
		0
		>>> ntseq('ATGC').seqmod
		1
		>>> ntseq('ATGCA').seqmod
		2
		'''
		if self._seqmod is None:
			self._seqmod = self.seqlen%3
		return self._seqmod


	@property
	def revcompl(self):
		'''
		stores reverse complement sequence

		doctest:
		>>> ntseq('AGGTAATGTCATCAACTTTAAAAATACAATTATTATTTGTACATCAAATGCTGGCTTTGG').revcompl
		'CCAAAGCCAGCATTTGATGTACAAATAATAATTGTATTTTTAAAGTTGATGACATTACCT'
		'''
		if self._revcompl == None:
			self._revcompl = str(self._seqobj.reverse_complement())
		return self._revcompl

	@property
	def gc(self):
		'''
		stores %GC

		doctest:
		>>> ntseq('AGGTAATGTCATCAACTTTAAAAATACAATTATTATTTGTACATCAAATGCTGGCTTTGG').gc
		28.333333333333332
		'''
		if self._gc is None:
			self._gc = 100 * SeqUtils.gc_fraction(self._seqobj)
		return self._gc

	@property
	def mw(self):
		'''
		stores molecular weight [Da]

		doctest:
		>>> ntseq('AGGTAATGTCATCAACTTTAAAAATACAATTATTATTTGTACATCAAATGCTGGCTTTGG').mw
		18545.8944
		'''
		if self._mw is None:
			self._mw = SeqUtils.molecular_weight(self._seqobj, circular=self.circ)
		return self._mw

	@property
	def tt(self):
		'''
		stores translation table id (based on ncbi)
		'''
		return self.__tt

	@tt.setter
	def tt(self, tt):
		'''
		sets translation table id (based on ncbi)
		'''
		if tt != self.__tt:
			self._translation = None
			self._alphabet = None
			self.__tt = tt

	def set_tt(self, tt):
		'''
		sets translation table id (based on ncbi)
		'''
		self.tt = tt

	@property
	def circ(self):
		'''
		stores molecule circularity (True|False)
		'''
		return self.__circ

	@circ.setter
	def circ(self, circ):
		'''
		sets molecule circularity (True|False)
		'''
		if not isinstance(circ, bool):
			raise ValueError('circ has to be boolean')
		if circ != self.__circ:
			self._translation = None
			self.__circ = circ

	@property
	def alphabet(self):
		'''
		stores dnaalphabet object from bioalphabets module
		'''
		if self._alphabet is None:
			if self.tt is None:
				raise Error('use set_tt() to define a ncbi translation table first')
			self._alphabet = bioalphabets.dnaalphabet(self.tt)
		return self._alphabet

	@staticmethod
	def dna_revcompl(seq):
		return str(Seq(seq).reverse_complement())

	@staticmethod
	def fromfile(fname, format="fasta", **args):
		return ntseq(**bioseq.fromfile(fname, format), **args)

	def find(self, needle):
		'''
		returns list of start coordinates of needle in sequence
		(0-based; ambiguous chars are interpreted)

		doctest:
		>>> ntseq('AGGTAATGTCATCAACTTTAAAAATACAATTATTATTTGTACATCAAATGCTGGCTTTGG').find('AAT')
		[4, 22, 27, 46]
		>>> ntseq('AGGTAATGTCATCAACTTTAAAAATACAATTATTATTTGTACATCAAATGCTGGCTTTGG').find('GTGG')
		[]
		'''
		return [x for x in SeqUtils.nt_search(self.seq, needle)][1:]

	@staticmethod
	def check_frame(*frames):
		for frame in frames:
			if frame not in {1,2,3,-1,-2,-3}:
				raise ValueError('invalid frame (' + str(frame) + ')')

	@staticmethod
	def check_strand(strand):
		if strand not in {1, -1}:
			raise ValueError('invalid strand')

	def slice(self, start=None, stop=None, strand = 1):
		'''
		circle-safe sequence slicing

		doctest:
		>>> ntseq('AGGTAA').slice(0,8)
		'AGGTAAAG'
		>>> ntseq('AGGTAA', circ=False).slice(0,8)
		'AGGTAA'
		>>> ntseq('AGGTAA').slice(3,8,-1)
		'CCTTT'
		'''
		self.check_strand(strand)
		start = start if start is not None else 0
		stop = stop if stop is not None else self.seqlen
		seq = self.seq if strand == 1 else self.revcompl

		while start < 0:
			start += self.seqlen
			stop += self.seqlen

		while stop < 0:
			start += self.seqlen
			stop += self.seqlen

		s = ""
		if self.circ:
			if start > stop:
				s = seq[start:]
				start = 0
			while len(seq) < stop:
				seq += seq

		return s + seq[start:stop]

	def get_framed_seq(self, frame):
		'''
		returns a tuple with sequence of the given frame (circularity considered) and start in (0-based) in the sequence

		doctest:
		>>> ntseq('AGGTAATGTCATCAACTTT').get_framed_seq(1)
		'AGGTAATGTCATCAACTTT'
		>>> ntseq('AGGTAATGTCATCAACTTT').get_framed_seq(2)
		'GGTAATGTCATCAACTTTA'
		>>> ntseq('AGGTAATGTCATCAACTTT', circ=False).get_framed_seq(3)
		'GTAATGTCATCAACTTT'
		>>> ntseq('AGGTAATGTCATCAACTTT').get_framed_seq(-1)
		'AAAGTTGATGACATTACCT'
		>>> ntseq('AGGTAATGTCATCAACTTT', circ=False).get_framed_seq(-2)
		'AAGTTGATGACATTACCT'
		>>> ntseq('AGGTAATGTCATCAACTTT').get_framed_seq(-3)
		'AGTTGATGACATTACCTAA'
		'''
		self.check_frame(frame)
		if frame < 0:
			seq = self.revcompl
		else:
			seq = self.seq
		s = abs(frame) - 1
		if self.__circ:
			e = s
		else:
			e = 0
		return seq[s:] + seq[:e]

	def correct_coord(self, x, strand=1, switch=False):
		'''
		scales coordinates (0-based) behind ring closure based on sequence length
		doctest:
		>>> ntseq('ATGTAA').correct_coord(2)
		2
		>>> ntseq('ATGTAA').correct_coord(6)
		0
		>>> ntseq('ATGTAA').correct_coord(4, -1)
		1
		>>> ntseq('ATGTAA').correct_coord(6, -1)
		5
		'''
		if x >= self.seqlen or x < 0:
			x = x%self.seqlen
		if strand == -1:
			x = self.seqlen - x - 1
		if switch:
			return self.switch_strand(x)
		return x

	def get_frame(self, pos, strand=1):
		'''
		returns frame of pos (0-based)

		doctest:
		>>> ntseq('ATGTAAC').get_frame(0)
		1
		>>> ntseq('ATGTAAC').get_frame(1, -1)
		-3
		'''
		if strand == 1:
			return pos%3+1
		else:
			return -(self.switch_strand(pos)%3+1)

	@staticmethod
	def get_strand(frame):
		if frame > 0:
			return 1
		return -1

	def itercodons(self, start=None, end=None, strand=1):
		'''
		returns codons of the sequence
		sequence circuality is considered

		doctest:
		>>> [x for x in ntseq('ATGTAAC', circ=False).itercodons()]
		['ATG', 'TAA']
		>>> [x for x in ntseq('ATGTAAC', circ=False).itercodons(start=2, strand=-1)]
		['TAC']
		'''

		if start is None:
			start = 0
		if end is None:
			end = self.seqlen

		dist = (end - start) - (end - start)%3
		if dist < 3:
			return

		start = start%self.seqlen
		end = start + dist

		if strand == 1:
			seq = self.seq
		else:
			seq = self.revcompl

		seq = seq * math.ceil(end/self.seqlen)

		for i in range(start, end, 3):
			yield seq[i:i+3]

	def translate(self, start=None, end=None, strand=1, mstart=False):
		'''
		returns aaobject based on translation considering the given frame, sequence circularity is considered

		doctest:
		>>> ntseq('ATGTAAC').translate(start=6, end=27)
		aaseq(seq=HVTCNM*, id=None, descr=None, parent=ntseq(seq=ATGTAAC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=6, end=6)
		>>> ntseq('ATGTAAC').translate(start=5, strand=-1)
		>>> ntseq('ATGTAAC').translate(start=9, end=30, strand=-1)
		aaseq(seq=YMLHVTC, id=None, descr=None, parent=ntseq(seq=ATGTAAC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-2, start=5, end=5)
		>>> ntseq('ATGTAAC', circ=False).translate(start=2, strand=-1)
		aaseq(seq=Y, id=None, descr=None, parent=ntseq(seq=ATGTAAC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=False), parent_frame=-2, start=2, end=5)
		>>> ntseq('CTGTAAC').translate(mstart=True)
		aaseq(seq=M*, id=None, descr=None, parent=ntseq(seq=CTGTAAC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=0, end=6)
		'''

		if start is None:
			start = 0

		aa = "".join([self.alphabet.tr(x) for x in self.itercodons(start=start, end=end, strand=strand)])
		if not aa:
			return
		if mstart and self.has_altstart():
			aa = "M" + aa[1:]

		dist = len(aa)*3

		switch = True if strand == -1 else False
		if strand == 1:
			start =self.correct_coord(start)
			end = self.correct_coord(start+dist-1)+1
		else:
			end = self.correct_coord(start, switch=True)+1
			start = self.correct_coord(end-dist)

		return aaseq(seq=aa, id=self.id, parent=self, parent_frame=self.get_frame(start, strand), start=start, end=end)

	def permutate(self, keep=1):
		'''
		returns a ntseq based on the permutated sequence.

		doctest:
		>>> perm = ntseq('ATGTGAATGA').permutate()
		>>> perm.seq.count("A")
		4
		>>> perm = ntseq('ATGGGAATGA').permutate(3)
		>>> perm.seq.count("ATG")
		2
		>>> perm.seq.count("A")
		4
		'''
		if keep > 1:
			seqlist =  re.findall(r'.{1,' + str(keep) + '}', self.seq)
		else:
			seqlist = list(self.seq)
		shuffle(seqlist)
		return ntseq(seq="".join(seqlist), parent=self)


	def switch_strand(self, *pos):
		'''
		switches a position (0-based) on one strand to the other

		doctest:
		>>> ntseq('ATGTGA').switch_strand(5)
		0
		>>> ntseq('ATGTGA').switch_strand(2)
		3
		'''
		return self.seqlen-pos[0]%self.seqlen-1

	def has_start(self, frame=1):
		return self.alphabet.is_start(self.get_framed_seq(frame)[:3])

	def has_altstart(self, frame=1):
		return self.alphabet.is_altstart(self.get_framed_seq(frame)[:3])

	def has_stop(self, frame=1):
		return self.alphabet.is_stop(self.get_framed_seq(frame)[-3:])

	def iterorfs(self, *frames, stop_to_stop=False, all_variants=False, prefix='orf_', minlen=6):
		'''
		generator iterating over detected open reading frames that are returned as a new ntseq object
		*frames						frames to consider (INT)
		all_starts					consider any codon as potential start codon (BOOL), cannot be combined with all_variants
		all_variants				consider internal start codons as independent start (BOOL), cannot be combined with all_starts
		prefix						used in ORF id (STR)

		doctest (a bit paranoid):
		>>> [x for x in ntseq('ATGTGA').iterorfs()]
		[ntseq(seq=ATGTGA, id=orf_1, descr=None, parent=ntseq(seq=ATGTGA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=0, end=6, circ=False)]
		>>> [x for x in ntseq('GATGTAACG').iterorfs(2)]
		[ntseq(seq=ATGTAA, id=orf_1, descr=None, parent=ntseq(seq=GATGTAACG, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=2, start=1, end=7, circ=False)]
		>>> [x for x in ntseq('GGATGTAAC').iterorfs(3)]
		[ntseq(seq=ATGTAA, id=orf_1, descr=None, parent=ntseq(seq=GGATGTAAC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=3, start=2, end=8, circ=False)]
		>>> [x for x in ntseq('CCGTTACAT').iterorfs(-1)]
		[ntseq(seq=ATGTAA, id=orf_1, descr=None, parent=ntseq(seq=CCGTTACAT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-1, start=3, end=9, circ=False)]
		>>> [x for x in ntseq('CGTTACATC').iterorfs(-2)]
		[ntseq(seq=ATGTAA, id=orf_1, descr=None, parent=ntseq(seq=CGTTACATC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-2, start=2, end=8, circ=False)]
		>>> [x for x in ntseq('GTTACATCC').iterorfs(-3)]
		[ntseq(seq=ATGTAA, id=orf_1, descr=None, parent=ntseq(seq=GTTACATCC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-3, start=1, end=7, circ=False)]
		>>> [x for x in ntseq('TTAGTTACATCTTA').iterorfs()]
		[ntseq(seq=ATAAGATGTAACTAA, id=orf_1, descr=None, parent=ntseq(seq=TTAGTTACATCTTA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-2, start=0, end=1, circ=False), ntseq(seq=ATGTAA, id=orf_2, descr=None, parent=ntseq(seq=TTAGTTACATCTTA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-2, start=4, end=10, circ=False), ntseq(seq=ATCTTATTAGTTACATCTTATTAG, id=orf_3, descr=None, parent=ntseq(seq=TTAGTTACATCTTA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=3, start=8, end=4, circ=False), ntseq(seq=ATTAGTTACATCTTATTAGTTACATCTTATTAG, id=orf_4, descr=None, parent=ntseq(seq=TTAGTTACATCTTA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=2, start=13, end=4, circ=False)]
		>>> [x for x in ntseq('ATGTAAC').iterorfs()]
		[ntseq(seq=ATGTAA, id=orf_1, descr=None, parent=ntseq(seq=ATGTAAC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=0, end=6, circ=False)]
		>>> [x for x in ntseq('ATGATTC').iterorfs(2)]
		[]
		>>> [x for x in ntseq('ATGATTC').iterorfs()]
		[ntseq(seq=ATGATTCATGATTCATGA, id=orf_1, descr=None, parent=ntseq(seq=ATGATTC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=0, end=4, circ=False), ntseq(seq=ATCATGAATCATGAATCATGA, id=orf_2, descr=None, parent=ntseq(seq=ATGATTC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-3, start=5, end=5, circ=False)]
		>>> [x for x in ntseq('ATGATTC').iterorfs(all_variants=True)]
		[ntseq(seq=ATGATTCATGATTCATGA, id=orf_1.1, descr=None, parent=ntseq(seq=ATGATTC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=0, end=4, circ=False), ntseq(seq=ATTCATGATTCATGA, id=orf_1.2, descr=None, parent=ntseq(seq=ATGATTC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=3, end=4, circ=False), ntseq(seq=ATCATGAATCATGAATCATGA, id=orf_2.1, descr=None, parent=ntseq(seq=ATGATTC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-3, start=5, end=5, circ=False), ntseq(seq=ATGAATCATGAATCATGA, id=orf_2.2, descr=None, parent=ntseq(seq=ATGATTC, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-3, start=5, end=2, circ=False)]
		>>> [x for x in ntseq('ATGTGACA').iterorfs(1, 2, 3, all_variants=True)]
		[ntseq(seq=GTGACAATGTGA, id=orf_1.1, descr=None, parent=ntseq(seq=ATGTGACA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=3, start=2, end=6, circ=False), ntseq(seq=ATGTGA, id=orf_1.2, descr=None, parent=ntseq(seq=ATGTGACA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=0, end=6, circ=False)]
		>>> [x for x in ntseq('TGTCACAT').iterorfs(all_variants=True)]
		[ntseq(seq=GTGACAATGTGA, id=orf_1.1, descr=None, parent=ntseq(seq=TGTCACAT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-3, start=2, end=6, circ=False), ntseq(seq=ATGTGA, id=orf_1.2, descr=None, parent=ntseq(seq=TGTCACAT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-1, start=2, end=8, circ=False)]
		>>> [x for x in ntseq('AGGTAATGTCATCAACTTT').iterorfs()]
		[ntseq(seq=ATGTCATCAACTTTAGGTAATGTCATCAACTTTAGGTAA, id=orf_1, descr=None, parent=ntseq(seq=AGGTAATGTCATCAACTTT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=3, start=5, end=6, circ=False), ntseq(seq=ATCAACTTTAGGTAA, id=orf_2, descr=None, parent=ntseq(seq=AGGTAATGTCATCAACTTT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=2, start=10, end=6, circ=False), ntseq(seq=ATTACCTAA, id=orf_3, descr=None, parent=ntseq(seq=AGGTAATGTCATCAACTTT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-1, start=17, end=7, circ=False), ntseq(seq=TTGATGACATTACCTAAAGTTGATGACATTACCTAA, id=orf_4, descr=None, parent=ntseq(seq=AGGTAATGTCATCAACTTT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-2, start=17, end=15, circ=False)]
		>>> [x for x in ntseq('AGGTAATGTCATCAACTT').iterorfs(-1, -2, -3)]
		[ntseq(seq=ATTACCTAA, id=orf_1, descr=None, parent=ntseq(seq=AGGTAATGTCATCAACTT, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=-3, start=16, end=7, circ=False)]
		>>> [x for x in ntseq('AGGTAATGTCATCAACTT', circ=False).iterorfs()]
		[]
		>>> [x for x in ntseq('AGGTAATGTCATCAACTT').iterorfs(1)]
		[]
		>>> [x for x in ntseq('TGATGATTTATGAAATGA', circ=False).iterorfs(1, stop_to_stop=True)]
		[ntseq(seq=TTTATGAAATGA, id=orf_1, descr=None, parent=ntseq(seq=TGATGATTTATGAAATGA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=False), parent_frame=1, start=6, end=18, circ=False)]
		>>> [x for x in ntseq('ATGTGA').iterorfs()]
		[ntseq(seq=ATGTGA, id=orf_1, descr=None, parent=ntseq(seq=ATGTGA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=1, start=0, end=6, circ=False)]
		>>> [x for x in ntseq('ATGTGACA').iterorfs(1, 2, 3, all_variants=True, minlen=12)]
		[ntseq(seq=GTGACAATGTGA, id=orf_1, descr=None, parent=ntseq(seq=ATGTGACA, id=None, descr=None, parent=None, parent_frame=None, start=None, end=None, circ=True), parent_frame=3, start=2, end=6, circ=False)]
		'''
		#check args
		prefix=str(prefix)

		if not frames:
			frames = {-3, -2, -1, 1, 2, 3}
		else:
			self.check_frame(*frames)
			frames = set(frames)

		if minlen < 6:
	 		raise ValueError('minlen cannot be smaller than 6.')

		strands = set()
		if any([True if x > 0 else False for x in frames]):
			strands.add(1)
		if any([True if x < 0 else False for x in frames]):
			strands.add(-1)

		#check detection modes
		check_starts = any((not stop_to_stop, all_variants))

		#get coords
		stops = "(" + "|".join(self.alphabet.stops) + ")"
		starts = "(" + "|".join(self.alphabet.starts) + ")"
		first_start = re.compile(r"^(?:...)*?" + starts)
		last_start = re.compile(r"(?:...)*" + starts)
		first_stop = re.compile(r"^(?:...)*?" + stops)
		last_stop = re.compile(r"(?:...)*" + stops)
		coords = {}
		for frame in frames:
			#linear seq
			if not self.circ:
				seq = self.get_framed_seq(frame)
				if stop_to_stop:
					s = 0
					e = self.seqlen
				else:
					s = first_start.match(seq)
					if not s:
						continue
					e = last_stop.search(seq[:-self.seqmod])
					if not e:
						continue
					s = s.start(1)
					e = e.end(1)
					if s > e:
						continue
			#circ seq without "overhanging nucleotides"
			elif self.seqmod == 0:
				seq = self.get_framed_seq(frame)
				if stop_to_stop:
					s = first_stop.match(seq)
					if not s:
						s = 0
						e = self.seqlen
					else:
						s = s.end(1)
						e = self.seqlen + s
				else:
					s = first_start.match(seq)
					if not s:
						continue
					e = last_stop.search(seq)
					if not e:
						continue
					s = s.start(1)
					e = e.end(1)
					if s > e:
						continue
			#circ seq with "overhanging nucleotides"
			else:
				seq = self.get_framed_seq(frame)*3
				l = self.seqlen - self.seqmod
				if stop_to_stop:
					if seq[:3] in self.alphabet.stops:
						s = 3
					else:
						s = last_stop.search(seq)
						if s is None:
							s = 0
							e = self.seqlen
						else:
							s = s.end(1)
							e = first_stop.match(seq[l:])
							if e:
								e = l + e.end(1)
							else:
								e = self.seqlen*3 + s
					while e - self.seqlen < s:
						e += self.seqlen
				else:
					e = last_stop.search(seq)
					if not e:
						continue
					e = e.end(1)
					s = first_start.match(seq[e:] + seq[:e])
					if not s:
						continue
					s = s.start(1) - e
			while s > e:
				e += self.seqlen
			coords[frame] = (s+abs(frame)-1, e+abs(frame)-1)

		#detection
		if stop_to_stop:
			orfs = re.compile(r"[.+]+(?:\*|$)")
		else:
			orfs = re.compile(r"\+[.+]*\*")
		if check_starts:
			vstarts = re.compile(r"\+")


		all_orfs = set()
		seen = set()
		m = int(-(math.ceil(minlen/3)-1))
		for frame in coords:
			start = offset = coords[frame][0]
			end = coords[frame][1]
			strand = self.get_strand(frame)

			if end - start < minlen:
				continue

			seq = [x for x in self.itercodons(start=start, end=end, strand=strand)]
			lineup = "".join(["+" if x in self.alphabet.starts else "*" if x in self.alphabet.stops else "." for x in seq])
			seq = "".join(seq)

			for orf in orfs.finditer(lineup):
				orf_end = orf.end()*3
				orf_start = orf.start()*3

				if orf_end - orf_start < minlen:
					continue

				orf_seq = seq[orf_start:orf_end]

				this_end = self.correct_coord(offset+orf_end-1, strand=strand)+1
				this_lineup = orf.group()[1:m]

				id = str(this_end) + "|" + str(strand)

				orf_starts = [orf_start]
				if all_variants and this_lineup:
					orf_starts += [orf_start+(x.start()+1)*3 for x in vstarts.finditer(this_lineup)]

				all_orfs.update([(id, self.correct_coord(offset+x, strand), this_end, strand, orf_seq[x-orf_start:]) if strand == 1 else (id, this_end-1, self.correct_coord(offset+x, strand)+1, strand, orf_seq[x-orf_start:]) for x in orf_starts])

		if not all_orfs:
			return

		#ordering & report

		#build a dict (avoiding pandas because of speed)
		# orfs_dict: key = 'id', value = ['start', 'end', 'strand', 'seq']
		orfs_dict = defaultdict(set)
		if all_variants:
			while all_orfs:
				orf = all_orfs.pop()
				orfs_dict[orf[0]].add(orf[1:])
		else:
			all_orfs = sorted([x[1:] for x in all_orfs], key=lambda x: (x[1], -len(x[3])) if x[2] == 1 else (x[0], -len(x[3])))
			if len(all_orfs) > 1:
				idels = set([i for i, x in enumerate(all_orfs[1:], start=1) if (x[2] == 1 and all_orfs[i-1][2] == 1 and x[1] == all_orfs[i-1][1] and (x[0]-all_orfs[i-1][0])%3 == 0) or (x[2] == -1 and all_orfs[i-1][2] == -1 and x[0] == all_orfs[i-1][0] and (x[1]-all_orfs[i-1][1])%3 == 0)])
				all_orfs = [x for i, x in enumerate(all_orfs) if i not in idels]
			i = 0
			while all_orfs:
				orfs = orfs_dict
				orfs_dict[i].add(all_orfs.pop())
				i += 1

		#define orf order
		orf_order = [w[1] for w in sorted([(min([y[0] for y in x[1]] + [y[1] for y in x[1]]), x[0]) for x in orfs_dict.items()])]
		zerofill = len(str(len(orf_order)))
		for i, id in enumerate(orf_order, start=1):
			variants = sorted(orfs_dict[id], key=lambda x: len(x[3]), reverse=True)
			hid = prefix + str(i).zfill(zerofill)
			h = hid

			if len(variants) == 1:
				var = None
			else:
				var = True

			for v, variant in enumerate(variants, start=1):
				if var:
					h = hid + "." + str(v)
				if variant[2] == 1:
					frame = self.get_frame(variant[0])
				else:
					frame = self.get_frame(variant[1]-1, -1)
				yield ntseq(variant[3], id=h, descr=None, parent=self, parent_frame=frame, start=variant[0], end=variant[1], circ=False)



if __name__ == "__main__":
	import doctest
	doctest.testmod(verbose=False)
