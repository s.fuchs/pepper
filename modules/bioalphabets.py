#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import path

class dnaalphabet(object):
	def __init__(self, tt):
		self._tt = str(tt)
		self._yml = self.read_yaml()
		self._alphabet = None
		self._starts = None
		self._stops = None
		self._altstarts = None

	@property
	def tt(self):
		'''
		stores used codon table id (based on ncbi)
		'''
		return self._tt

	@property
	def alphabet(self):
		'''
		stores alphabet as dict (key: codon, val: aa)
		'''
		if self._alphabet is None:
			self._alphabet = {}
			for key, val in self._yml.items():
				self._alphabet[key] = val[0]
		return self._alphabet

	@property
	def starts(self):
		'''
		stores set of start codons of the selected alphabet.

		doctest:
		>>> sorted(dnaalphabet(11).starts)
		['ATA', 'ATC', 'ATG', 'ATT', 'CTG', 'GTG', 'TTG']
		'''
		if self._starts is None:
			self._starts = set([x for x in self._yml.keys() if self._yml[x][-1] == "+"])
		return self._starts

	@property
	def altstarts(self):
		'''
		stores set of alternative start codons of the selected alphabet.

		doctest:
		>>> sorted(dnaalphabet(11).altstarts)
		['ATA', 'ATC', 'ATT', 'CTG', 'GTG', 'TTG']
		'''
		if self._altstarts is None:
			self._altstarts = set([x for x in self._yml.keys() if self._yml[x][-1] == "+" and self._yml[x][0] != "M"])
		return self._altstarts

	@property
	def stops(self):
		'''
		stores set of stop codons of the selected alphabet.

		doctest:
		>>> sorted(dnaalphabet(11).stops)
		['TAA', 'TAG', 'TGA']
		'''
		if self._stops is None:
			self._stops = set([x for x in self._yml.keys() if self._yml[x] == "*"])
		return self._stops

	def read_yaml(self):
		'''
		stores yaml based alphabet config
		config file has to be named: codon_table/ncbi_{tt}.yml
		'''
		fname = path.join(path.dirname(path.abspath(__file__)), "codon_tables", "ncbi_" + self.tt + ".yml")
		if not path.isfile(fname):
			raise Error('ncbi translation table \'' + str(self.tt ) + '\' not implemented yet')
		data = {}
		with open(fname, "r") as handle:
			for line in handle:
				line = line.strip()
				if len(line) > 0 and line[0] != "#":
					fields = [x.strip() for x in line.split(":")]
					data[fields[0]] = fields[1]
		return data

	def tr(self, codon):
		'''
		translates a given codon to the respective aa based on the alphabet.

		doctest:
		>>> dnaalphabet(11).tr('ATG')
		'M'
		>>> dnaalphabet(11).tr('NTG')
		'X'
		>>> dnaalphabet(11).tr('TAG')
		'*'
		>>> dnaalphabet(11).tr('TA')
		''
		'''
		codon = codon.strip().upper()
		try:
			return self.alphabet[codon]
		except:
			if len(codon) != 3:
				return ""
			return "X"

	def is_stop(self, codon):
		return codon in self.stops

	def is_start(self, codon):
		return codon in self.starts

	def is_altstart(self, codon):
		return codon in self.altstarts

if __name__ == "__main__":
	import doctest
	doctest.testmod()
