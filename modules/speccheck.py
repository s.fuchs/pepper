#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import pandas as pd
from pandas.util import hash_pandas_object
import argparse
import os
import hashlib

version = "0.0.10"

class specchecker(object):
	def __init__(self, quiet = False):
		self.csvcols = {
			'Raw file': 'object',
			'Sequence': 'object',
			'Score': 'float64',
			'Matches': 'object',
			'Intensity coverage': 'float64',
			'Peptide ID': 'int64',
			'Mod. peptide ID': 'int64',
			'Evidence ID': 'int64',
		}
		self.report_col_order = [
			'Sequence',
			'Source file',
			'Raw file',
			'Specclass',
			'Intensity coverage',
			'Score',
			'y1',
			'y2',
			'b1',
			'b2',
			'Matches',
			'Peptide ID',
			'Mod. peptide ID',
			'Evidence ID']
		self.patterns= {'y': re.compile(r"y\d+"), 'b': re.compile(r"b\d+")}
		self.specdata = pd.DataFrame()
		self._minintcov = False
		self._classified = False
		self._bestfilter = False
		self.quiet = quiet

	def echo(self, *msg):
		'''
		as print but considers quiet mode
		'''
		if not self.quiet:
			print(*msg)

	@property
	def minintcov(self):
		'''
		returns minimal intensity coverage
		'''
		return self._minintcov

	@minintcov.setter
	def minintcov(self, minintcov):
		'''
		sets minimal intensity coverage (irreversible)
		Tip: If you set a threshold before multiple file import, memory is saved.
		'''
		self.echo("  minimal intensity coverage set to", minintcov)
		if self.minintcov and self.minintcov < minintcov and not self.specdata.empty:
			exit("error: minimal intensity coverage threshold cannot be applied since it is greater than the current threshold (" + str(self.minintcov) + ").")
		self._minintcov = minintcov

	def apply_minintcov(self, df):
		return df.loc[df["Intensity coverage"] >= self.minintcov]

	def set_minintcov(self, minintcov):
		'''
		sets the minimal intensity threshold and applies it if necessary.
		Tip: If you set a threshold before multiple file import, memory is saved.
		'''
		self.minintcov = minintcov

	def csv_to_df(self, fname, sep="\t"):
		'''
		reads msms csv and returns a panda dataframe
		only columns defined in init are considered
		if a threshold is set (self.minintcov), a minimal intensity coverage filter is applied
		'''
		if not os.path.isfile(fname):
			exit("error: " + fname + "does not exist.")
		df = pd.read_csv(fname, header=0, usecols=self.csvcols.keys(), dtype=self.csvcols, sep=sep)
		self._classified = False
		return df

	def hash_df(self, df):
		'''
		returns hash based on df content and structure
		'''
		return hashlib.sha256(pd.util.hash_pandas_object(df, index=True).values).hexdigest()

	def add_files(self, *fnames):
		'''
		imports one or more msms csv files
		'''
		dfs = []
		for fname in fnames:
			self.echo("  processing " + fname + "...")
			df = self.csv_to_df(fname)
			df['Source file'] = fname
			dfs.append(df)
		self.echo("  merging data ...")
		self.specdata = pd.concat(dfs, ignore_index=True).drop_duplicates().reset_index(drop=True)
		self._classified = False
		self._bestfilter = False

	def get_filenames(self):
		'''
		return imported file names
		'''
		if self.specdata.empty:
			return None
		return self.specdata['Source file'].unique()

	def apply_best_filter(self):
		'''
		groups spectrum data by sequence and reducs to "best spectrum" defined by (irreversible):
			- lowest spectrum class (highest weight)
			- max consecutive b1 or y1 ions
			- max score (lowest weight)
		'''
		self.is_classified(True)
		self.specdata = self.specdata.sort_values(['Specclass', 'max_ions', 'Score'], ascending=[True, False, False]).groupby('Sequence').head(1)

	def count_consecutive_ions(self, ionstr, iontype):
		'''
		give number of consecutive ions based on a given ionstring
		'''
		ion_positions = sorted(set([int(x) for x in [x[1:]for x in self.patterns[iontype].findall(ionstr)]]))
		consec = [[1]]
		for i in range(1, len(ion_positions)):
			if ion_positions[i] - 1 == ion_positions[i-1]:
				consec[-1].append(1)
			else:
				consec.append([1])
		return sorted([sum(x) for x in consec])

	def classify(self, keep_best_only=False):
		'''
		assigns spectrum classes
		per default, data is reduced to "best data row" per sequence only (keep_best_only)
		'''
		self.echo("  classifying (" + str(len(self.specdata.index)) + " spectra) ..."		)
		if self.specdata.empty:
			exit("error: no msms data imported yet.")
		self.specdata[['Intensity_coverage', 'y1', 'y2', 'b1', 'b2', 'max_ions', 'Specclass']] = self.specdata.apply(self.get_specdata, axis=1)
		self._classified = True
		if keep_best_only:
			self.apply_best_filter()
			self._bestfilter = True
		return True

	def get_specdata(self, row):
		'''
		assigns b/y ion data and spectrum classes
		'''
		ionstr = row['Matches']
		intcov = row['Intensity coverage']

		y_consecutive_list = [0, 0] + self.count_consecutive_ions(ionstr, "y")
		b_consecutive_list = [0, 0] + self.count_consecutive_ions(ionstr, "b")

		if self.minintcov and intcov < self.minintcov:
			specclass = 5
		elif y_consecutive_list[-1] > 4 or b_consecutive_list[-1] > 4:
			specclass = 1
		elif (
			(y_consecutive_list[-1] == 4 and y_consecutive_list[-2] == 4) or
			(b_consecutive_list[-1] == 4 and b_consecutive_list[-2] == 4)
			):
			specclass = 2
		elif (
			(y_consecutive_list[-1] == 4 and b_consecutive_list[-1] == 4)
			):
			specclass = 3
		else:
			specclass = 4
		return pd.Series([intcov, y_consecutive_list[-1], y_consecutive_list[-2], b_consecutive_list[-1], b_consecutive_list[-2], max(y_consecutive_list[-1], b_consecutive_list[-1]), specclass])

	def is_classified(self, die_if_not=False):
		if die_if_not and not self._classified:
			exit("error: spectrum data has to be classified first.")
		return self._classified

	def write_spec_report(self, fname):
		'''
		writes spectrum class report. Existing file will be overwritten!
		'''
		self.is_classified(True)
		self.echo("  writing spectrum class report to", fname)
		self.specdata[self.report_col_order].sort_values(['Sequence', 'Specclass']).to_csv(fname, index=False, sep='\t', encoding='utf-8')
		return True

	def get_class_dict(self):
		'''
		returns a dictionary (key: sequence, value: Spectrum class [1-4])
		'''
		self.is_classified(True)
		return dict(zip(self.specdata.Sequence, self.specdata.Specclass))

if __name__ == "__main__":
	parser = argparse.ArgumentParser(prog=os.path.basename(__file__), description='spectrum checker of pepper.', add_help=False)
	parser.add_argument('files', metavar="FILE", help="msms file(s)", type=argparse.FileType('r'), nargs="+")
	parser.add_argument('-i', metavar="FLOAT", help="minimal intensity coverage (default: 0.01)", type=float, default=0.01)
	parser.add_argument('--report', metavar="FILE", help="if set, a spectrum class report is generated. Please consider, that an existing file is overwritten!", type=str, default=False)
	parser.add_argument('--quiet', help="verbose output to STDOUT", action='store_true')
	parser.add_argument('--best', help="report best spectrum per sequence only", action='store_true')
	parser.add_argument('--version', action='version', version='%(prog)s ' + version)
	parser.add_argument("-h", "--help", action="help", help="show this help message and exit")
	args = parser.parse_args()
	if not args.quiet:
		print("checking ms/ms spectrum data ...")
	sc = specchecker(args.quiet)
	if args.i > 0:
		sc.set_minintcov(args.i)
	sc.add_files(*[x.name for x in args.files])
	sc.classify(args.best)
	if args.report:
		sc.write_spec_report(args.report)
