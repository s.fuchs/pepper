#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author Stephan Fuchs (fuchss@rki.de)
# This code is under MIT licence, you can find the complete licence here: https://gitlab.com/s.fuchs/pepper
__version_info__ = (1, 6, 1)
__version__ = ".".join(map(str, __version_info__))
