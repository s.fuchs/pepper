#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author Stephan Fuchs (fuchss@rki.de)
# This code is under a modified MIT licence, you can find the complete licence here: https://gitlab.com/s.fuchs/pepper

import pytest
import os
import subprocess
import filecmp
import shutil

inputfile = os.path.join(os.path.dirname(os.path.realpath(__file__)), "test", "sequence.fasta")
e2e_scenarios = [
			 ("test_salt_e2e_01", "--report nuc.fasta --nuc_fasta_style '{id} {coords}' -l 80 -i " + inputfile),
			 ("test_salt_e2e_02", '--report nuc.fasta --nuc_fasta_style "{id} {coords}" -l 0 --full -i ' + inputfile),
			 ("test_salt_e2e_03", '--report nuc.fasta --nuc_fasta_style "{id} {coords}" -l 0 --full --alt -i ' + inputfile),
			]

def is_apostroph(this, prev):
	if prev == "\\":
		return False
	if this == "'" or this == '"':
		return True

def check_protection(this, prev, protected):
	if is_apostroph(this, prev):
		if this == protected:
			return False
		else:
			return this
	else:
		return protected

def create_cmd(cmd):
	cmd_list = []
	protected = False
	elem = []
	apos = {'"', "'"}
	prev = "x"
	for letter in list(cmd):
		if letter == " " and not protected and len(elem) > 0:
			cmd_list.append("".join(elem))
			elem = []
		else:
			elem += letter
		protected = check_protection(letter, prev, protected)
		prev = letter
	if len(elem) > 0:
		cmd_list.append("".join(elem))
	return cmd_list

def run(cmd):
	proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
	stdout, stderr = proc.communicate()
	return proc.returncode, stdout, stderr

@pytest.mark.parametrize("testid, cmd", e2e_scenarios, ids=[x[0] for x in e2e_scenarios])
def test_e2e(tmpdir, testid, cmd):
	dirname = os.path.dirname(__file__)
	outbase = os.path.join(str(tmpdir), testid)
	prog = os.path.join(dirname, "salt.py")
	cmd = [prog, "--outbase", outbase] + create_cmd(cmd)
	errcode, stdout, stderr = run(cmd)
	out_file = outbase + ".nuc.fasta"
	template_file = os.path.join(dirname, "test", testid + ".nuc.fasta")
	filecheck = filecmp.cmp(out_file, template_file)
	print(filecheck)
	if errcode == 0 and filecheck == True:
		shutil.rmtree(str(tmpdir))
	assert errcode == 0
	assert filecheck == True

if __name__ == "__main__":
	pytest.main([os.path.realpath(__file__)])
