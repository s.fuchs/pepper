<img src="img/logo.png"  width="130" height="130" align="right"><br>
<img src="img/pepper.png" width="178" height="100" align="left"><br><br><br><br><br><br>

## Short Description
Pepper is part of the Salt & Pepper proteogenomics software suite and allows the identification
of open reading frames in a given genome sequence based on mass-spectrometically 
detected peptides.

## Installation
Information about installing Salt & Pepper can be found at [README.md](README.md).

## Quick Start
```bash
# basic pepper command
path/to/saltnpepper/pepper.py -r reference.gb -e evidence.txt
```

## Quick help
```bash
# show help page listing all available parameters
path/to/saltnpepper/pepper.py -h
```

## Advanced Options

### Grouping Multiple Evidence Files
Pepper can handle multiple evidence files as input. Evidence files can be combined into different evidence groups. Files in the same evidence group are handled like one single file.
```bash
# grouping evidence1.txt/evidence2.txt and evidence3.txt/evidence4.txt files
path/to/saltnpepper/pepper.py -r reference.gb -e evidence1.txt evidence2.txt -e evidence3.txt evidence4.txt
```

### Filtering PSMs
```bash
# only consider PSMs identified in at least 3 samples of evidence1.txt
path/to/saltnpepper/pepper.py --minsample 3 -r reference.gb -e evidence1.txt

# also consider PSMs without MS/MS spectra
path/to/saltnpepper/pepper.py --no_msms -r reference.gb -e evidence1.txt

# only consider PSMs whose sequence is listed in peps.txt (plain text file, one sequence per line)
path/to/saltnpepper/pepper.py --seqfilter peps.txt -r reference.gb -e evidence1.txt

# only consider PSMs with a posterior error of less than 0.2
path/to/saltnpepper/pepper.py --perrp 0.2  -r reference.gb -e evidence1.txt

# only consider PSMs with a Andromeda Score of more than 35
path/to/saltnpepper/pepper.py --score 35  -r reference.gb -e evidence1.txt

# only consider PSMs which map to one position in the reference genome only
path/to/saltnpepper/pepper.py --uniq_only -r reference.gb -e evidence1.txt

# only consider PSMs which map to one position in the reference genome only 
# or are part of the same protein family. In a protein family the longest member
# contains all other members with 100% sequence identity.
path/to/saltnpepper/pepper.py --uniq_only_smart -r reference.gb -e evidence1.txt

# filtering PSMs based on ms/ms spectral data in given msms export file(s) 
# only spectra with a minimal intensity coverage of 0.01 and a class < 4 
# are considered (see table below)
path/to/saltnpepper/pepper.py --msms ms_data.csv --minintcov 0.01 -r reference.gb -e evidence1.txt
```

Spectrum classes (referred from the highest scored spectrum for each PSM):

|class   |y1        |y2       |b1       |b2         |
|--------|----------|---------|---------|-----------|
|1       |>4        |n.c.     |n.c.     |n.c.       |
|1       |n.c.      |n.c.     |>4       |n.c.       |
|2       |4         |4        |n.c.     |n.c.       |
|2       |n.c.      |n.c.     |4        |4          |
|3       |4         |n.c.     |4        |n.c.       |

y1, b1: largest number of consecutive y/b ions

y2, b2: second largest number of consecutive y/b ions

n.c.: not considered

Spectra not meeting any of those criteria are assigned to class 4.

### Predicting ORFs
```bash
# consider non-tryptic peptides as N-terminal (Default)
path/to/saltnpepper/pepper.py --protease trypsin -r reference.gb -e evidence1.txt

# do not favor annotated ORFs
path/to/saltnpepper/pepper.py --detect_all_orfs -r reference.gb -e evidence1.txt

# not only include best ORF predictions but all potential ORFs in the gene bank output file
path/to/saltnpepper/pepper.py --gbk_all_orfs -r reference.gb -e evidence1.txt
```

### Screening Sequence Collections
```bash
# check ORF and peptide frequencies in sequences listed in the genus.fasta (fasta file, DNA sequences only)
path/to/saltnpepper/pepper.py --protease trypsin -r reference.gb -e evidence1.txt
```

### Speeding Up
```bash
# use 10 Threads/CPUs
path/to/saltnpepper/pepper.py -t 10 -r reference.gb -e evidence1.txt
```

## Output
### Genbank File (GB)
The generated genbank file is a modified version of the file used as a reference. In addition to the original annotation, it contains the following features:

|feature          |qualifiers                         |descriptions                                                                                             |
|-----------------|-----------------------------------|---------------------------------------------------------------------------------------------------------|
|best_orf         |annotated<br>transation<br>uniq_pep|predicted open reading frame with the highest coding potential according to Pepper's scoring heuristics  |
|best_orf_rbs     |seq                                |putative ribosomal binding site                                                                          |
|potential_orf    |annotated<br>transation<br>uniq_pep|alternatively predicted open reading frame                                                               |
|potential_orf_rbs|seq                                |putative ribosomal binding site                                                                          |
|exclusive_match  |pepid<br>translation               |DNA region encoding an identified peptide that does not match to any other region of the reference genome|
|match            |pepid<br>translation<br>exclusivity|DNA region encoding an identified peptide that does match to other regions of the reference genome, too  |
|TYPE_A           |-                                  |indicating a "best orf" that is not overlapping with any existing CDS annotation                         |
|TYPE_B           |-                                  |indicating a "best orf" that is overlapping with existing CDS annotation(s) in sense direction           |
|TYPE_C           |-                                  |indicating a "best orf" that is overlapping with existing CDS annotation(s) in antisense direction       |
|TYPE_D           |-                                  |indicating a "best orf" that is extending an existing CDS annotation                                     |
|TYPE_E           |-                                  |indicating a "best orf" that confirms an existing CDS annotation                                         |

The Genbank file can be used for intuitive data visualization using 3rd party software (here Geneious):

![geneplot](img/geneplot.png)

### Summary Table (TSV)
The tab-delimited text file contains all results ot the applied analyses. 
This file contains the following information:

**Identified peptide information**

|column header (1st line)                   |comment (2nd line)        |content                                                                 |
|-------------------------------------------|--------------------------|------------------------------------------------------------------------|
|#                                          |used pepper version       |row index number                                                        |
|peptide                                    |-                         |identified peptide sequence                                             |
|peptideID (pepper)                         |-                         |peptide ID assigned by pepper                                           |
|peptideID (evidence file *x*)              |name of evidence file *x* |peptide ID used in the respective evidence file                         |
|identification (evidence file *x*)         |name of evidence file *x* |inferred protein identification                                         |
|*sample* in evidence file *x* (MS/MS count)|name of evidence file *x* |number of correlated MS/MS spectra in the named sample and evidence file|
|predicted pI                               |-                         |theoretic isoelectric point of the identified peptide                   |
|predicted MW                               |-                         |theoretic molecular weight of the identified peptide                    |
|predicted GRAVY                            |-                         |theoretic grand average of hydropathy of the identified peptide         |
|DNA matches (total)                        |reference genome accession|number of coding sites in the reference genome                          |
|suggested match type                       |reference genome accession|suggested match type of the peptide based on all DNA matches            |

**DNA match information**

|column header (1st line)      |comment (2nd line)             |content                                                                                                |
|------------------------------|-------------------------------|-------------------------------------------------------------------------------------------------------|
|match sequence                |reference genome accession     |sequence of the DNA match                                                                              |
|match start                   |reference genome accession     |start coordinate of the DNA match                                                                      |
|match end                     |reference genome accession     |end coordinate of the DNA match                                                                        |
|match length                  |reference genome accession     |DNA match length in bp                                                                                 |
|match strand                  |reference genome accession     |strand the DNA match is encoded on                                                                     |
|match frame                   |reference genome accession     |frame the DNA match is encoded in                                                                      |
|aa upstream match             |reference genome accession     |amino acid encoded immediatly upstream of the DNA match                                                |
|match type                    |reference genome accession     |DNA match type                                                                                         |
|DNA match occurence in DB #*x*|name of sequence collection *x*|entries (e.g. genomes) in sequence collection *x* encoding the same DNA match (100% identity)          |
|peptide occurence in DB #*x*  |name of sequence collection *x*|entries (e.g. genomes) in sequence collection *x* encoding the same peptide match (100% identity)      |
|inframe overlapped gene(s)    |reference genome accession     |accessions of annotated gene(s) that are overlapped by the DNA match sharing the same reading frame    |
|inframe overlap length(s)     |reference genome accession     |overlap length(s) in bp                                                                                |
|%inframe overlap(s)           |reference genome accession     |relative overlap length(s) compared to the respective DNA match length                                 |
|outframe overlapped gene(s)   |reference genome accession     |accessions of annotated gene(s) that are overlapped by the DNA match not sharing the same reading frame|
|outframe overlap length(s)    |reference genome accession     |overlap length(s) in bp                                                                                |
|%outframe overlap(s)          |reference genome accession     |relative overlap length(s) compared to the respective DNA match length                                 |
|upstream distance             |reference genome accession     |bp distance between DNA match and the closest upstream gene annotated                                  |
|downstream distance           |reference genome accession     |bp distance between DNA match and the closest downstream gene annotated                                |
|synteny                       |reference genome accession     |text representation of the gene synteny                                                                |
|synteny annotation            |reference genome accession     |annotation of genes shown in the gene synteny                                                          |
|number predicted orfs         |reference genome accession     |number of predicted orfs covering the respective DNA match                                             |
|orf classes                   |reference genome accession     |classes of predicted orfs covering the respective DNA match                                            |
|orf classes                   |reference genome accession     |classes of predicted orfs covering the respective DNA match                                            |

**Information on ORFs with the highest coding potential ("*best orf*")**

|column header (1st line)                        |comment (2nd line)             |content                                                                                                  |
|------------------------------------------------|-------------------------------|---------------------------------------------------------------------------------------------------------|
|best potential ORF: start                       |reference genome accession     |start coordinate of the *best orf*                                                                       |
|best potential ORF: end                         |reference genome accession     |end coordinate of the DNA match                                                                          |
|best potential ORF: frame                       |reference genome accession     |frame the *best orf* is encoded in                                                                       |
|best potential ORF: type                        |reference genome accession     |*best orf* type                                                                                          |
|best potential ORF: gene synteny                |reference genome accession     |text representation of the gene synteny                                                                  |
|best potential ORF: gene synteny annotation     |reference genome accession     |annotation of genes shown in the gene synteny                                                            |
|best potential ORF: annotated                   |reference genome accession     |existing annotation (+/-)                                                                                |
|best potential ORF: length                      |reference genome accession     |*best orf* length in bp                                                                                  |
|best potential ORF: start codon                 |reference genome accession     |start codon of the *best orf*                                                                            |
|best potential ORF: potential RBS               |reference genome accession     |sequence of the putative ribosomal binding site (RBS)                                                    |
|best potential ORF: spacer length               |reference genome accession     |distance between putative RBS and the respective orf in bp (spacer)                                      |
|best potential ORF: start codon class           |reference genome accession     |assigned start codon class (1-3)                                                                         |
|best potential ORF: RBS class                   |reference genome accession     |assigned RBS class (1-3)                                                                                 |
|best potential ORF: spacer class                |reference genome accession     |assigned spacer class (1-2)                                                                              |
|best potential ORF: orf class                   |reference genome accession     |assigned orf class (1-21)                                                                                |
|best potential ORF: inframe overlaps            |reference genome accession     |accessions of annotated gene(s) that are overlapped by the *best orf* sharing the same reading frame     |
|best potential ORF: outframe overlaps           |reference genome accession     |accessions of annotated gene(s) that are overlapped by the *best orf*  not sharing the same reading frame|
|best potential ORF: mapped unique peptides      |reference genome accession     |number of unique peptides mapped to the respective *best orf*                                            |
|best potential ORF: DNA occurence in DB #*x*    |name of sequence collection *x*|entries (e.g. genomes) in sequence collection *x* encoding the same *best orf* sequence (100% identity)  |
|best potential ORF: peptide occurence in DB #*x*|name of sequence collection *x*|entries (e.g. genomes) in sequence collection *x* encoding the same *best orf* product (100% identity)   |
|best potential ORF: sequence                    |reference genome accession     |DNA sequence of the respective *best orf*                                                                |
|best potential ORF: upstream sequence           |reference genome accession     |DNA sequence upstream of the respective *best orf* (-1 to -21)                                           |
|best potential ORF: translation                 |reference genome accession     |amino acid sequence of the *best orf* product                                                            |
|best potential ORF: predicted pI                |reference genome accession     |theoretic isoelectric point of the*best orf* product                                                     |
|best potential ORF: predicted MW                |reference genome accession     |theoretic molecular weight of the *best orf* product                                                     |
|best potential ORF: predicted GRAVY             |reference genome accession     |theoretic grand average of hydropathy of the *best orf* product                                          |

**Information on alternative ORFs ("*potential orf*")**

|column header (1st line)                        |comment (2nd line)             |content                                                                                                  |
|------------------------------------------------|-------------------------------|---------------------------------------------------------------------------------------------------------|
|best potential ORF: start                       |reference genome accession     |start coordinate of the *potential orf*                                                                       |
|best potential ORF: end                         |reference genome accession     |end coordinate of the DNA match                                                                          |
|best potential ORF: frame                       |reference genome accession     |frame the *potential orf* is encoded in                                                                       |
|best potential ORF: type                        |reference genome accession     |*potential orf* type                                                                                          |
|best potential ORF: gene synteny                |reference genome accession     |text representation of the gene synteny                                                                  |
|best potential ORF: gene synteny annotation     |reference genome accession     |annotation of genes shown in the gene synteny                                                            |
|best potential ORF: annotated                   |reference genome accession     |existing annotation (+/-)                                                                                |
|best potential ORF: length                      |reference genome accession     |*potential orf* length in bp                                                                                  |
|best potential ORF: start codon                 |reference genome accession     |start codon of the *potential orf*                                                                            |
|best potential ORF: potential RBS               |reference genome accession     |sequence of the putative ribosomal binding site (RBS)                                                    |
|best potential ORF: spacer length               |reference genome accession     |distance between putative RBS and the respective orf in bp (spacer)                                      |
|best potential ORF: start codon class           |reference genome accession     |assigned start codon class (1-3)                                                                         |
|best potential ORF: RBS class                   |reference genome accession     |assigned RBS class (1-3)                                                                                 |
|best potential ORF: spacer class                |reference genome accession     |assigned spacer class (1-2)                                                                              |
|best potential ORF: orf class                   |reference genome accession     |assigned orf class (1-21)                                                                                |
|best potential ORF: inframe overlaps            |reference genome accession     |accessions of annotated gene(s) that are overlapped by the *potential orf* sharing the same reading frame     |
|best potential ORF: outframe overlaps           |reference genome accession     |accessions of annotated gene(s) that are overlapped by the *potential orf*  not sharing the same reading frame|
|best potential ORF: mapped unique peptides      |reference genome accession     |number of unique peptides mapped to the respective *potential orf*                                            |
|best potential ORF: DNA occurence in DB #*x*    |name of sequence collection *x*|entries (e.g. genomes) in sequence collection *x* encoding the same *potential orf* sequence (100% identity)  |
|best potential ORF: peptide occurence in DB #*x*|name of sequence collection *x*|entries (e.g. genomes) in sequence collection *x* encoding the same *potential orf* product (100% identity)   |
|best potential ORF: sequence                    |reference genome accession     |DNA sequence of the respective *potential orf*                                                                |
|best potential ORF: upstream sequence           |reference genome accession     |DNA sequence upstream of the respective *potential orf* (-1 to -21)                                           |
|best potential ORF: translation                 |reference genome accession     |amino acid sequence of the *potential orf* product                                                            |
|best potential ORF: predicted pI                |reference genome accession     |theoretic isoelectric point of the*potential orf* product                                                     |
|best potential ORF: predicted MW                |reference genome accession     |theoretic molecular weight of the *potential orf* product                                                     |
|best potential ORF: predicted GRAVY             |reference genome accession     |theoretic grand average of hydropathy of the *potential orf* product                                          |

### Spectrum Class Report (TSV)
When option `--msms` is set a spectrum class report is generated (file ending with .specclasses.tsv) containing the following information:

| column             | content                                                                                               |
| ------------------ | ----------------------------------------------------------------------------------------------------- |
| Sequence           | peptide sequence                                                                                      |
| Source file        | file name spectral data derived from                                                                  |
| Raw file           | raw file name                                                                                         |
| Specclass          | assigned spectrum class  (s. section 'Filtering PSMs' for more information on classes)                |
| Intensity Coverage | intensity coverageof the spectrum  (minimal intensity coverage threshold is defined by `--minintcov`) |
| Score              | spectrum score                                                                                        |
| y1                 | largest number of consecutive y ions                                                                  |
| y2                 | second largest number of consecutive y ions                                                           |
| b1                 | largest number of consecutive b ions                                                                  |
| b2                 | second largest number of consecutive b ions                                                           |
| Matches            | match string                                                                                          | 
| Peptide ID         | peptide ID                                                                                            |
| Mod. peptide ID    | Modified peptide ID                                                                                   |
| Evidence ID        | Evidence ID                                                                                           | 

### Log file (LOG)
The log file contains information on the provided input files and used parameters. Additionally, the analysis workflow is described in detail.
