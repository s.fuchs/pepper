<img src="img/logo.png"  width="130" height="130" align="right"><br>
<img src="img/salt.png" width="178" height="100" align="left"><br><br><br><br><br><br>

## Short Description
Salt is part of the Salt & Pepper proteogenomics suite and allows the extraction 
of potential coding sequences (open reading frames = orfs) from a given genome 
sequence. Extracted orfs can be translated according to NCBI's translation 
table 11 (bacteria). The resulting protein sequences can be further 
digested into single peptides *in silico* using predefined enzymes such as trypsin 
or chymotrypsin. Additionally, feature tables containing physicochemical properties
of the respective sequences (e.g. molecular weight, isoelectric point, ...) can 
be created based on all sequences.

## Installation

Information about installing Salt & Pepper can be found at [README.md](README.md).

## Quick Start
```bash
# basic pepper command to retrieve potential protein sequences as fasta file
path/to/saltnpepper/salt.py -i reference.fasta
```

## Quick help
```bash
# show help page listing all available parameters
path/to/saltnpepper/salt.py -h
```

## Input
Salt expects a single genome sequence in the form of a fasta or genbank file. 

```bash
# import a circular genomic sequence from a fasta file
path/to/saltnpepper/salt.py -i reference.fasta

# import a circular genomic sequence from a genbank file
path/to/saltnpepper/salt.py -i reference.gb --intype genbank

# import a linear genomic sequence from a fasta file
path/to/saltnpepper/salt.py --linear -i reference.fasta
```

## Output
Different result reports can be generated, which in turn determine the scope of
required calculations (see table below). Generally, Salt allows the generation
of sequence collections in form of fasta files (fasta) and/or feature tables in form
of tab-delimited text files (txt). Ouput stlyes can be defined to customize fasta 
headers or feature table content (see Advanced Options below)

| report      | type of generated sequences | output format | required calculations                             |
|-------------|-----------------------------|---------------|---------------------------------------------------|
| nuc.fasta   | nucleotides (orfs)          | fasta         | orf detection                                     |
| nuc.txt     | nucleotides (orfs)          | txt           | orf detection                                     |
| prot.fasta* | amino acids (proteins)      | fasta         | orf detection, orf translation                    |
| prot.txt    | amino acids (proteins)      | txt           | orf detection, orf translation                    |
| pep.fasta   | amino acids (peptides)      | fasta         | orf detection, orf translation, protein digestion |
| pep.txt     | amino acids (peptides)      | txt           | orf detection, orf translation, protein digestion |

*default

```bash
# create a fasta file of detected orf sequences
path/to/saltnpepper/salt.py --report nuc.fasta -i reference.fasta

# create a fasta file and feature table of translated orf sequences
path/to/saltnpepper/salt.py --report prot.fasta prot.txt -i reference.fasta

# create a feature table of tryptic peptides derived from translated orf sequences
path/to/saltnpepper/salt.py --report pep.txt prot.txt -i reference.fasta

# create all available reports
path/to/saltnpepper/salt.py --report all -i reference.fasta
```

## Advanced Options

### Orf Detection
```bash
# consider only orfs with a minimal length of 21bp
path/to/saltnpepper/salt.py --nuclen 21 -i reference.fasta

# consider full coding potential (orfs defined from stop to stop codon)
path/to/saltnpepper/salt.py --full -i reference.fasta

# consider interior start codons (results in orf variants)
path/to/saltnpepper/salt.py --alt -i reference.fasta

# use 'sacol_' as prefix for orf sequence identifier
path/to/saltnpepper/salt.py -p sacol_
```

### Translation
```bash
# translate only plus strand
path/to/saltnpepper/salt.py --frames 1 2 3 -i reference.fasta

# translate alternative start codons as methionine
path/to/saltnpepper/salt.py --m1 -i reference.fasta 

# translate alternative starts as both methionine and literal translation)
path/to/saltnpepper/salt.py --m2 -i reference.fasta
```

### Protein Digestion
Translated protein sequences can be digested *in silico* by the following 
proteases.

| protease     | cleavage site                            |
|--------------|------------------------------------------|
| trypsin*     | after K or R if not followed by P        |
| lys-c        | after K if not followed by P             |
| arg-c        | after R if not followed by P or after KK |
| glu-c        | after D or E                             |
| asp-n        | before D or C                            |
| chymotrypsin | after W, Y, F or L                       |
| proteinasek  | after any amino acid                     |

*default

```bash
# perform in silico digestion by Asp-c
path/to/saltnpepper/salt.py --protease asp-c -i reference.fasta
# allow up to two miscleavages by glu-c
path/to/saltnpepper/salt.py --mc 2 --protease glu-c -i reference.fasta
# consider only tryptic peptides with a minimal length of 6 amino acids
path/to/saltnpepper/salt.py --peplen 6 -i reference.fasta
```

### Sequence Permutation

```bash
# shuffle nucleotides of input sequence (before orf detection)
path/to/saltnpepper/salt.py --inperm

# shuffle codons of input sequence (before orf detection)
path/to/saltnpepper/salt.py --incodonperm

# shuffle nucleotide of detected orfs (before translation)
path/to/saltnpepper/salt.py --nucperm 

# shuffle codons of detected orfs (before translation)
path/to/saltnpepper/salt.py --codonperm 

# shuffle amino acids of translated orfs (before digestion)
path/to/saltnpepper/salt.py --protperm

# shuffle amino acids of peptides from digestion
path/to/saltnpepper/salt.py --pepperm

# allow up to two miscleavages by glu-c
path/to/saltnpepper/salt.py --mc 2 --protease glu-c -i reference.fasta

# consider only tryptic peptides with a minimal length of 6 amino acids
path/to/saltnpepper/salt.py --peplen 6 -i reference.fasta
```

### Output Styles
Output styles can be used to customize fasta headers and/or feature table 
contents. A style is arbitrary string containing defined tags that will be 
replaced by respective values.

| tag      | substitute value                            | restriction                        |
|----------|---------------------------------------------|------------------------------------|
| {id}     | sequence id                                 |                                    |
| {descr}  | sequence description                        |                                    |
| {start}  | start coordinate*                           |                                    |
| {end}    | end coordinate*                             |                                    |
| {coords} | coordinate string*                          |                                    |
| {strand} | coding strand*                              |                                    |
| {frame}  | coding frame*                               |                                    |
| {parent} | id of the parental sequence                   |                                    |
| {origin} | id of the initial sequence                  |                                    |  
| {seqlen} | sequence length                             |                                    |
| {seguid} | seguid sequence hash                        |                                    |
| {pi}     | isoelectric point                           | amino acid sequences only          |
| {mw}     | molecular weight                            |                                    |
| {gravy}  | grand average of hydropathy                 | amino acid sequences only          |
| {seq*}   | sequence (including translated stop codons) | amino acid sequences only          |
| {seq}    | sequence (without translated stop codons)   |                                    |
| {digest} | protease used for *in silico* digestion     | digested amino acid sequences only |

 *related to parental sequence
 
 Individual styles can be defined for each report using respective arguments.
 
 | report     | style argument       | default style*                                                                                                    |
 |------------|----------------------|-------------------------------------------------------------------------------------------------------------------|
 | nuc.fasta  | `--nuc_fasta_style`  | `{id}` [coords=`{coords}`][frame=`{frame}`][origin=`{origin}`]                                                    |
 | nuc.txt    | `--nuc_txt_style`    | `{id}`\t`{coords}`\t`{seqlen}`\t`{frame}`\t`{origin}`\t`{seguid}`\t`{seq}`                                        |
 | prot.fasta | `--prot_fasta_style` | `{id}` [coords=`{coords}`][parent=`{parent}`][origin=`{origin}`]                                                  |
 | prot.txt   | `--prot_txt_style`   | `{id}`\t`{coords}`\t`{seqlen}`\t`{frame}`\t`{parent}`\t`{origin}`\t`{mw}`\t`{pi}`\t`{gravy}`\t`{seguid}`\t`{seq}` |
 | pep.fasta  | `--pep_fasta_style`  | `{id}` [digest=`{digest}`][coords=`{coords}`][parent=`{parent}`][origin=`{origin}`]                               |
 | pep.txt    | `--pep_txt_style`    | `{id}`\t`{coords}`\t`{seqlen}`\t`{parent}`\t`{origin}`\t`{digest}`\t`{seguid}`\t`{seq}`                           |
 
 *'\t' stands for tab
 
 ```bash
# use sequence id and seguid in fasta header of orf sequences
path/to/saltnpepper/salt.py --nuc_fasta_style '{id} {seguid}' --report nuc.fasta

# create a csv listing  sequence id, parental id, mw, pi and gravy for tryptic peptides
path/to/saltnpepper/salt.py --pep_txt_style '{id},{parent},{mw},{pi},{gravy}' --report pep.txt
```






