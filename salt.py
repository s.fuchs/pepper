#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author Stephan Fuchs (fuchss@rki.de)
# This code is under MIT licence, you can find the complete licence here: https://gitlab.com/s.fuchs/pepper

import hashlib
import os
import sys
import argparse
import threading
import concurrent.futures
import re
import copy

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "modules"))
import bioseqs
from _version import __version_info__, __version__

BUFFER_SIZE = 1000  # number of lines to buffer before writing to disk

class salt(object):
    def __init__(self, fname, outbase="salted_", tt=11, circ=True, format="fasta", debug=False, cpus=1):
        # Allowed report types
        self.allowed_reports = {'nuc.fasta', 'nuc.txt', 'prot.fasta', 'prot.txt', 'pep.fasta', 'pep.txt', 'config.txt'}
        self.outbase = outbase
        # Dictionary of open file handles (only created for requested reports)
        self._reports = {x: None for x in self.allowed_reports}
        # Styles for each report type
        self._styles = {x: None for x in self.allowed_reports}
        # Buffer for each report to reduce I/O calls
        self._buffers = {x: [] for x in self.allowed_reports}
        
        # Debug mode
        self.debug = debug
        self.debugger = []
        # Threading lock for safe buffer updates
        self._lock = threading.Lock()

        # Input: note that bioseqs.ntseq.fromfile should be implemented in a streaming/lazy way.
        self.fname = fname
        self.dnaobj = bioseqs.ntseq.fromfile(fname, tt=tt, circ=circ, format=format)

        # Workflow flags
        self._translate = None
        self._digest = None
        self.config = {}

    def iter_active_report_types(self):
        for rep_type, handle in self._reports.items():
            if handle:
                yield rep_type

    def check_workflow(self):
        reportstr = "_".join([x for x in self._reports if self._reports[x]])
        if "pep." in reportstr:
            self._translate = True
            self._digest = True
        elif "prot." in reportstr:
            self._translate = True

    def add_report(self, rep_type, style):
        if rep_type not in self.allowed_reports:
            raise ValueError("unknown report type")
        if self.debug and sum(1 for x in self._reports.values() if x) > 0:
            raise ValueError("debug mode allows only one report type added")
        filename = self.outbase + "." + rep_type
        self._reports[rep_type] = open(filename, "w")
        self._styles[rep_type] = style

    def flush_buffer(self, rep_type):
        """Flush the buffer for a given report type to its file."""
        if self._reports[rep_type] and self._buffers[rep_type]:
            self._reports[rep_type].write("".join(self._buffers[rep_type]))
            self._buffers[rep_type] = []

    def remove_report(self, rep_type):
        if self._reports[rep_type]:
            self.flush_buffer(rep_type)
            self._reports[rep_type].close()
            self._reports[rep_type] = None
            self._styles[rep_type] = None

    def remove_all_reports(self):
        for rep_type in list(self.iter_active_report_types()):
            self.remove_report(rep_type)

    def orf_worker(self, orfobj):
        # Process nucleotide permutation if requested.
        if self.config.get('nucperm'):
            orfobj = orfobj.permutate()
            orfobj.id = orfobj.parent.id + "_perm"
        elif self.config.get('codonperm'):
            orfobj = orfobj.permutate(3)
            orfobj.id = orfobj.parent.id + "_codonperm"
        self.write_entry(orfobj, "nuc")
        if self._translate:
            for protobj in self.iterprots(orfobj, variants=self.config.get('m_init', 0)):
                self.prot_worker(protobj)

    def prot_worker(self, protobj):
        if self.config.get('protperm'):
            protobj = protobj.permutate()
            protobj.id = protobj.parent.id + "_perm"
        self.write_entry(protobj, "prot")
        if self._digest:
            for pepobj in self.iterpeps(protobj, protease=self.config.get('protease'),
                                         miscleav=self.config.get('mc'),
                                         minlen=self.config.get('minlen_pep')):
                self.pep_worker(pepobj)

    def pep_worker(self, pepobj):
        if self.config.get('pepperm'):
            pepobj = pepobj.permutate()
            pepobj.id = pepobj.parent.id + "_perm"
        self.write_entry(pepobj, "pep")

    def write_report_headlines(self):
        # For text reports, write the header line once.
        for rep_type, handle in self._reports.items():
            if handle and rep_type.endswith(".txt"):
                header = self._styles[rep_type].replace("{", "").replace("}", "") + "\n"
                self.shout(header, rep_type)

    def write_entry(self, obj, level):
        fasta_key = level + '.fasta'
        txt_key = level + '.txt'
        if self._reports.get(fasta_key):
            out_str = obj.tofasta(self._styles[fasta_key], seqwrap=self.config.get('seqwrap'),
                                   star=self.config.get('star')) + "\n"
            self.shout(out_str, fasta_key)
        if self._reports.get(txt_key):
            out_str = obj.tostr(self._styles[txt_key]) + "\n"
            self.shout(out_str, txt_key)

    def create(self, *frames, cpus=1, seqwrap=0, protease="trypsin", star=False, mc=0, minlen_nuc=6,
               minlen_pep=1, prefix="orf_", alt_starts=False, stop_to_stop=False, m_init=0,
               nucperm=False, codonperm=False, protperm=False, pepperm=False, cut_N=10):
        # Save configuration
        self.config['seqwrap'] = seqwrap
        self.config['protease'] = protease
        self.config['star'] = star
        self.config['mc'] = mc
        self.config['minlen_nuc'] = minlen_nuc
        self.config['minlen_pep'] = minlen_pep
        self.config['prefix'] = prefix
        self.config['m_init'] = m_init
        self.config['nucperm'] = nucperm
        self.config['codonperm'] = codonperm  # Correctly assign codonperm flag
        self.config['protperm'] = protperm
        self.config['pepperm'] = pepperm

        # Write config file if requested.
        if self._reports.get("config.txt"):
            with open(self.outbase + ".config.txt", "w") as handle:
                for param, val in self.config.items():
                    handle.write(f"{param}\t{val}\n")

        # If no reports were added, nothing to do.
        if not any(self._reports.values()):
            return

        self.check_workflow()
        self.write_report_headlines()

        # Process each ORF from the input in a streaming (generator) fashion.
        orf_iterator = self.iterorfs(*frames, minlen=minlen_nuc, prefix=prefix,
                                     all_variants=alt_starts, stop_to_stop=stop_to_stop, cut_N=cut_N)

        if cpus > 1:
            # Use ThreadPoolExecutor to process ORFs in parallel.
            with concurrent.futures.ThreadPoolExecutor(max_workers=cpus) as executor:
                for _ in executor.map(self.orf_worker, orf_iterator):
                    for rep in self.iter_active_report_types():
                        if len(self._buffers[rep]) >= BUFFER_SIZE:
                            self.flush_buffer(rep)
        else:
            for orfobj in orf_iterator:
                self.orf_worker(orfobj)
                for rep in self.iter_active_report_types():
                    if len(self._buffers[rep]) >= BUFFER_SIZE:
                        self.flush_buffer(rep)
        # Flush any remaining buffered output.
        for rep in self.iter_active_report_types():
            self.flush_buffer(rep)
        self.remove_all_reports()

    def shout(self, x, report):
        """Buffer the output string 'x' for the given report type, or save to debugger in debug mode."""
        if self.debug:
            self.debugger.append(x)
        elif self._reports.get(report):
            with self._lock:
                self._buffers[report].append(x)

    def iterorfs(self, *frames, minlen, prefix, all_variants, stop_to_stop, cut_N=10):
        """
        Wrap the bioseqs ntseq.iterorfs generator.
        If the ORF's sequence contains a stretch of 'N' (ambiguous nucleotide)
        of length >= cut_N, split the ORF at that region so that no yielded ORF spans it.
        If cut_N <= 0, no splitting is performed.
        """
        for orfobj in self.dnaobj.iterorfs(*frames, minlen=minlen, prefix=prefix,
                                           all_variants=all_variants, stop_to_stop=stop_to_stop):
            # Only split if cut_N > 0 and the ORF has a sequence attribute.
            if cut_N > 0 and hasattr(orfobj, 'seq'):
                seq = orfobj.seq
                # Split the sequence on stretches of 'N' (case insensitive) of length >= cut_N.
                parts = re.split(r'N{' + str(cut_N) + r',}', seq.upper())
                if len(parts) > 1:
                    for i, part in enumerate(parts):
                        if len(part) >= minlen:
                            try:
                                new_orf = orfobj.copy()
                            except AttributeError:
                                new_orf = copy.copy(orfobj)
                            # Try to set the sequence via the property.
                            try:
                                object.__setattr__(new_orf, 'seq', part)
                            except AttributeError:
                                # Fallback: set the internal attribute (adjust if necessary).
                                object.__setattr__(new_orf, '_seq', part)
                            new_orf.id = orfobj.id + f"_cut{i+1}"
                            yield new_orf
                    continue
            yield orfobj

    def itertranslations(self, orfobj, m):
        # Yield translation(s) based on whether alternative start codons are available.
        if not orfobj.has_altstart() or m in (0, 2):
            yield orfobj.translate(mstart=False)
        elif m > 0 and orfobj.has_altstart():
            t = orfobj.translate(mstart=True)
            t.id += "_M"
            yield t

    def iterprots(self, orfobj, variants):
        for protobj in self.itertranslations(orfobj, variants):
            yield protobj

    def iterpeps(self, protobj, **kwargs):
        for pepobj in protobj.iterdigest(**kwargs):
            yield pepobj

if __name__ == "__main__":

    def get_args():
        parser = argparse.ArgumentParser(
            prog="salt.py",
            description='SALT: A pipeline for simple ORF extraction from bacterial genomes',
            add_help=False
        )

        in_opt = parser.add_argument_group('1. Input Options')
        in_opt.add_argument('-i', '--input', metavar="FILE",
                            help="input file containing exactly one genome sequence (required)",
                            type=argparse.FileType('r'), required=True)
        in_opt.add_argument('--intype', metavar="STR", help="input file type (default: fasta)",
                            choices=['fasta', 'genbank'], default='fasta')
        in_opt.add_argument('--linear', help="input sequence is not circular", action="store_true")

        ex_opt = parser.add_argument_group('2. Extraction Options')
        ex_opt.add_argument('--frames', metavar="INT", help="reading frames to consider (default: all)",
                            type=int, nargs='+', default=[1, 2, 3, -1, -2, -3])
        ex_opt.add_argument('--nuclen', metavar="INT",
                            help="minimal length of nucleotide sequences to consider [nt] (default: 6)",
                            type=int, default=6)
        ex_opt.add_argument('--full', help="perform full (stop-to-stop) extraction", action="store_true")
        ex_opt.add_argument('--alt', help="consider interior starts", action="store_true")
        # --cut_N option: set to -1 to disable splitting on ambiguous N's.
        ex_opt.add_argument('--cut_N', metavar="INT",
                            help="do not span stretches of ambiguous 'N's of length INT (default: 10; use -1 to disable)",
                            type=int, default=10)
        m_group = ex_opt.add_mutually_exclusive_group()
        m_group.add_argument('--m1', help="translate all alternative starts as Methionine", action="store_true")
        m_group.add_argument('--m2', help="translate all alternative starts as Methionine but keep also genuine translation as variant", action="store_true")

        dig_opt = parser.add_argument_group('3. Digestion Options')
        dig_opt.add_argument('--protease', metavar="CHOICE",
                             help="protease used to digest translated sequences (default: trypsin). Allowed are: 'trypsin', 'lys-c', 'arg-c', 'glu-c', 'asp-n', 'chymotrypsin', 'proteinasek'",
                             choices=['trypsin', 'lys-c', 'arg-c', 'glu-c', 'asp-n', 'chymotrypsin', 'proteinasek'], default='trypsin')
        dig_opt.add_argument('--mc', metavar="INT",
                             help="number of allowed miscleavages (default: 0)",
                             type=int, default=0)
        dig_opt.add_argument('--peplen', metavar="INT",
                             help="minimal length of peptides to be considered [aa] (default: 1)",
                             type=int, default=1)

        perm_opt = parser.add_argument_group('4. Permutation Options')
        p_group = perm_opt.add_mutually_exclusive_group()
        p_group.add_argument('--inperm', help="permutate letters in input sequence", action="store_true")
        p_group.add_argument('--incodonperm', help="permutate codons in input sequence", action="store_true")
        p_group.add_argument('--nucperm', help="permutate letters in nucleotide sequences", action="store_true")
        p_group.add_argument('--codonperm', help="permutate codons in nucleotide sequences", action="store_true")
        perm_opt.add_argument('--protperm', help="permutate letters in translated sequences", action="store_true")
        perm_opt.add_argument('--pepperm', help="permutate letters in digested sequences", action="store_true")

        out_opt = parser.add_argument_group('5. Output Options')
        out_opt.add_argument('-p', metavar="STR", help="prefix used for ORF accessions (default: orf_)",
                             type=str, default="orf_")
        out_opt.add_argument('--report', metavar="CHOICE",
                             help="output type(s) (default: prot.fasta). Allowed are: 'nuc.fasta', 'prot.fasta', 'pep.fasta', 'nuc.txt', 'prot.txt', 'pep.txt', 'config.txt', 'all'",
                             choices=['nuc.fasta', 'prot.fasta', 'pep.fasta', 'nuc.txt', 'prot.txt', 'pep.txt', 'config.txt', 'all'],
                             default=['prot.fasta'], nargs='+')
        out_opt.add_argument('--outbase', metavar="STR",
                             help="output basename (default: salted_<orig_basename>). Please note: Existing files will be overwritten.",
                             type=str, default="salted_")
        out_opt.add_argument('--nuc_fasta_style', metavar="STR",
                             help="style used for fasta headers of nucleotide sequences (default: '{id} [coords={coords}][frame={frame}][origin={origin}]')",
                             type=str, default='{id} [coords={coords}][frame={frame}][origin={origin}]')
        out_opt.add_argument('--prot_fasta_style', metavar="STR",
                             help="style used for fasta headers of translated sequences (default: '{id} [coords={coords}][parent={parent}][origin={origin}]')",
                             type=str, default='{id} [coords={coords}][parent={parent}][origin={origin}]')
        out_opt.add_argument('--pep_fasta_style', metavar="STR",
                             help="style used for fasta headers  of digested sequences (default: '{id} [digest={digest}][coords={coords}][parent={parent}][origin={origin}]')",
                             type=str, default='{id} [digest={digest}][coords={coords}][parent={parent}][origin={origin}]')
        out_opt.add_argument('--nuc_txt_style', metavar="STR",
                             help="style used for text files containing nucleotide sequences (default: '{id}\t{coords}\t{seqlen}\t{frame}\t{origin}\t{seguid}\t{seq}')",
                             type=str, default='{id}\t{coords}\t{seqlen}\t{frame}\t{origin}\t{seguid}\t{seq}')
        out_opt.add_argument('--prot_txt_style', metavar="STR",
                             help="style used for text files containing translated sequences (default: '{id}\t{coords}\t{seqlen}\t{frame}\t{parent}\t{origin}\t{mw}\t{pi}\t{gravy}\t{seguid}\t{seq}')",
                             type=str, default='{id}\t{coords}\t{seqlen}\t{frame}\t{parent}\t{origin}\t{mw}\t{pi}\t{gravy}\t{seguid}\t{seq}')
        out_opt.add_argument('--pep_txt_style', metavar="STR",
                             help="style used for text files containing digested sequences (default: '{id}\t{coords}\t{seqlen}\t{frame}\t{parent}\t{origin}\t{digest}\t{seguid}\t{seq}')",
                             type=str, default='{id}\t{coords}\t{seqlen}\t{frame}\t{parent}\t{origin}\t{digest}\t{seguid}\t{seq}')
        out_opt.add_argument('--star', help="translate stop codons as *", action='store_true')
        out_opt.add_argument('-l', metavar="INT",
                             help="length of sequence lines in fasta output (default: 80)",
                             type=int, default=80)

        general_opt = parser.add_argument_group('6. General Options')
        general_opt.add_argument('--threads', type=int, default=1,
                                 help="number of threads to use (default: 1)")
        general_opt.add_argument('--version', action='version', version='%(prog)s ' + __version__)
        general_opt.add_argument("-h", "--help", action="help", help="show this help message and exit")

        parser.add_argument('--debug', help=argparse.SUPPRESS, action="store_true")
        return parser.parse_args()

    def check_args(args):
        if args.nuclen < 6:
            sys.exit("error: --nuclen cannot be smaller than 6")
        if "all" in args.report:
            args.report = {'nuc.fasta', 'prot.fasta', 'pep.fasta', 'nuc.txt', 'prot.txt', 'pep.txt', 'config.txt'}
        else:
            args.report = set(args.report)
        if args.mc < 0:
            sys.exit("error: --mc cannot be smaller than 0")
        if args.l < 0:
            args.l = 0
        args.l = int(args.l)
        if args.outbase == "salted_":
            args.outbase += os.path.splitext(os.path.basename(args.input.name))[0]
        if args.debug and len(args.report) > 1:
            sys.exit("error: in debug mode only one report type is allowed")
        if args.m1:
            args.protvariants = 1
        elif args.m2:
            args.protvariants = 2
        else:
            args.protvariants = 0

    args = get_args()
    check_args(args)
    saltobj = salt(args.input, outbase=args.outbase, tt=11, circ=not args.linear,
                   format=args.intype, debug=args.debug, cpus=args.threads)
    # Process input permutation if requested.
    if args.inperm:
        saltobj.dnaobj = saltobj.dnaobj.permutate()
    elif args.incodonperm:
        saltobj.dnaobj = saltobj.dnaobj.permutate(3)
    
    styles = {
        "nuc.fasta": args.nuc_fasta_style.strip('\'"'),
        "prot.fasta": args.prot_fasta_style.strip('\'"'),
        "pep.fasta": args.pep_fasta_style.strip('\'"'),
        "nuc.txt": args.nuc_txt_style.strip('\'"'),
        "prot.txt": args.prot_txt_style.strip('\'"'),
        "pep.txt": args.pep_txt_style.strip('\'"'),
        "config.txt": "parameter\tvalue"
    }
    for report in args.report:
        saltobj.add_report(report, styles[report])

    saltobj.create(*args.frames, cpus=args.threads, seqwrap=args.l, protease=args.protease, mc=args.mc,
                     minlen_nuc=args.nuclen, minlen_pep=args.peplen, prefix=args.p,
                     alt_starts=args.alt, m_init=args.protvariants, stop_to_stop=args.full,
                     nucperm=args.nucperm, codonperm=args.codonperm, protperm=args.protperm,
                     pepperm=args.pepperm, star=args.star, cut_N=args.cut_N)
